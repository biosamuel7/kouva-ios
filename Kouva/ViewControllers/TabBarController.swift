//
//  TabBarController.swift
//  Kouva
//
//  Created by Bio'S on 03/06/2019.
//  Copyright © 2019 Kouva. All rights reserved.
//

import UIKit

class TabBarController : UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let navigationController = UINavigationController(rootViewController: MainViewController())
        navigationController.title = "Recherche"
        navigationController.tabBarItem.image = #imageLiteral(resourceName: "icon_discorver_tab")
        
        
        let favorisController = FavorisHomeController()
        favorisController.title = "Enregistrés"
        favorisController.tabBarItem.image = #imageLiteral(resourceName: "likebar_icon")
        
        let alerteController = AlerteViewController()
        alerteController.title = "Alertes"
        alerteController.tabBarItem.image = #imageLiteral(resourceName: "alerte_icon")
        
        let userNavigationColler = UINavigationController(rootViewController: UserProfilController())
        userNavigationColler.title = "Profil"
        userNavigationColler.tabBarItem.image = #imageLiteral(resourceName: "icon_profil_bar")
        
        viewControllers = [navigationController,favorisController,alerteController,userNavigationColler]
        tabBar.tintColor = AppColor.primaryColor()
        tabBar.barTintColor = UIColor.white
        tabBar.isTranslucent = true
        
    }
}



