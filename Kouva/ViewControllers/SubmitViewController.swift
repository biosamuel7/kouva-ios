//
//  SubmitViewController.swift
//  Kouva
//
//  Created by MACBOOK on 10/01/2020.
//  Copyright © 2020 Kouva. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMapsBase

class SubmitViewController: UIViewController {

    var homeType: TypeDebienModel?
    
    var minBudget = [String]()
    var maxBudget = [String]()
    
    var budget = [[String]]()

    let budgetTextField = UITextField()
    let locationTextField = UITextField()
    
    let pickerView = UIPickerView()
    
    var min:String!
    var max:String!
    
    var places = [GMSPlace]()
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
        setupConstraints()
        
        var d = [Int]()
        d.append(50000)
        for _ in 1...100 {
            guard let last = d.last else { return }
            d.append(last + 50000)
            minBudget.append(String(last + 50000))
            maxBudget.append(String(last + 50000))
        }
        
        budget.append(minBudget)
        budget.append(maxBudget)
        
        min = budget[0][0]
        max = budget[1][0]
        
        let tap = UITapGestureRecognizer()
        tap.numberOfTapsRequired = 1
        tap.addTarget(self, action: #selector(dismissPicker))
        self.view.addGestureRecognizer(tap)
    }
    
    
    @objc func autoCompletion(){
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self

        let filter = GMSAutocompleteFilter()
        filter.type = .establishment
        autocompleteController.autocompleteFilter = filter
        present(autocompleteController, animated: true, completion: nil)
        //present(MapSearchViewController(), animated: true, completion: nil)
    }
    
    func setupConstraints() {
        var heighBtn:CGFloat = 0
          var titleSize:CGFloat = 0
          var subtitleSize:CGFloat = 14
          var subtitleHeigh:CGFloat = 90
          var conditionTop:CGFloat = 50
          var conditionSize:CGFloat = 14
          switch view.frame.width {
          case 320.0:
              heighBtn = 40
              titleSize = 20
              subtitleHeigh = 80
              subtitleSize = 13
              conditionTop = 30
              conditionSize = 13
          case 375.0 :
              heighBtn = 45
              titleSize = 25
              subtitleSize = 14
              subtitleHeigh = 90
              conditionSize = 13
          case 414.0:
              heighBtn = 50
              titleSize = 30
              subtitleHeigh = 100
          default:
              break
          }
        
        let titleTextView: UITextView = UITextView()
        titleTextView.text = "Créer une alerte pour mon studio"
        titleTextView.font = UIFont(name: "Avenir-black", size: titleSize)
        titleTextView.textColor = AppColor.black()
        titleTextView.textAlignment = .left
        titleTextView.isEditable = false
        titleTextView.isScrollEnabled = false
        titleTextView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(titleTextView)
        titleTextView.topAnchor.constraint(equalTo: view.topAnchor, constant: 10).isActive = true
        titleTextView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -40).isActive = true
        titleTextView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        titleTextView.heightAnchor.constraint(equalToConstant: subtitleHeigh).isActive = true
        
        
        let subTitleTextView: UITextView = UITextView()
        subTitleTextView.text = "Enregistrer votre recherche et recevez les biens d’un centaine d’agent immobilier selon vos critères."
        subTitleTextView.font = UIFont(name: "Avenir-Medium", size: subtitleSize)
        subTitleTextView.textColor = AppColor.black()
        subTitleTextView.textAlignment = .left
        subTitleTextView.isEditable = false
        subTitleTextView.isScrollEnabled = false
        subTitleTextView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(subTitleTextView)
        subTitleTextView.topAnchor.constraint(equalTo: titleTextView.bottomAnchor, constant: 5).isActive = true
        subTitleTextView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -40).isActive = true
        subTitleTextView.leftAnchor.constraint(equalTo: titleTextView.leftAnchor).isActive = true
        subTitleTextView.heightAnchor.constraint(equalToConstant: subtitleHeigh).isActive = true
        
        
        let locationTitle = UILabel()
        locationTitle.text = "Localisation"
        locationTitle.textColor = AppColor.black()
        locationTitle.textAlignment = .left
        locationTitle.translatesAutoresizingMaskIntoConstraints = false
        locationTitle.font = UIFont(name: "Avenir-Book", size: subtitleSize)
        view.addSubview(locationTitle)
        locationTitle.topAnchor.constraint(equalTo: subTitleTextView.bottomAnchor, constant: 40).isActive = true
        locationTitle.leftAnchor.constraint(equalTo: subTitleTextView.leftAnchor, constant: 5).isActive = true
        
        
        locationTextField.placeholder = "Entrer la localisation"
        locationTextField.textAlignment = .center
        locationTextField.backgroundColor = UIColor(r: 247, g: 247, b: 247)
        locationTextField.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(locationTextField)
        locationTextField.topAnchor.constraint(equalTo: locationTitle.bottomAnchor, constant: 5).isActive = true
        locationTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
        locationTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
        locationTextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
        locationTextField.layer.cornerRadius = 10
        locationTextField.clipsToBounds = true
        locationTextField.addTarget(self, action: #selector(autoCompletion), for: .editingDidBegin)
        
        
        
        let budgetTitle = UILabel()
        budgetTitle.text = "Localisation"
        budgetTitle.textColor = AppColor.black()
        budgetTitle.textAlignment = .left
        budgetTitle.translatesAutoresizingMaskIntoConstraints = false
        budgetTitle.font = UIFont(name: "Avenir-Book", size: subtitleSize)
        view.addSubview(budgetTitle)
        budgetTitle.topAnchor.constraint(equalTo: locationTextField.bottomAnchor, constant: 40).isActive = true
        budgetTitle.leftAnchor.constraint(equalTo: locationTextField.leftAnchor, constant: 5).isActive = true
        
        
        budgetTextField.placeholder = "Entrer votre budget"
        budgetTextField.textAlignment = .center
        budgetTextField.backgroundColor = UIColor(r: 247, g: 247, b: 247)
        budgetTextField.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(budgetTextField)
        budgetTextField.topAnchor.constraint(equalTo: budgetTitle.bottomAnchor, constant: 5).isActive = true
        budgetTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
        budgetTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
        budgetTextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
        budgetTextField.layer.cornerRadius = 10
        budgetTextField.clipsToBounds = true
        
        
        pickerView.showsSelectionIndicator = true
        pickerView.dataSource = self
        pickerView.delegate = self
        budgetTextField.inputView = pickerView
        
        
        
        var submitBtn = UIButton()
        submitBtn.backgroundColor = AppColor.primaryColor()
        submitBtn.SetTitleBtnAndFont(title: "Soumettre")
        submitBtn.translatesAutoresizingMaskIntoConstraints = false
        submitBtn.addTarget(self, action: #selector(submitUserSearch), for: .touchUpInside)
        view.addSubview(submitBtn)
        submitBtn.widthAnchor.constraint(equalTo: budgetTextField.widthAnchor).isActive = true
        submitBtn.heightAnchor.constraint(equalToConstant: 50).isActive = true
        submitBtn.centerXAnchor.constraint(equalTo: budgetTextField.centerXAnchor).isActive = true
        if #available(iOS 11.0, *) {
            submitBtn.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -20).isActive = true
        } else {
            submitBtn.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -20).isActive = true
        }
    }
    
    @objc func submitUserSearch() {
        guard let homeType = homeType else { return  }
        
        if UserDefaults.standard.value(forKey: "userID") != nil {
            if places.count > 0 {
                let place = places[0]
                DataBaseService.instance.userSearchCollection.addDocument(data: [
                    "budget": [
                        "min": min,
                        "max": max
                    ],
                    "type": homeType.type,
                    "possession": homeType.possession,
                    "places":[
                        "longitude": place.coordinate.longitude,
                        "latitude": place.coordinate.latitude,
                        "formatted_address": place.formattedAddress,
                        "name": place.name,
                        "placeId": place.placeID,
                    ]
                ])
                self.dismiss(animated: true, completion: nil)
            } else {
                Alertes.AlerteError(error: "Remplissez tous les champs", VC: self)
            }
        } else {
            let loginVC = UserLoginViewController()
            present(loginVC, animated: true, completion: nil)
        }
        
        
    }

}

extension SubmitViewController: GMSAutocompleteViewControllerDelegate {

  // Handle the user's selection.
  func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
    places.removeAll()
    places.append(place)
    locationTextField.text = place.name
    print("Place name: \(place.name)")
    print("Place ID: \(place.placeID)")
    print("Place attributions: \(place.attributions)")
    dismiss(animated: true, completion: nil)
  }

  func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
    // TODO: handle the error.
    print("Error: ", error.localizedDescription)
  }

  // User canceled the operation.
  func wasCancelled(_ viewController: GMSAutocompleteViewController) {
    dismiss(animated: true, completion: nil)
  }

  // Turn the network activity indicator on and off again.
  func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
    UIApplication.shared.isNetworkActivityIndicatorVisible = true
  }

  func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
    UIApplication.shared.isNetworkActivityIndicatorVisible = false
  }

}

extension SubmitViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return budget[component].count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\(budget[component][row])"
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if component == 0 {
            min = budget[0][row]
            max = budget[0][row + 1]
            pickerView.selectRow(row + 1, inComponent: 1, animated: true)
        } else if component == 1{
            max = budget[1][row]
        }
        budgetTextField.text = "\(min!) - \(max!)"
    }
    @objc func dismissPicker(){
        self.view.endEditing(true)
    }
}
