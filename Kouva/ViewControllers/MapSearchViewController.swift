//
//  MapSearchViewController.swift
//  Kouva
//
//  Created by Bio'S on 25/08/2019.
//  Copyright © 2019 Kouva. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMapsBase

struct MapStruct {
    let name:String!
    let subName: String!
}

class MapSearchViewController: UIViewController {

    var listingMapCollectionView: UITableView = {
        let tableview = UITableView()
        tableview.translatesAutoresizingMaskIntoConstraints = false
        return tableview
    }()
    
    var fetcher: GMSAutocompleteFetcher?
    
    var tableData =  [MapStruct]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
        userPlaceData()
        
    }
    func userPlaceData() {
        
        let textField = UITextField()
        textField.backgroundColor = .lightGray
        textField.addTarget(self, action: #selector(textFieldDidChanged(_:)), for: .editingChanged)
        textField.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(textField)
        textField.topAnchor.constraint(equalTo: view.topAnchor, constant: 50).isActive = true
        textField.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        textField.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        textField.heightAnchor.constraint(equalToConstant: 70).isActive = true
        
        // Set bounds to inner-west Sydney Australia.
        let neBoundsCorner = CLLocationCoordinate2D(latitude: -4.0676075,
                                                    longitude: 5.3198249)
        let swBoundsCorner = CLLocationCoordinate2D(latitude: -4.0676075,
                                                    longitude: 5.3198249)
        let bounds = GMSCoordinateBounds(coordinate: neBoundsCorner,
                                         coordinate: swBoundsCorner)

        // Set up the autocomplete filter.
        let filter = GMSAutocompleteFilter()
        filter.type = .establishment

        // Create the fetcher.
        fetcher = GMSAutocompleteFetcher(bounds: bounds, filter: filter)
        fetcher?.delegate = self as GMSAutocompleteFetcherDelegate
        
        
        listingMapCollectionView.dataSource = self
        listingMapCollectionView.delegate = self
        listingMapCollectionView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        view.addSubview(listingMapCollectionView)
        listingMapCollectionView.topAnchor.constraint(equalTo: textField.bottomAnchor, constant: 10).isActive = true
        listingMapCollectionView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        listingMapCollectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        
    }
    @objc func textFieldDidChanged(_ textField:UITextField ){
        print(textField.text!)
        fetcher?.sourceTextHasChanged(textField.text!)
    }
}

extension MapSearchViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell = UITableViewCell(style: .subtitle, reuseIdentifier: "cell")
        cell.textLabel?.text = tableData[indexPath.item].name
        cell.detailTextLabel?.text = tableData[indexPath.item].subName
    
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}

extension MapSearchViewController :  GMSAutocompleteFetcherDelegate {
    
    func didAutocomplete(with predictions: [GMSAutocompletePrediction]) {
        tableData = []
        for prediction in predictions {
            print("prediction:",prediction.attributedSecondaryText?.string)
            print("prediction primary:",prediction.attributedPrimaryText.string)
            tableData.append(MapStruct(name: prediction.attributedPrimaryText.string, subName: prediction.attributedSecondaryText?.string))
            
//            let placeField: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.))
            let placeClient: GMSPlacesClient = GMSPlacesClient()
//            placeClient.fetchPlace(fromPlaceID: prediction.placeID, placeFields: GMSPlaceField(rawValue: UInt(GMSPlaceField.coordinate.rawValue))!, sessionToken: nil) { (place: GMSPlace?, error: Error?) in
//                if error != nil {
//                    print("ddd)
//                }
//
//                print("prediction coordinate:", place?.coordinate)
//            }
        }
        listingMapCollectionView.reloadData()
    }
    
    func didFailAutocompleteWithError(_ error: Error) {
        print("error prediction: ",error.localizedDescription)
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        print(searchController.searchBar.text!)
        fetcher?.sourceTextHasChanged(searchController.searchBar.text!)
    }
    
    
}
