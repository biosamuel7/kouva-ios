//
//  AproposViewController.swift
//  Kouva
//
//  Created by Bio'S on 03/06/2019.
//  Copyright © 2019 Kouva. All rights reserved.
//

import UIKit

//aporpos model
struct aproposModel {
    var header:String
    var content:String
}

class AproposViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    var data = [aproposModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = UIColor.white
        navigationController?.navigationBar.tintColor = AppColor.primaryColor()
        navigationController?.navigationBar.isHidden = false
        
        collectionView?.backgroundColor = UIColor.white
        collectionView?.register(aboutCell.self, forCellWithReuseIdentifier: "cell")
        collectionView?.register(aboutHeader.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "headerCell")
        
        data.append(aproposModel(header: "Qui sommes-nous ?", content: "Kouva est une plateforme immobilière qui réunit tous les acteurs de l’immobilier afin de faciliter les transactions entre l'offre et la demande pour la location et la vente de biens."))
        data.append(aproposModel(header: "Notre mission", content: "Notre mission est de faciliter l'accès aux biens immobiliers sur le marché africain afin de permettre à quiconque de louer et acheter un bien immobilier peu importe sa situation géographique , sans avoir à effectuer le moindre déplacement déplacer."))
        data.append(aproposModel(header: "Nos services", content: "Nous vous proposons une panoplie de biens immobiliers de tout types ( studio , appartement , villa , magasin etc...), avec toutes les informations afférentes à ceux-ci ,  afin de vous permettre de choisir votre bien en fonction de vos critères."))
        
    }
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return data.count
    }
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! aboutCell
        cell.textView.text = data[indexPath.section].content
        return cell
    }
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "headerCell", for: indexPath) as! aboutHeader
        header.label.text = data[indexPath.section].header
        return header
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width - 40
        let contentHeigh = data[indexPath.section].content.estimateheight(withConstrainedWidth: width, font: UIFont(name: "Avenir-Heavy", size: 14)!)
        return .init(width: width, height: contentHeigh + 40)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return .init(width: collectionView.frame.width - 40, height: 40)
    }
    
}

//
class aboutCell: UICollectionViewCell {
    let textView = UITextView()
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .yellow
        
        
        textView.textColor = AppColor.black()
        textView.font = UIFont(name: "Avenir-Heavy", size: 14)
        textView.textAlignment = .justified
        textView.sizeToFit()
        textView.text = "Kouva est une plateforme immobilière qui réunit tous les acteurs de l’immobilier afin de faciliter les transactions entre l'offre et la demande pour la location et la vente de biens."
        textView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(textView)
        textView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        textView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        textView.heightAnchor.constraint(equalTo: heightAnchor).isActive = true
        textView.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//about header for collectionview
class aboutHeader: UICollectionReusableView {
    
    let label = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        label.text = "Qui sommes-nous ?"
        label.font = UIFont(name: "Avenir-Black", size: 20)
        label.textColor = AppColor.primaryColor()
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        addSubview(label)
        label.leftAnchor.constraint(equalTo: leftAnchor,constant: 30).isActive = true
        label.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
