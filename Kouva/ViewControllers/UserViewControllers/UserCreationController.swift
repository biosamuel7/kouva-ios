//
//  UserCreationController.swift
//  Kouva
//
//  Created by Bio'S on 03/06/2019.
//  Copyright © 2019 Kouva. All rights reserved.
//

import UIKit

class UserCreationController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITextFieldDelegate {
    
    //TODO: creation de user, edite user profile
    
    var nom:String?
    var prenom:String?
    var email:String?
    var password:String?
    
    var VC:UserLoginViewController?
    
    
    lazy var textFieldCollection: UICollectionView = {
        var layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.sectionInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
        layout.minimumLineSpacing = 3
        layout.minimumInteritemSpacing = 1
        var collection = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        collection.register(TextFieldCell.self, forCellWithReuseIdentifier: "cell")
        collection.backgroundColor = UIColor.white
        collection.showsVerticalScrollIndicator = false
        collection.isScrollEnabled = false
        collection.translatesAutoresizingMaskIntoConstraints = false
        return collection
    }()
    let NextTapBtn: UIButton = {
        let btn = UIButton()
        btn.setTitle("Valider", for: .normal)
        btn.backgroundColor = AppColor.primaryColor()
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    var progressView: UIActivityIndicatorView = {
        let progress = UIActivityIndicatorView()
        progress.color = UIColor.white
        progress.translatesAutoresizingMaskIntoConstraints = false
        return progress
    }()
    
    var containerScollView: UIScrollView = {
        let scroll = UIScrollView()
        scroll.backgroundColor = .white
        scroll.showsVerticalScrollIndicator = false
        scroll.translatesAutoresizingMaskIntoConstraints = false
        return scroll
    }()
    
    var showPasswordVerification:Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        
        SetUpConstraints()
        
        textFieldCollection.delegate = self
        textFieldCollection.dataSource = self
        
        NextTapBtn.addTarget(self, action: #selector(NextViewForRegistation), for: .touchUpInside)
        
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(ResignAllKeyBoardWhenViewIsTaped)))
        
        //        print("debug",view.frame.height)
        switch view.frame.height {
        case 568.0: // iphone 5
            containerScollView.contentSize = CGSize(width: view.frame.width, height: view.frame.height + 210)
        case 667.0: // iphone 6
            containerScollView.contentSize = CGSize(width: view.frame.width, height: view.frame.height + 200)
        case 736.0: // iphone Plus
            containerScollView.contentSize = CGSize(width: view.frame.width, height: view.frame.height + 220)
        case 812.0: // iphone X
            containerScollView.contentSize = CGSize(width: view.frame.width, height: view.frame.height + 130)
        case 896.0: // iphone Xs
            containerScollView.contentSize = CGSize(width: view.frame.width, height: view.frame.height + 100)
        default:
            break
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .lightContent
        
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! TextFieldCell
        
        cell.label.textColor = AppColor.titleGrayColor()
        cell.input.textColor = AppColor.titleGrayMediumColor()
        cell.viewBar.backgroundColor = UIColor.lightGray
        cell.input.tag = indexPath.row
        cell.input.returnKeyType = .continue
        cell.input.delegate = self
        
        switch indexPath.row {
        case 0:
            cell.label.text = "Nom"
            cell.input.placeholder = "Ajouter votre nom"
            cell.input.keyboardType = .emailAddress
            break
        case 1:
            cell.label.text = "Prenom"
            cell.input.placeholder = "Ajouter votre prenom"
            break
        case 2:
            cell.label.text = "Email"
            cell.input.placeholder = "Ajouter votre email"
            cell.input.keyboardType = .emailAddress
            break
        case 3:
            cell.label.text = "Password"
            cell.input.placeholder = "Ajouter votre password"
            cell.showPassword.setTitleColor(AppColor.titleGrayColor(), for: .normal)
            cell.showPassword.isHidden = false
            cell.input.isSecureTextEntry = true
            cell.showPassword.addTarget(self, action: #selector(ShowPassword), for: .touchUpInside)
            break
        default:
            break
        }
        
        return cell
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        //        let tag = textField.tag
        
        //        switch tag {
        //        case 1:
        //            ScrollTextField(tag: tag)
        //            break
        //        case 2:
        //            ScrollTextField(tag: tag)
        //            break
        //        case 3:
        //            ScrollTextField(tag: tag)
        //            break
        //        case 4:
        //
        //            ScrollTextField(tag: tag)
        //
        //            switch view.frame.width {
        //            case 320:
        //                NextTapBtnY?.constant = -250
        //                break
        //            case 375:
        //                NextTapBtnY?.constant = -300
        //                break
        //            case 414:
        //                NextTapBtnY?.constant = -350
        //                break
        //            default:
        //                break
        //            }
        //            view.layoutIfNeeded()
        //
        //            break
        //        default:
        //            break
        //        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let tag = textField.tag
        if tag <= 3 {
            let index = IndexPath(item: tag + 1, section: 0)
            if let cell = textFieldCollection.cellForItem(at: index) as? TextFieldCell {
                cell.input.becomeFirstResponder()
            }
        }
        return true
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: (collectionView.frame.height / 4))
    }
    func SetUpConstraints() {
        view.addSubview(containerScollView)
        view.addContraintAllScreen(view: containerScollView)
        
        let dissMissViewBtn = UIButton(type: .system)
        dissMissViewBtn.setImage(#imageLiteral(resourceName: "Back Chevron_blue").withRenderingMode(.alwaysOriginal), for: .normal)
        dissMissViewBtn.addTarget(self, action: #selector(DismissView), for: .touchUpInside)
        dissMissViewBtn.translatesAutoresizingMaskIntoConstraints = false
        containerScollView.addSubview(dissMissViewBtn)
        dissMissViewBtn.topAnchor.constraint(equalTo: containerScollView.topAnchor, constant: 20).isActive = true
        dissMissViewBtn.leftAnchor.constraint(equalTo: containerScollView.leftAnchor, constant: 15).isActive = true
        dissMissViewBtn.heightAnchor.constraint(equalToConstant: 20).isActive = true
        dissMissViewBtn.widthAnchor.constraint(equalToConstant: 25).isActive = true
        
        
        let headerTitle: UILabel = UILabel()
        headerTitle.text = "Inscription"
        headerTitle.font = UIFont(name: "Avenir-Black", size: 30)
        headerTitle.textColor = AppColor.black()
        headerTitle.translatesAutoresizingMaskIntoConstraints = false
        containerScollView.addSubview(headerTitle)
        headerTitle.topAnchor.constraint(equalTo: dissMissViewBtn.bottomAnchor, constant: 20).isActive = true
        headerTitle.leftAnchor.constraint(equalTo: dissMissViewBtn.rightAnchor, constant: 0).isActive = true
        
        
        let subTitle: UITextView = UITextView()
        subTitle.text = "Inscrivez les champs ci-dessous pour etre connecte."
        subTitle.font = UIFont(name: "Avenir-Medium", size: 15)
        subTitle.textColor = AppColor.black()
        subTitle.translatesAutoresizingMaskIntoConstraints = false
        containerScollView.addSubview(subTitle)
        subTitle.topAnchor.constraint(equalTo: headerTitle.bottomAnchor, constant: 10).isActive = true
        subTitle.centerXAnchor.constraint(equalTo: containerScollView.centerXAnchor).isActive = true
        subTitle.heightAnchor.constraint(equalToConstant: 50).isActive = true
        subTitle.widthAnchor.constraint(equalTo: containerScollView.widthAnchor, constant: -70).isActive = true
        
        containerScollView.addSubview(textFieldCollection)
        textFieldCollection.topAnchor.constraint(equalTo: subTitle.bottomAnchor, constant: 50).isActive = true
        textFieldCollection.centerXAnchor.constraint(equalTo: containerScollView.centerXAnchor).isActive = true
        textFieldCollection.widthAnchor.constraint(equalTo: containerScollView.widthAnchor, constant: -70).isActive = true
        textFieldCollection.heightAnchor.constraint(equalToConstant: 400).isActive = true
        
        
        containerScollView.addSubview(NextTapBtn)
        NextTapBtn.topAnchor.constraint(equalTo: textFieldCollection.bottomAnchor, constant: 20).isActive = true
        NextTapBtn.widthAnchor.constraint(equalTo: containerScollView.widthAnchor, multiplier: 1/3, constant: -20).isActive = true
        NextTapBtn.heightAnchor.constraint(equalTo: NextTapBtn.widthAnchor, multiplier: 1/3, constant: 10).isActive = true
        NextTapBtn.rightAnchor.constraint(equalTo: textFieldCollection.rightAnchor).isActive = true
        NextTapBtn.layer.cornerRadius = 5
        NextTapBtn.SetTitleBtnAndFont(title: "Suivant")
        NextTapBtn.addTarget(self, action: #selector(NextViewForRegistation), for: .touchUpInside)
        
        containerScollView.addSubview(progressView)
        progressView.centerXAnchor.constraint(equalTo: NextTapBtn.centerXAnchor).isActive = true
        progressView.centerYAnchor.constraint(equalTo: NextTapBtn.centerYAnchor).isActive = true
    }
    @objc func DismissView() {
        self.dismiss(animated: false, completion: nil)
        
    }
    func DismissViewForInscription() {
        self.dismiss(animated: false, completion: {
            self.VC?.DismissLoginView()
        })
        
    }
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        //        NextTapBtnY?.constant = -10
        UIView.animate(withDuration: 0.24, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
        
    }
    
    @objc func ShowPassword(){
        let index = IndexPath(item: 3, section: 0)
        if showPasswordVerification{
            if let cell = textFieldCollection.cellForItem(at: index) as? TextFieldCell {
                cell.input.isSecureTextEntry = false
                showPasswordVerification = false
            }
        }else{
            if let cell = textFieldCollection.cellForItem(at: index) as? TextFieldCell {
                cell.input.isSecureTextEntry = true
                showPasswordVerification = true
            }
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        let tag = textField.tag
        
        switch tag {
        case 0:
            let indexNom = IndexPath(item: tag, section: 0)
            if let cell = textFieldCollection.cellForItem(at: indexNom) as? TextFieldCell {
                if let value = cell.input.text {
                    nom = value
                }
            }
            break
        case 1:
            let indexPrenom = IndexPath(item: tag, section: 0)
            if let cell = textFieldCollection.cellForItem(at: indexPrenom) as? TextFieldCell {
                if let value = cell.input.text {
                    prenom = value
                }
            }
        case 2:
            let indexEmail = IndexPath(item: tag, section: 0)
            if let cell = textFieldCollection.cellForItem(at: indexEmail) as? TextFieldCell {
                if let value = cell.input.text {
                    email = value
                }
            }
            break
        case 3 :
            let indexPass = IndexPath(item: tag, section: 0)
            if let cell = textFieldCollection.cellForItem(at: indexPass) as? TextFieldCell {
                if let value = cell.input.text {
                    password = value
                }
            }
            break
        default:
            break
        }
        
    }
    
    @objc func NextViewForRegistation() {
        ResignAllKeyBoardWhenViewIsTaped()
        if let name = nom,let pren = prenom, let mail = email,let pssw = password {
            
            if name.isEmpty {
                Alertes.AlerteError(error: "Remplissez tous les champs", VC: self)
            }
            
            if pren.isEmpty {
                Alertes.AlerteError(error: "Remplissez tous les champs", VC: self)
            }
            
            if mail.isEmpty{
                Alertes.AlerteError(error: "Remplissez tous les champs", VC: self)
            }
            
            if pssw.isEmpty{
                Alertes.AlerteError(error: "Remplissez tous les champs", VC: self)
            }
            
            //            let token:String = UserDefaults.standard.value(forKey: "tokenFCM") as! String
            let data:[String:Any] = ["nom":name,"prenom":pren,"email":mail]
            NextTapBtn.SetTitleBtnAndFont(title: " ")
            progressView.startAnimating()
            NextTapBtn.isEnabled = false
            AuthService.instance.CreateUser(email: mail, password: pssw, VC: self, dataInfo: data)
            
        }
    }
    
    @objc func ResignAllKeyBoardWhenViewIsTaped() {
        for i in 0...4 {
            let index = IndexPath(item: i, section: 0)
            if let cell = textFieldCollection.cellForItem(at: index) as? TextFieldCell {
                cell.input.resignFirstResponder()
            }
        }
        //        NextTapBtnY?.constant = -10
        UIView.animate(withDuration: 0.75, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.view.layoutIfNeeded()
            self.textFieldCollection.contentOffset.y = 0
        }, completion: nil)
    }
}



