//
//  UserLoginViewController.swift
//  Kouva
//
//  Created by Bio'S on 03/06/2019.
//  Copyright © 2019 Kouva. All rights reserved.
//

import UIKit
import FirebaseAuth
import GoogleSignIn
import SafariServices


class UserLoginViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITextFieldDelegate, GIDSignInDelegate, UITextViewDelegate  {
    
    
    let cellId = "cell"
    
    var showPasswordVerification:Bool = true
    
    let connectionLoadView: UIView = {
        let v = UIView()
        v.isHidden = true
        v.backgroundColor = UIColor(r: 0, g: 0, b: 0, alpha: 0.3)
        v.translatesAutoresizingMaskIntoConstraints = false
        
        let loadView:UIView = UIView()
        loadView.backgroundColor = UIColor.white
        loadView.translatesAutoresizingMaskIntoConstraints = false
        v.addSubview(loadView)
        loadView.centerYAnchor.constraint(equalTo: v.centerYAnchor).isActive = true
        loadView.centerXAnchor.constraint(equalTo: v.centerXAnchor).isActive = true
        loadView.widthAnchor.constraint(equalTo: v.widthAnchor, multiplier: 1/3).isActive = true
        loadView.heightAnchor.constraint(equalTo: loadView.widthAnchor).isActive = true
        loadView.layer.cornerRadius = 10
        loadView.clipsToBounds = true
        
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
        indicator.color = AppColor.primaryColor()
        indicator.startAnimating()
        indicator.translatesAutoresizingMaskIntoConstraints = false
        loadView.addSubview(indicator)
        indicator.centerYAnchor.constraint(equalTo: loadView.centerYAnchor).isActive = true
        indicator.centerXAnchor.constraint(equalTo: loadView.centerXAnchor).isActive = true
        
        return v
    }()
    
    lazy var dissMissViewBtn:UIButton = {
        let dissMissViewBtn = UIButton(type: .system)
        dissMissViewBtn.setImage(#imageLiteral(resourceName: "Back Chevron_blue").withRenderingMode(.alwaysOriginal), for: .normal)
        dissMissViewBtn.addTarget(self, action: #selector(DismissLoginView), for: .touchUpInside)
        dissMissViewBtn.translatesAutoresizingMaskIntoConstraints = false
        return dissMissViewBtn
    }()
    lazy var textFieldCollection: UICollectionView = {
        var layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumLineSpacing = 3
        layout.minimumInteritemSpacing = 1
        var collection = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        collection.register(TextFieldCell.self, forCellWithReuseIdentifier: "cell")
        collection.backgroundColor = UIColor.white
        collection.isScrollEnabled = false
        collection.translatesAutoresizingMaskIntoConstraints = false
        return collection
    }()
    let connexionBtn: UIButton = {
        let btn = UIButton()
        btn.SetTitleBtnAndFont(title: "Se connecter", size: 16, titleColor: .white, font: "Avenir-Black")
        btn.backgroundColor = AppColor.primaryColor()
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    let creationCompteBtn: UIButton = {
        let btn = UIButton()
        btn.SetTitleBtnAndFont(title: "Créer un compte", size: 16, titleColor: AppColor.primaryColor(), font: "Avenir-Black")
        btn.backgroundColor = UIColor.white
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    var progressView: UIActivityIndicatorView = {
        let loadView = UIActivityIndicatorView()
        loadView.color = UIColor.white
        loadView.translatesAutoresizingMaskIntoConstraints = false
        return loadView
    }()
    
    var userNumberView: UserAddNumberView = {
        let view = UserAddNumberView()
        view.isHidden = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var containerScollView: UIScrollView = {
        let scroll = UIScrollView()
        scroll.showsVerticalScrollIndicator = false
        scroll.translatesAutoresizingMaskIntoConstraints = false
        return scroll
    }()
    let conditionTextView: UITextView = UITextView()
    
    
    @objc func showUserAddNumberView() {
        userNumberView.isHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        UIApplication.shared.isStatusBarHidden = false
        UIApplication.shared.statusBarStyle = .default
        
        creationCompteBtn.addTarget(self, action: #selector(ShowCreationCompteVC), for: .touchUpInside)
        connexionBtn.addTarget(self, action: #selector(ConnextionUserEvent), for: .touchUpInside)
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(ResignTextField)))
        SetUpLoginContainer()
        SetupkeyboardObserver()
        
        NotificationCenter.default.addObserver(self, selector: #selector(signUserPhoneNumberUsingCodeVerification(_:)), name: .didSignUserPhoneNumberByVerificationCode, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(showUserAddNumberView), name: .didShowUserAddNumberView, object: nil)
        
        
        switch view.frame.height {
        case 568.0: // iphone 5
            containerScollView.contentSize = CGSize(width: view.frame.width, height: view.frame.height + 210)
        case 667.0: // iphone 6
            containerScollView.contentSize = CGSize(width: view.frame.width, height: view.frame.height + 200)
        case 736.0: // iphone Plus
            containerScollView.contentSize = CGSize(width: view.frame.width, height: view.frame.height + 200)
        case 812.0: // iphone X
            containerScollView.contentSize = CGSize(width: view.frame.width, height: view.frame.height + 70)
        case 896.0: // iphone Xs Max
            containerScollView.contentSize = CGSize(width: view.frame.width, height: view.frame.height + 200)
        default:
            break
        }
        containerScollView.delegate = self
        
        textFieldCollection.delegate = self
        textFieldCollection.dataSource = self
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 2
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! TextFieldCell
        cell.label.textColor = AppColor.titleGrayColor()
        cell.input.textColor = AppColor.titleGrayMediumColor()
        cell.viewBar.backgroundColor = UIColor.lightGray
        cell.input.delegate = self
        cell.input.tag = indexPath.row
        
        switch indexPath.row {
        case 0:
            cell.label.text = "email"
            cell.input.keyboardType = .emailAddress
            cell.input.returnKeyType = .next
            break
        case 1:
            cell.label.text = "password"
            cell.showPassword.setTitle("afficher", for: .normal)
            cell.showPassword.isHidden = false
            cell.input.isSecureTextEntry = true
            cell.input.returnKeyType = .done
            cell.showPassword.addTarget(self, action: #selector(ShowPassword), for: .touchUpInside)
            break
            
        default:
            break
        }
        return cell
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height / 2)
        
    }
    
    //MARK: keyboard handle
    func SetupkeyboardObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(handlekeyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handlekeyboardWillHide), name: .UIKeyboardWillHide, object: nil)
    }
    @objc func handlekeyboardWillShow(notification: Notification) {
        if let keyboardDuration = notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as? Double{
            
            UIView.animate(withDuration: keyboardDuration) {
                switch self.view.frame.height {
                case 568.0: // iphone 5
                    self.containerScollView.contentOffset.y = 420.0
                case 667.0: // iphone 6
                    self.containerScollView.contentOffset.y = 410.0
                case 736.0: // iphone Plus
                    self.containerScollView.contentOffset.y = 420.0
                case 812.0: // iphone X
                    self.containerScollView.contentOffset.y = 380.0
                case 896.0: // iphone Xs Max
                    self.containerScollView.contentOffset.y = 420.0
                default:
                    break
                }
            }
        }
    }
    @objc func handlekeyboardWillHide(notification: Notification) {
        switch self.view.frame.height {
        case 568.0: // iphone 5
            self.containerScollView.contentOffset.y = -20
        case 667.0: // iphone 6
            self.containerScollView.contentOffset.y = -20
        case 736.0: // iphone Plus
            self.containerScollView.contentOffset.y = -20
        case 812.0: // iphone X
            self.containerScollView.contentOffset.y = -40
        case 896.0: // iphone Xs Max
            self.containerScollView.contentOffset.y = -20
        default:
            break
        }
        self.view.layoutIfNeeded()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let tag = textField.tag
        if tag < 1 {
            let index = IndexPath(item: tag + 1, section: 0)
            if let cell = textFieldCollection.cellForItem(at: index) as? TextFieldCell {
                cell.input.becomeFirstResponder()
            }
        }
        if tag == 1 {
            ConnextionUserEvent()
        }
        return true
    }
    
    @objc func ShowPassword(){
        let index = IndexPath(item: 1, section: 0)
        if showPasswordVerification {
            if let cell = textFieldCollection.cellForItem(at: index) as? TextFieldCell {
                cell.input.isSecureTextEntry = false
                showPasswordVerification = false
                cell.showPassword.setTitle("Masquer", for: .normal)
            }
        }else{
            if let cell = textFieldCollection.cellForItem(at: index) as? TextFieldCell {
                cell.input.isSecureTextEntry = true
                showPasswordVerification = true
                cell.showPassword.setTitle("Afficher", for: .normal)
            }
        }
    }
    
    // connect user with eamil and password
    @objc func ConnextionUserEvent() {
        var email:String?
        var password:String?
        let emailIndexPath = IndexPath(item: 0, section: 0)
        if let cell = textFieldCollection.cellForItem(at: emailIndexPath) as? TextFieldCell {
            if let value = cell.input.text {
                email = value
            }
        }
        
        let passwordIndexPath = IndexPath(item: 1, section: 0)
        if let cell = textFieldCollection.cellForItem(at: passwordIndexPath) as? TextFieldCell {
            if let value = cell.input.text {
                password = value
            }
        }
        if let em = email, let pwsd = password {
            if em.isEmpty {
                Alertes.AlerteError(error: "email vide", VC: self)
            } else if  pwsd.isEmpty {
                Alertes.AlerteError(error: "password vide", VC: self)
            }else {
                let emailIndexPath = IndexPath(item: 1, section: 0)
                if let cell = textFieldCollection.cellForItem(at: emailIndexPath) as? TextFieldCell {
                    cell.input.resignFirstResponder()
                }
                connexionBtn.SetTitleBtnAndFont(title: "", size: 16, titleColor: .white, font: "Avenir-Black")
                progressView.startAnimating()
                connexionBtn.isEnabled = false
                AuthService.instance.login(email: em, password: pwsd, VC: self)
            }
        }
        
    }
    
    @objc func facebookConnexion(){
        AuthService.instance.facebookLogin(VC: self)
    }
    
    @objc func googleSignConnexion(){
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance()?.delegate = self
        GIDSignIn.sharedInstance()?.signIn()
        connectionLoadView.isHidden = false
    }
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if error != nil {
            print("Google Login failed:", error!)
            connectionLoadView.isHidden = true
            return
        }
        guard let authentication = user.authentication else { return }
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken, accessToken: authentication.accessToken)
        Auth.auth().signIn(with: credential) { (authResult, errorSign) in
            if let err = errorSign {
                print(err)
            }
            guard let user = authResult?.user else { return }
            
            SaveUserSecure.saveID(userID: user.uid)
            UserDefaults.standard.set(user.email!, forKey: "userEmail")
            UserDefaults.standard.set(user.displayName!, forKey: "userFirstName")
            UserDefaults.standard.set("", forKey: "userName")

            DataBaseService.instance.setFcmToken
            
            //check if user number is already save before save user data informations
            DataBaseService.instance.userDocument.getDocument { (snapshot, error) in
                if let snapshot = snapshot {
                    if snapshot.exists {
                        if let data = snapshot.data(){
                            if let numero = data["numero"] as? String {
                                self.connectionLoadView.isHidden = true
                                self.DismissLoginView()
                            }else {
                                NotificationCenter.default.post(name: .didShowUserAddNumberView, object: nil)
                            }
                        }
                    } else {
                        let data:[String:Any] = ["nom":user.displayName!,"prenom":"","email":user.email!]
                        DataBaseService.instance.userDocument.setData(data)
                        DataBaseService.instance.setFcmToken
                        NotificationCenter.default.post(name: .didShowUserAddNumberView, object: nil)
                   }
                }
            }
        }
    }
    
    func SetUpLoginContainer(){
        view.addSubview(containerScollView)
        view.addContraintAllScreen(view: containerScollView)
        
        let dismissTop:CGFloat = 20
        
        var allViewWitdh:CGFloat = 80
        
        var heighBtn:CGFloat = 0
        var titleSize:CGFloat = 0
        var subtitleSize:CGFloat = 14
        var subtitleHeigh:CGFloat = 90
        var conditionTop:CGFloat = 50
        var conditionSize:CGFloat = 14
        switch view.frame.width {
        case 320.0:
            heighBtn = 40
            titleSize = 20
            subtitleHeigh = 60
            subtitleSize = 13
            conditionTop = 30
            conditionSize = 13
            allViewWitdh = 60
        case 375.0 :
            heighBtn = 45
            titleSize = 30
            subtitleSize = 14
            subtitleHeigh = 70
            conditionSize = 13
        case 414.0:
            heighBtn = 50
            titleSize = 30
        default:
            break
        }
        
        
        containerScollView.addSubview(dissMissViewBtn)
        dissMissViewBtn.topAnchor.constraint(equalTo: containerScollView.topAnchor, constant: dismissTop).isActive = true
        dissMissViewBtn.widthAnchor.constraint(equalToConstant: 25).isActive = true
        dissMissViewBtn.heightAnchor.constraint(equalToConstant: 20).isActive = true
        dissMissViewBtn.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 15).isActive = true
        
        
        let connexionTitle = UILabel()
        containerScollView.addSubview(connexionTitle)
        connexionTitle.text = "Connexion"
        connexionTitle.textColor = AppColor.black()
        connexionTitle.textAlignment = .left
        connexionTitle.font = UIFont(name: "Avenir-Black", size: titleSize)
        connexionTitle.translatesAutoresizingMaskIntoConstraints = false
        connexionTitle.topAnchor.constraint(equalTo: dissMissViewBtn.bottomAnchor, constant: 20).isActive = true
        connexionTitle.leftAnchor.constraint(equalTo: containerScollView.leftAnchor, constant: allViewWitdh / 2).isActive = true
        connexionTitle.widthAnchor.constraint(equalTo: containerScollView.widthAnchor).isActive = true
        
        let subTitleTextView: UITextView = UITextView()
        subTitleTextView.text = "Faites votre recherche plus rapidement et plus facilement grâce au profil Kouva"
        subTitleTextView.font = UIFont(name: "Avenir-Medium", size: subtitleSize)
        subTitleTextView.textColor = AppColor.black()
        subTitleTextView.textAlignment = .left
        subTitleTextView.isEditable = false
        subTitleTextView.isScrollEnabled = false
        subTitleTextView.translatesAutoresizingMaskIntoConstraints = false
        containerScollView.addSubview(subTitleTextView)
        subTitleTextView.topAnchor.constraint(equalTo: connexionTitle.bottomAnchor, constant: 5).isActive = true
        subTitleTextView.widthAnchor.constraint(equalTo: containerScollView.widthAnchor, constant: -allViewWitdh).isActive = true
        subTitleTextView.leftAnchor.constraint(equalTo: connexionTitle.leftAnchor).isActive = true
        subTitleTextView.heightAnchor.constraint(equalToConstant: subtitleHeigh).isActive = true
        
        let facebookConnexionBtn = UIButton()
        facebookConnexionBtn.SetTitleBtnAndFont(title: "Facebook", size: 16, titleColor: .white, font: "Avenir-Black")
        facebookConnexionBtn.backgroundColor = UIColor(r: 59, g: 89, b: 152)
        facebookConnexionBtn.addTarget(self, action: #selector(facebookConnexion), for: .touchUpInside)
        facebookConnexionBtn.translatesAutoresizingMaskIntoConstraints = false
        containerScollView.addSubview(facebookConnexionBtn)
        facebookConnexionBtn.topAnchor.constraint(equalTo: subTitleTextView.bottomAnchor, constant: 20).isActive = true
        facebookConnexionBtn.widthAnchor.constraint(equalTo: subTitleTextView.widthAnchor).isActive = true
        facebookConnexionBtn.heightAnchor.constraint(equalToConstant: heighBtn).isActive = true
        facebookConnexionBtn.leftAnchor.constraint(equalTo: connexionTitle.leftAnchor).isActive = true
        facebookConnexionBtn.layer.cornerRadius = 4
        let fbIconView = UIImageView()
        fbIconView.image = #imageLiteral(resourceName: "facebook_icon").withRenderingMode(.alwaysOriginal)
        fbIconView.translatesAutoresizingMaskIntoConstraints = false
        facebookConnexionBtn.addSubview(fbIconView)
        fbIconView.leftAnchor.constraint(equalTo: facebookConnexionBtn.leftAnchor, constant: 20).isActive = true
        fbIconView.centerYAnchor.constraint(equalTo: facebookConnexionBtn.centerYAnchor).isActive = true
        fbIconView.widthAnchor.constraint(equalToConstant: 10).isActive = true
        fbIconView.heightAnchor.constraint(equalToConstant: 18).isActive = true
        
        let gmailConnexionBtn = UIButton()
        gmailConnexionBtn.SetTitleBtnAndFont(title: "Gmail", size: 16, titleColor: UIColor(r: 178, g: 178, b: 178), font: "Avenir-Black")
        gmailConnexionBtn.backgroundColor = UIColor.white
        gmailConnexionBtn.addTarget(self, action: #selector(googleSignConnexion), for: .touchUpInside)
        gmailConnexionBtn.translatesAutoresizingMaskIntoConstraints = false
        containerScollView.addSubview(gmailConnexionBtn)
        gmailConnexionBtn.topAnchor.constraint(equalTo: facebookConnexionBtn.bottomAnchor, constant: 20).isActive = true
        gmailConnexionBtn.widthAnchor.constraint(equalTo: facebookConnexionBtn.widthAnchor, constant: -2).isActive = true
        gmailConnexionBtn.heightAnchor.constraint(equalToConstant: heighBtn).isActive = true
        gmailConnexionBtn.leftAnchor.constraint(equalTo: facebookConnexionBtn.leftAnchor).isActive = true
        gmailConnexionBtn.layer.cornerRadius = 4
        gmailConnexionBtn.layer.shadowOpacity = 0.1
        gmailConnexionBtn.layer.shadowRadius = 4
        gmailConnexionBtn.layer.masksToBounds = false
        gmailConnexionBtn.layer.shadowColor = UIColor.black.cgColor
        gmailConnexionBtn.layer.shadowOffset = CGSize(width: 2, height: 2)
        let googleIconView = UIImageView()
        googleIconView.image = #imageLiteral(resourceName: "google_icon").withRenderingMode(.alwaysOriginal)
        googleIconView.translatesAutoresizingMaskIntoConstraints = false
        gmailConnexionBtn.addSubview(googleIconView)
        googleIconView.leftAnchor.constraint(equalTo: facebookConnexionBtn.leftAnchor, constant: 20).isActive = true
        googleIconView.centerYAnchor.constraint(equalTo: gmailConnexionBtn.centerYAnchor).isActive = true
        googleIconView.widthAnchor.constraint(equalToConstant: 18).isActive = true
        googleIconView.heightAnchor.constraint(equalToConstant: 18).isActive = true
        
        
        containerScollView.addSubview(creationCompteBtn)
        creationCompteBtn.topAnchor.constraint(equalTo: gmailConnexionBtn.bottomAnchor, constant: 20).isActive = true
        creationCompteBtn.widthAnchor.constraint(equalTo: facebookConnexionBtn.widthAnchor).isActive = true
        creationCompteBtn.heightAnchor.constraint(equalToConstant: heighBtn).isActive = true
        creationCompteBtn.leftAnchor.constraint(equalTo: gmailConnexionBtn.leftAnchor).isActive = true
        creationCompteBtn.layer.cornerRadius = 4
        creationCompteBtn.layer.borderColor = AppColor.primaryColor().cgColor
        creationCompteBtn.layer.borderWidth = 0.5
        
        
        let autherTitle = UILabel()
        containerScollView.addSubview(autherTitle)
        autherTitle.text = "ou"
        autherTitle.textColor = UIColor(r: 46, g: 47, b: 65)
        autherTitle.textAlignment = .center
        autherTitle.font = UIFont(name: "Avenir-Medium", size: (view.frame.width == 320.0) ? 14 : 16)
        autherTitle.translatesAutoresizingMaskIntoConstraints = false
        autherTitle.topAnchor.constraint(equalTo: creationCompteBtn.bottomAnchor, constant: 50).isActive = true
        autherTitle.centerXAnchor.constraint(equalTo: creationCompteBtn.centerXAnchor).isActive = true
        autherTitle.widthAnchor.constraint(equalTo: creationCompteBtn.widthAnchor).isActive = true
        
        
        containerScollView.addSubview(textFieldCollection)
        textFieldCollection.topAnchor.constraint(equalTo: autherTitle.bottomAnchor, constant: 40).isActive = true
        textFieldCollection.widthAnchor.constraint(equalTo: facebookConnexionBtn.widthAnchor).isActive = true
        textFieldCollection.heightAnchor.constraint(equalToConstant: view.bounds.height / 4).isActive = true
        textFieldCollection.leftAnchor.constraint(equalTo: creationCompteBtn.leftAnchor).isActive = true
        
        
        containerScollView.addSubview(connexionBtn)
        connexionBtn.topAnchor.constraint(equalTo: textFieldCollection.bottomAnchor, constant: (view.frame.width == 320.0) ? 20 : 10).isActive = true
        connexionBtn.heightAnchor.constraint(equalToConstant: heighBtn).isActive = true
        connexionBtn.widthAnchor.constraint(equalTo: facebookConnexionBtn.widthAnchor).isActive = true
        connexionBtn.leftAnchor.constraint(equalTo: textFieldCollection.leftAnchor).isActive = true
        connexionBtn.layer.cornerRadius = 4
        
        connexionBtn.addSubview(progressView)
        progressView.centerXAnchor.constraint(equalTo: connexionBtn.centerXAnchor).isActive = true
        progressView.centerYAnchor.constraint(equalTo: connexionBtn.centerYAnchor).isActive = true
        
        
        
        let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: "En appuyant sur les boutons de connexion ci-dessus, j’accepte les Termes et conditions de Kouva.")
        attributedString.setAttributes([.link:NSURL(string: "https://kouva.net/terms")!,.foregroundColor: UIColor.blue], range: NSMakeRange(66, 20))
        conditionTextView.attributedText = attributedString
        conditionTextView.delegate = self
        conditionTextView.font = UIFont(name: "Avenir-Medium", size: conditionSize)
        conditionTextView.textColor = UIColor(r: 46, g: 47, b: 65)
        conditionTextView.textAlignment = .left
        conditionTextView.isEditable = false
        conditionTextView.isScrollEnabled = false
        conditionTextView.translatesAutoresizingMaskIntoConstraints = false
        containerScollView.addSubview(conditionTextView)
        conditionTextView.topAnchor.constraint(equalTo: connexionBtn.bottomAnchor, constant: conditionTop).isActive = true
        conditionTextView.widthAnchor.constraint(equalTo: connexionBtn.widthAnchor).isActive = true
        conditionTextView.leftAnchor.constraint(equalTo: connexionTitle.leftAnchor).isActive = true
        conditionTextView.heightAnchor.constraint(equalToConstant: (view.frame.width == 414.0) ? 90 : 80).isActive = true
        
        
        /* LOADING VIEW FOR CONNECTION */
        view.addSubview(connectionLoadView)
        connectionLoadView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        connectionLoadView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        connectionLoadView.heightAnchor.constraint(equalTo: view.heightAnchor).isActive = true
        connectionLoadView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        
        
        // user add number
        view.addSubview(userNumberView)
        view.addContraintAllScreen(view: userNumberView)
        
        let dismissBtn = UIButton()
        dismissBtn.setImage(#imageLiteral(resourceName: "Back Chevron_blue").withRenderingMode(.alwaysOriginal), for: .normal)
        dismissBtn.addTarget(self, action: #selector(hiddenUserNumberView), for: .touchUpInside)
        dismissBtn.translatesAutoresizingMaskIntoConstraints = false
        userNumberView.addSubview(dismissBtn)
        dismissBtn.topAnchor.constraint(equalTo: userNumberView.topAnchor, constant: 60).isActive = true
        dismissBtn.widthAnchor.constraint(equalToConstant: 25).isActive = true
        dismissBtn.heightAnchor.constraint(equalToConstant: 20).isActive = true
        dismissBtn.leftAnchor.constraint(equalTo: userNumberView.leftAnchor, constant: 15).isActive = true
    }
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        print("debug immobilier:",URL)
        self.present(SFSafariViewController(url: URL), animated: true, completion: nil)
        return false
    }
    
    //TODO: notfi error to use when user take time to store code
    // verify credential store in userDefault if it changed
    //save user phone number in database
    @objc func signUserPhoneNumberUsingCodeVerification(_ notification: Notification){
        if let data = notification.userInfo as? [String:Any] {
            if let verificationID = UserDefaults.standard.value(forKey: "verificationID") as? String {
                let credential = PhoneAuthProvider.provider().credential(withVerificationID: verificationID , verificationCode: (data["code"] as! String))
                print("credential:", credential)
                
                Auth.auth().signInAndRetrieveData(with: credential) { (authData, err) in
                    if err != nil {
                        print(err!)
                        Alertes.AlerteError(error: "Le code a expiré. Renvoyer le code de verification", VC: self)
                        return
                    }
                    guard let _ = authData?.user else { return }
                    
                    DataBaseService.instance.setUserPhoneNumber
                    NotificationCenter.default.post(name: .didLoadUserFavoris, object: nil)
                    
                    self.DismissLoginView()
                }
            }
        }
    }
    
    @objc func hiddenUserNumberView(){
        connectionLoadView.isHidden = true
        AuthService.instance.logOut()
        userNumberView.isHidden = true
    }
    
    @objc func ShowCreationCompteVC() {
        let vc = UserCreationController()
        vc.VC = self
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func ResignTextField() {
        let emailIndexPath = IndexPath(item: 1, section: 0)
        if let cell = textFieldCollection.cellForItem(at: emailIndexPath) as? TextFieldCell {
            cell.input.resignFirstResponder()
        }
        let passowrdIndexPath = IndexPath(item: 0, section: 0)
        if let cell = textFieldCollection.cellForItem(at: passowrdIndexPath) as? TextFieldCell {
            cell.input.resignFirstResponder()
        }
    }
    
    @objc func DismissLoginView() {
        NotificationCenter.default.removeObserver(self)
        self.dismiss(animated: true, completion: nil)
        NotificationCenter.default.post(name: .didLoadUserFavoris, object: nil)
        NotificationCenter.default.post(name: .didUserConnexion, object: nil)
    }
}
extension UserLoginViewController: UISearchControllerDelegate {
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        ResignTextField()
    }
}


