//
//  UserProfilController.swift
//  Kouva
//
//  Created by Bio'S on 03/06/2019.
//  Copyright © 2019 Kouva. All rights reserved.
//

import UIKit
import FirebaseAuth
import WebKit
import FBSDKCoreKit
import SafariServices
import MessageUI

import FirebaseDynamicLinks


class UserProfilController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, MFMailComposeViewControllerDelegate, UINavigationControllerDelegate {
    
    let profileMenuCollection: UICollectionView = {
        let layout =  UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 0)
        let collection =  UICollectionView(frame: .zero, collectionViewLayout: layout)
        collection.backgroundColor = UIColor.white
        collection.register(userProfileMenuCell.self, forCellWithReuseIdentifier: "cellMenu")
        collection.register(userProfileHeaderView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "headerId")
        collection.translatesAutoresizingMaskIntoConstraints = false
        collection.showsVerticalScrollIndicator = false
        collection.isScrollEnabled = false
        return collection
    }()
    
    
    //MARK:- lifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
    
        view.backgroundColor = UIColor.white
        self.navigationController?.navigationBar.tintColor = AppColor.black()
        UIApplication.shared.statusBarStyle = .default
        profileMenuCollection.delegate = self
        profileMenuCollection.dataSource = self
        view.addSubview(profileMenuCollection)
        view.addContraintAllScreen(view: profileMenuCollection)
        
        NotificationCenter.default.addObserver(self, selector: #selector(showUserConnectedView), name: .didUserConnexion, object: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellMenu", for: indexPath) as! userProfileMenuCell
        switch indexPath.item {
        case 0:
            cell.label.text = "Termes et Conditions"
            break
        case 1:
            cell.label.text = "Apropos de nous"
            break
        case 2:
            cell.label.text = "Nous contacter"
            break
        case 3:
            cell.label.text = "Partager"
            break
        case 4:
//            cell.separateView.isHidden = true
            cell.label.text = (UserDefaults.standard.value(forKey: "userID") != nil) ? "Deconnexion" : "Connexion"
            
        default:
            break
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        menuListRedirections(item: indexPath.item)
    }
    
    func menuListRedirections(item:Int) {
        switch item {
        case 0:
            guard let url = URL(string: "https://kouva.net/terms") else { return }
            self.present(SFSafariViewController(url: url), animated: true, completion: nil)
            break
            
        case 1:
            self.navigationController?.pushViewController(AproposViewController(collectionViewLayout: UICollectionViewFlowLayout()), animated: true)
            break
        case 2:
            if !MFMailComposeViewController.canSendMail() {
                print("Mail services are not available")
                return
            }
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["info@kouva.net"])
            mail.setBccRecipients(["biosamuel7@gmail.com"])
            mail.setSubject("Nous contacter")
            mail.setMessageBody("Ecrivez votre message ici...", isHTML: false)
            present(mail, animated: true, completion: nil)
            
            break
        case 3:
            
            let activityVC = UIActivityViewController(activityItems: ["https://kouva.page.link/m6ab"], applicationActivities: nil)
            activityVC.popoverPresentationController?.sourceView = self.view
            present(activityVC, animated: true, completion: nil)
        case 4:
            handleUserSession()
            break
        default:
            break
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        if let _ = error {
            controller.dismiss(animated: true)
            return
        }
        
        switch result {
        case .sent:
            print("debug mail:","sent")
        case .cancelled :
            print("debug mail:","cancelled")
        case .failed:
            print("debug mail:","error")
            guard let err = error else { return }
            Alertes.AlerteError(error: err.localizedDescription, VC: self)
        default:
            break
        }
        controller.dismiss(animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width - 50, height: collectionView.frame.width / 5)
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "headerId", for: indexPath) as! userProfileHeaderView
        
        return header
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return (UserDefaults.standard.value(forKey: "userID") != nil) ? CGSize(width: collectionView.frame.width, height: 100) : CGSize.zero
    }
    
    @objc func showUserConnectedView(){
        profileMenuCollection.reloadData()
    }
    //MARK: Connexion handle
    @objc func handleUserSession() {
        if UserDefaults.standard.value(forKey: "userID") != nil {
            AuthService.instance.logOut()
            profileMenuCollection.reloadData()
        }else {
            present(UserLoginViewController(), animated: true, completion: nil)
        }
    }
    
    func DismissView() {
        self.dismiss(animated: true, completion: nil)
    }
    
}

class userProfileHeaderView: UICollectionReusableView {
    let userNameLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.textColor = AppColor.black()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    let emailLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.textColor = AppColor.black()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    
            if let userName = UserDefaults.standard.value(forKey: "userName") as? String,
                let userFirstName = UserDefaults.standard.value(forKey: "userFirstName") as? String,
                let userEmail = UserDefaults.standard.value(forKey: "userEmail") as? String {
                self.emailLabel.text = userEmail
                self.userNameLabel.text = "\(userFirstName) \(userName)"
            }
        
        var titleSize:CGFloat = 0

        switch frame.width {
        case 320:
            titleSize = 25
            break
        case 375 :
            titleSize = 30
        case 414:
            titleSize = 40
        default:
            break
        }

        addSubview(userNameLabel)
        userNameLabel.font = UIFont(name: "Avenir-Black", size: frame.width / 18)
        userNameLabel.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true
        userNameLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 20).isActive = true

        addSubview(emailLabel)
        emailLabel.font = UIFont(name: "Avenir-Roman", size: 15)
        emailLabel.topAnchor.constraint(equalTo: userNameLabel.bottomAnchor, constant: 0).isActive = true
        emailLabel.leftAnchor.constraint(equalTo: userNameLabel.leftAnchor).isActive = true
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

