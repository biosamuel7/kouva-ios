//
//  ListingHomesViewController.swift
//  Kouva
//
//  Created by Bio'S on 03/06/2019.
//  Copyright © 2019 Kouva. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseFirestore

struct TypeDebienModel {
    var possession: String!
    var type:String!
}

class ListingHomesViewController: UIViewController {
    
    var homeType: TypeDebienModel?
    
    var listingHomesCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        //        layout.sectionInset = UIEdgeInsets(top: 10, left: 0, bottom: 50, right: 0)
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 20
        layout.minimumInteritemSpacing = 0
        
        let collection = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        collection.backgroundColor = .white
        collection.translatesAutoresizingMaskIntoConstraints = false
        return collection
    }()
    
    lazy var alerteButton: UIButton = {
        let v = UIButton()
        v.SetTitleBtnAndFont(title: "Créer une alerte")
        v.backgroundColor = AppColor.primaryColor()
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    let listingHomeShimmerView: UIView = {
        let v = UIView()
        v.isHidden = false
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        tabBarController?.tabBar.isHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        tabBarController?.tabBar.isHidden = false
    }
    
    
    //var dataHomes = [HomeListingModel]()
    
    var dataHomes = [HomeModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
//        if #available(iOS 11.0, *) {
//            navigationController?.navigationBar.prefersLargeTitles = false
//            let searBarLocation = UISearchController(searchResultsController: nil)
//            searBarLocation.delegate = self
//            searBarLocation.searchResultsUpdater = self
//            navigationItem.searchController = searBarLocation
//            navigationItem.hidesSearchBarWhenScrolling = false
//        }
        
        //notification to load user wihslist in Favoris activity
        NotificationCenter.default.addObserver(self, selector: #selector(reloadListingHomeToCheckUserWish), name: .didLoadUserFavoris, object: nil)
        
        SetUpConstraintView()
        
        listingHomesCollectionView.delegate = self
        listingHomesCollectionView.dataSource = self
        listingHomesCollectionView.register(FetchHomeWhitInfoCell.self, forCellWithReuseIdentifier: "cell")
        
        listingHomesCollectionView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 60, right: 0)
        
        loadData()
        
    }
    @objc func reloadListingHomeToCheckUserWish(){
        listingHomesCollectionView.reloadData()
    }
    
    func loadData() {

        if let possession = homeType?.possession, let type = homeType?.type {
            DataBaseService.instance.homeQuery(possession: possession, type: type).getDocuments{ (snapshot, error) in
                if let documents = snapshot?.documents {
                    self.dataHomes = []
                    for document in documents {
                        switch type {
                        case "appartement":
                            if let home = HomeModel(appartement: document.data()){
                                self.listingHomeShimmerView.isHidden = true
                                home.homeID = document.documentID
                                if home.stateHome {
                                    self.dataHomes.append(home)
                                }
                            }
                            break
                        case "villa":
                            if let home = HomeModel(villa: document.data()){
                                self.listingHomeShimmerView.isHidden = true
                                home.homeID = document.documentID
                                if home.stateHome {
                                    self.dataHomes.append(home)
                                }
                            }
                            break
                            
                        case "magasin":
                            if let home = HomeModel(magasin: document.data()){
                                self.listingHomeShimmerView.isHidden = true
                                home.homeID = document.documentID
                                if home.stateHome {
                                    self.dataHomes.append(home)
                                }
                            }
                            break
                        case "studio":
                            if let home = HomeModel(studio: document.data()){
                                self.listingHomeShimmerView.isHidden = true
                                home.homeID = document.documentID
                                if home.stateHome {
                                    self.dataHomes.append(home)
                                }
                            }
                            break
                        default: break
                        }
                    }
                    DispatchQueue.main.async {
                           self.listingHomesCollectionView.reloadData()
                        }
                }
            }
        }
    }
    
    
    //like home or deleting
    @objc func LikeHandle(btn: UIButton){
        
        let item = btn.tag
        let indexPath = IndexPath(item: item, section: 0)
        
        let home = dataHomes[indexPath.item]
        if home.isLiked {
            if let cell = listingHomesCollectionView.cellForItem(at: indexPath) as? FetchHomeWhitInfoCell {
                    cell.homeLikeBtn.setImage(#imageLiteral(resourceName: "unlike").withRenderingMode(.alwaysOriginal), for: .normal)
                    home.isLiked = false
                DataBaseService.instance.userWhishCollection.document(home.homeID).delete()
                   
            }
        } else {
            if UserDefaults.standard.value(forKey: "userID") != nil {
                if let cell = listingHomesCollectionView.cellForItem(at: indexPath) as? FetchHomeWhitInfoCell {
                    cell.homeLikeBtn.setImage(#imageLiteral(resourceName: "like").withRenderingMode(.alwaysOriginal), for: .normal)
                    let value:[String:Any] = ["state":"wish","createdAt": FieldValue.serverTimestamp()]
                    DataBaseService.instance.userWhishCollection.document(home.homeID).setData(value)
                    home.isLiked = true
                }
            } else {
                let vc = UserLoginViewController()
                vc.dissMissViewBtn.isHidden = false
                self.present(vc, animated: true, completion: nil)
            }
        }
        
    }
    
    
    
    func getHomePieceIndex(sectionPiece:Int) {
        listingHomesCollectionView.scrollToItem(at: IndexPath(item: 0, section: sectionPiece), at: .top, animated: true)
    }
    
    @objc func showSubmitController(){
        var submitVC = SubmitViewController()
        guard let homeType = homeType else { return }
        submitVC.homeType = TypeDebienModel(possession: homeType.possession, type: homeType.type)
        present(submitVC, animated: true, completion: nil)
    }
    
    func SetUpConstraintView() {
        view.addSubview(listingHomesCollectionView)
        view.addContraintAllScreen(view: listingHomesCollectionView)
        
        
        //// Shimmer view constraints
        view.addSubview(listingHomeShimmerView)
        
        print("debug:",view.frame.width,view.frame.height)
        
        switch (view.frame.width,view.frame.height){
        case (375.0,667.0): // for iphone 6,7,8,S
            listingHomeShimmerView.topAnchor.constraint(equalTo: listingHomesCollectionView.topAnchor, constant: 80).isActive = true
            break
        case (414.0,736.0): // for iphone 6,7,8,S
            listingHomeShimmerView.topAnchor.constraint(equalTo: listingHomesCollectionView.topAnchor, constant: 110).isActive = true
            break
        case (375.0,812.0): // for iphone X,Xs
            listingHomeShimmerView.topAnchor.constraint(equalTo: listingHomesCollectionView.topAnchor, constant: 110).isActive = true
            break
        case (414.0,896.0)://for iphone XS Max
            listingHomeShimmerView.topAnchor.constraint(equalTo: listingHomesCollectionView.topAnchor, constant: 110).isActive = true
            break
        case (320.0,568.0): // for iphone SE
            listingHomeShimmerView.topAnchor.constraint(equalTo: listingHomesCollectionView.topAnchor, constant: 90).isActive = true
            break
        default:
            break
        }
    
        listingHomeShimmerView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        listingHomeShimmerView.heightAnchor.constraint(equalTo: view.heightAnchor).isActive = true
        listingHomeShimmerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        let titleHeigh:CGFloat = 20
        
        let imgShimme1 = UIView()
        imgShimme1.backgroundColor = UIColor(r: 241, g: 241, b: 241)
        imgShimme1.translatesAutoresizingMaskIntoConstraints = false
        listingHomeShimmerView.addSubview(imgShimme1)
        imgShimme1.topAnchor.constraint(equalTo: listingHomeShimmerView.topAnchor, constant: 0).isActive = true
        imgShimme1.centerXAnchor.constraint(equalTo: listingHomeShimmerView.centerXAnchor).isActive = true
        imgShimme1.widthAnchor.constraint(equalTo: listingHomeShimmerView.widthAnchor, constant: -40).isActive = true
        imgShimme1.heightAnchor.constraint(equalToConstant: (view.frame.width / 1.5) - 65).isActive = true
        
        let priceShimmer1 = UIView()
        priceShimmer1.backgroundColor = UIColor(r: 241, g: 241, b: 241)
        priceShimmer1.translatesAutoresizingMaskIntoConstraints = false
        listingHomeShimmerView.addSubview(priceShimmer1)
        priceShimmer1.topAnchor.constraint(equalTo: imgShimme1.bottomAnchor, constant: 5).isActive = true
        priceShimmer1.leftAnchor.constraint(equalTo: imgShimme1.leftAnchor).isActive = true
        priceShimmer1.widthAnchor.constraint(equalTo: imgShimme1.widthAnchor, multiplier: 1/2).isActive = true
        priceShimmer1.heightAnchor.constraint(equalToConstant: titleHeigh).isActive = true
        
        let secteurShimmer1 = UIView()
        secteurShimmer1.backgroundColor = UIColor(r: 241, g: 241, b: 241)
        secteurShimmer1.translatesAutoresizingMaskIntoConstraints = false
        listingHomeShimmerView.addSubview(secteurShimmer1)
        secteurShimmer1.topAnchor.constraint(equalTo: priceShimmer1.bottomAnchor, constant: 5).isActive = true
        secteurShimmer1.leftAnchor.constraint(equalTo: imgShimme1.leftAnchor).isActive = true
        secteurShimmer1.widthAnchor.constraint(equalTo: imgShimme1.widthAnchor, constant: -40).isActive = true
        secteurShimmer1.heightAnchor.constraint(equalToConstant: titleHeigh).isActive = true
        
        let pieceShimmer1 = UIView()
        pieceShimmer1.backgroundColor = UIColor(r: 241, g: 241, b: 241)
        pieceShimmer1.translatesAutoresizingMaskIntoConstraints = false
        listingHomeShimmerView.addSubview(pieceShimmer1)
        pieceShimmer1.topAnchor.constraint(equalTo: secteurShimmer1.bottomAnchor, constant: 5).isActive = true
        pieceShimmer1.leftAnchor.constraint(equalTo: imgShimme1.leftAnchor).isActive = true
        pieceShimmer1.widthAnchor.constraint(equalTo: imgShimme1.widthAnchor, multiplier: 1/4).isActive = true
        pieceShimmer1.heightAnchor.constraint(equalToConstant: titleHeigh).isActive = true
        
        let imgShimme2 = UIView()
        imgShimme2.backgroundColor = UIColor(r: 241, g: 241, b: 241)
        imgShimme2.translatesAutoresizingMaskIntoConstraints = false
        listingHomeShimmerView.addSubview(imgShimme2)
        imgShimme2.topAnchor.constraint(equalTo: pieceShimmer1.bottomAnchor, constant: 10).isActive = true
        imgShimme2.centerXAnchor.constraint(equalTo: listingHomeShimmerView.centerXAnchor).isActive = true
        imgShimme2.widthAnchor.constraint(equalTo: listingHomeShimmerView.widthAnchor, constant: -40).isActive = true
        imgShimme2.heightAnchor.constraint(equalToConstant: (view.frame.width / 1.5) - 65).isActive = true
        
        let priceShimmer2 = UIView()
        priceShimmer2.backgroundColor = UIColor(r: 241, g: 241, b: 241)
        priceShimmer2.translatesAutoresizingMaskIntoConstraints = false
        listingHomeShimmerView.addSubview(priceShimmer2)
        priceShimmer2.topAnchor.constraint(equalTo: imgShimme2.bottomAnchor, constant: 5).isActive = true
        priceShimmer2.leftAnchor.constraint(equalTo: imgShimme2.leftAnchor).isActive = true
        priceShimmer2.widthAnchor.constraint(equalTo: imgShimme2.widthAnchor, multiplier: 1/2).isActive = true
        priceShimmer2.heightAnchor.constraint(equalToConstant: titleHeigh).isActive = true
        
        let secteurShimmer2 = UIView()
        secteurShimmer2.backgroundColor = UIColor(r: 241, g: 241, b: 241)
        secteurShimmer2.translatesAutoresizingMaskIntoConstraints = false
        listingHomeShimmerView.addSubview(secteurShimmer2)
        secteurShimmer2.topAnchor.constraint(equalTo: priceShimmer2.bottomAnchor, constant: 5).isActive = true
        secteurShimmer2.leftAnchor.constraint(equalTo: imgShimme2.leftAnchor).isActive = true
        secteurShimmer2.widthAnchor.constraint(equalTo: imgShimme2.widthAnchor, constant: -40).isActive = true
        secteurShimmer2.heightAnchor.constraint(equalToConstant: titleHeigh).isActive = true
        
        let pieceShimmer2 = UIView()
        pieceShimmer2.backgroundColor = UIColor(r: 241, g: 241, b: 241)
        pieceShimmer2.translatesAutoresizingMaskIntoConstraints = false
        listingHomeShimmerView.addSubview(pieceShimmer2)
        pieceShimmer2.topAnchor.constraint(equalTo: secteurShimmer2.bottomAnchor, constant: 5).isActive = true
        pieceShimmer2.leftAnchor.constraint(equalTo: imgShimme2.leftAnchor).isActive = true
        pieceShimmer2.widthAnchor.constraint(equalTo: imgShimme2.widthAnchor, multiplier: 1/4).isActive = true
        pieceShimmer2.heightAnchor.constraint(equalToConstant: titleHeigh).isActive = true
        
        view.addSubview(alerteButton)
       if #available(iOS 11.0, *) {
           alerteButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -5).isActive = true
       } else {
           alerteButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -10).isActive = true
       }
       alerteButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
       alerteButton.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -60).isActive = true
       alerteButton.heightAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1/8).isActive = true
       alerteButton.layer.cornerRadius = (view.frame.width / 8) / 2
       alerteButton.clipsToBounds =  true
       alerteButton.addTarget(self, action: #selector(showSubmitController), for: .touchUpInside)
               
    }
}

extension ListingHomesViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataHomes.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! FetchHomeWhitInfoCell
        cell.home = dataHomes[indexPath.item]
        cell.homeLikeBtn.tag = indexPath.item
        let home = dataHomes[indexPath.item]
        cell.homeLikeBtn.addTarget(self, action: #selector(LikeHandle(btn:)), for: .touchUpInside)
        DataBaseService.instance.checkIfHomeIsLiked(likeBtn: cell.homeLikeBtn, home: home)
        
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var heigh:CGFloat = collectionView.frame.height / 3
        switch (view.frame.width,view.frame.height){
        case (375.0,667.0): // for iphone 6,7,8,S
            heigh = collectionView.frame.height / 2.5
            break
        case (414.0,736.0): // for iphone 6,7,8,+
            heigh = collectionView.frame.height / 2.5
            break
        case (375.0,812.0): // for iphone X,Xs
            break
        case (414.0,896.0)://for iphone XS Max
            break
        case (320.0,568.0): // for iphone SE
            heigh = collectionView.frame.height / 2.4
            break
        default:
            break
        }
        return CGSize(width: collectionView.frame.width - 40, height: heigh)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        var sectionHeight:CGFloat = 20
        if section == 0 {
            sectionHeight = 0
        }
        return CGSize(width: 100, height: sectionHeight)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let VC = ShowDetailHomeViewController()
        VC.homeID = dataHomes[indexPath.item].homeID
        present(VC, animated: true, completion: {
            collectionView.deselectItem(at: indexPath, animated: false)
        })
    }
}


