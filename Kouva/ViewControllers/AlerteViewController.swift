//
//  AlerteViewController.swift
//  Kouva
//
//  Created by MACBOOK on 26/01/2020.
//  Copyright © 2020 Kouva. All rights reserved.
//

import UIKit

class AlerteViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    let alerteListCollection: UICollectionView = {
        var layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.sectionInset = UIEdgeInsets(top: 30, left: 0, bottom: 0, right: 0)
        layout.minimumLineSpacing = 20
        layout.minimumInteritemSpacing = 0
        var collection = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        collection.register(FetchHomeWhitInfoCell.self, forCellWithReuseIdentifier: "cell")
        collection.register(FavorisHeaderView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "headerId")
        collection.backgroundColor = UIColor.white
        collection.showsHorizontalScrollIndicator = false
        collection.allowsMultipleSelection = true
        collection.translatesAutoresizingMaskIntoConstraints = false
        return collection
    }()
    
    var indicatorView:UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView()
        indicator.activityIndicatorViewStyle = .whiteLarge
        indicator.color = AppColor.primaryColor()
        indicator.startAnimating()
        indicator.translatesAutoresizingMaskIntoConstraints = false
        return indicator
    }()
    
    var homeData:[HomeModel] = []
    
    var handle:UInt?
    var sdd:UInt?
    
//    //MARK: lifeCycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .default
        UIApplication.shared.isStatusBarHidden = false
        tabBarController?.tabBar.isHidden = false

        
        self.ReloadData()
    }
    
    var tabItems:UITabBarItem!
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if UserDefaults.standard.value(forKey: "userID") != nil {
            if let ref = handle {
                //DataBaseService.instance.userWishlistRef.child(AuthService.instance.userID).removeObserver(withHandle: ref)
            }
            if let ref = sdd {
                //DataBaseService.instance.homeListingsRef.removeObserver(withHandle: ref)
            }
        }
        
    }
    
    //todo: add notificationCenter to load user favoris
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = UIColor.white
        navigationController?.navigationBar.tintColor = AppColor.primaryColor()
        view.backgroundColor = UIColor.white
        
        tabItems = self.tabBarController?.tabBar.items?[1]

        //NotificationCenter.default.addObserver(self, selector: #selector(ReloadData), name: .didLoadUserFavoris, object: nil)
        
        SetupConstraints()
        alerteListCollection.delegate = self
        alerteListCollection.dataSource = self
        
    }
    @objc func ReloadData() {
        
        tabItems.badgeValue = nil
        self.homeData = []
        if UserDefaults.standard.value(forKey: "userID") != nil {
            DataBaseService.instance.alerteCollection.addSnapshotListener { (snapshot, error) in
                if let documents = snapshot?.documents{
                    if documents.count > 0 {
                        for document in documents {
                            self.indicatorView.stopAnimating()
                            DataBaseService.instance.homeCollection.document(document.documentID).getDocument { (snapshotHome, error) in
                                guard let homeData = snapshotHome?.data() else { return }
                                if let home = HomeModel(data: homeData) {
                                    home.isLiked = true
                                    home.homeID = document.documentID

                                    if home.stateHome {
                                        self.homeData.append(home)
                                    }
                                
                                }
                                DispatchQueue.main.async {
                                   self.alerteListCollection.reloadData()
                                   self.tabItems.badgeValue = String(self.homeData.count)
                                   
                               }
                                
                            }
                            
                        }

                    } else {
                        self.indicatorView.stopAnimating()
                    }
                }
            }
        
        } else {
            self.indicatorView.stopAnimating()
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return homeData.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! FetchHomeWhitInfoCell
        
        cell.homeLikeBtn.addTarget(self, action: #selector(LikeHandle(btn:)), for: .touchUpInside)
        cell.homeLikeBtn.tag = indexPath.row
        
        let home = homeData[indexPath.row]
        
//        if home.stateHome == "disponible" {
//            cell.homeTakenView.isHidden = true
//        } else {
//            cell.homeTakenView.isHidden = false
//        }
        
        cell.imageHome.loadImageUsingCache(withPath: home.imagePrincipale.path)
        cell.prixLabel.text = home.prix + " CFA"
        cell.secteurLabel.text = "\(home.commune!) - \(home.secteur!)"
        
        switch home.stateLike {
        case "visite":
            cell.homeLikeBtn.setImage(#imageLiteral(resourceName: "visite-home-icon").withRenderingMode(.alwaysOriginal), for: .normal)
            break
        default:
            cell.homeLikeBtn.setImage(#imageLiteral(resourceName: "like").withRenderingMode(.alwaysOriginal), for: .normal)
        }
        switch home.possessionDuBien {
        case "location":
            cell.nombreDPieceLabel.text = "\(home.type!) \(home.piece!) Pieces à louer"
            if home.type == "Studio" {
                cell.nombreDPieceLabel.text = "Studio à louer"
            }
            if home.type == "magasin" {
                cell.nombreDPieceLabel.text = "Magasin à louer"
            }
            break
        case "vente":
            cell.nombreDPieceLabel.text = "\(home.type!) \(home.piece!) Pieces à vendre"
            if home.type == "Studio" {
                cell.nombreDPieceLabel.text = "Studio à vendre"
            }
            if home.type == "Magasin" {
                cell.nombreDPieceLabel.text = "Magasin à vendre"
            }
            break
        default:
            break
        }
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header =  collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "headerId", for: indexPath) as! FavorisHeaderView
        header.enteteTitleLabel.text = "Alertes"
        return header
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width - 60, height: collectionView.frame.height / 3)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 100)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let homeSelected = homeData[indexPath.row]
        if homeSelected.stateHome {
            let VC = ShowDetailHomeViewController()
            VC.homeID = homeSelected.homeID
            present(VC, animated: true, completion: {
                collectionView.deselectItem(at: indexPath, animated: false)
            })
        }
        
    }
    
    @objc func LikeHandle(btn: UIButton) {
        let toItem = btn.tag
        DataBaseService.instance.userWhishCollection.document(homeData[toItem].homeID).delete()
        homeData.remove(at: toItem)
        
        if homeData.isEmpty {
            tabItems.badgeValue = nil
        }else{
            tabItems.badgeValue = String(homeData.count)
        }
        
        alerteListCollection.reloadData()
    }
    
    func SetupConstraints() {
        
        view.addSubview(alerteListCollection)
        view.addContraintAllScreen(view: alerteListCollection)
        
        view.addSubview(indicatorView)
        indicatorView.centerYAnchor.constraint(equalTo: alerteListCollection.centerYAnchor).isActive = true
        indicatorView.centerXAnchor.constraint(equalTo: alerteListCollection.centerXAnchor).isActive = true
    }
    
    
}




