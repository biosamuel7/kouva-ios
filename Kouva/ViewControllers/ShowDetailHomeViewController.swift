//
//  ShowDetailHomeViewController.swift
//  Kouva
//
//  Created by Bio'S on 03/06/2019.
//  Copyright © 2019 Kouva. All rights reserved.
//

import UIKit
import Firebase
import GoogleMaps

class ShowDetailHomeViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIApplicationDelegate {
    
    var homeSelected:HomeModel!
    var homeID: String?
    
    lazy var imageSlideView: SlideImageCollection = {
        let v = SlideImageCollection()
        v.showDetailHomeVC = self
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    var compagnyLogoImgView: UIImageView = {
        let img = UIImageView()
        img.contentMode = .scaleAspectFill
        img.image = #imageLiteral(resourceName: "placeholder")
        img.backgroundColor = .white
        img.clipsToBounds = true
        img.isHidden = true
        img.translatesAutoresizingMaskIntoConstraints = false
        return img
    }()
    var homeLikeBtn: UIButton = {
        let btn = UIButton(type: UIButtonType.system)
        btn.setImage(#imageLiteral(resourceName: "unlike").withRenderingMode(.alwaysOriginal), for: .normal)
        btn.addTarget(self, action: #selector(LikeHandle), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    var shareHomeBtn: UIButton = {
        let btn = UIButton(type: UIButtonType.system)
        btn.setImage(#imageLiteral(resourceName: "share").withRenderingMode(.alwaysOriginal), for: .normal)
        btn.addTarget(self, action: #selector(shareHome), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    lazy var navBarSlideView: NavBarView = {
        let v = NavBarView(frame: self.view.frame)
        v.table = ["INFO","GALLERIE"]
        v.showDetailHomeVC = self
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    let detailHomeCollection: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.scrollDirection = .horizontal
        let collection = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        collection.backgroundColor = UIColor.white
        collection.isPagingEnabled = true
        collection.translatesAutoresizingMaskIntoConstraints = false
        return collection
        
    }()
    
    lazy var infoVillaView: InfoDetailVillaView = {
        var v = InfoDetailVillaView(frame: self.view.frame)
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    lazy var infoAppartementView: InfoDetailAppartementView = {
        let v = InfoDetailAppartementView( frame: self.view.frame)
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    lazy var infoStudioView: InfoDetailStudioView = {
        let v = InfoDetailStudioView(frame: self.view.frame)
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    lazy var infoMagasinView: InfoDetailMagasinView = {
        let v = InfoDetailMagasinView(frame: self.view.frame)
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    lazy var GalleryV: GalleryView = {
        let view = GalleryView()
        view.backgroundColor = UIColor.white
        view.showDetailHomeVC = self
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let surView: UIView = {
        let surView = UIView()
        surView.backgroundColor = UIColor.white
        surView.translatesAutoresizingMaskIntoConstraints = false
        return surView
    }()
    
    lazy var imageZoomBackground: ImagezoomView = {
        let v = ImagezoomView(frame: self.view.frame)
        v.showDetailHomeVC = self
        v.isHidden = true
        v.backgroundColor = UIColor.black
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    let prendreHomeBtn = UIButton()
    
    var imageData:[ImageHome] = []
    
    var urlLink: URL?
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarStyle = .default
        view.backgroundColor = UIColor.white
        
        let value = UIInterfaceOrientation.landscapeLeft.isLandscape.hashValue
        UIDevice.current.setValue(value, forKey: "orientation")
        UIViewController.attemptRotationToDeviceOrientation()
    
        
        detailHomeCollection.register(detailHomeCollectionCell.self, forCellWithReuseIdentifier: "cell")
        detailHomeCollection.delegate = self
        detailHomeCollection.dataSource = self
        
        if let id = homeID {
            DataBaseService.instance.homeCollection.document(id).getDocument { (snapshot, error) in
                if let snapshot = snapshot {
                    if let data = snapshot.data() {
                        guard let type = data["type"] as? String else { return }
                        switch type {
                        case "appartement":
                            if let home = HomeModel(appartement: data){
                                home.homeID = snapshot.documentID
                                self.homeSelected = home
                            }
                            break
                        case "villa":
                            if let home = HomeModel(villa: data){
                                home.homeID = snapshot.documentID
                                self.homeSelected = home
                            }
                            break
                        case "studio":
                            if let home = HomeModel(studio: data){
                                home.homeID = snapshot.documentID
                                self.homeSelected = home
                            }
                            break
                        case "magasin":
                            if let home = HomeModel(magasin: data){
                                home.homeID = snapshot.documentID
                                self.homeSelected = home
                            }
                            break
                        default: break
                        }
                
                        self.SetUpViewConstaints()
                        self.setShareLink()
                    }
                    
                }
            }
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.isStatusBarHidden = true
        UIApplication.shared.statusBarStyle = .lightContent
        
        
        
    }
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return .landscape
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let timer = imageSlideView.timer {
            timer.invalidate()
            imageSlideView.timer = nil
        }
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! detailHomeCollectionCell
        
        if indexPath.item == 0{
            switch homeSelected.type {
            case "studio":
                cell.addSubview(infoStudioView)
                cell.addContraintAllScreen(view: infoStudioView)
                break
            case "magasin":
                cell.addSubview(infoMagasinView)
                cell.addContraintAllScreen(view: infoMagasinView)
                break
            case "appartement":
                cell.addSubview(infoAppartementView)
                cell.addContraintAllScreen(view: infoAppartementView)
                break
            case "villa":
                cell.addSubview(infoVillaView)
                cell.addContraintAllScreen(view: infoVillaView)
                break
            default:
                break
            }
        }
        if indexPath.item == 1{
            cell.addSubview(GalleryV)
            cell.addContraintAllScreen(view: GalleryV)
        }
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    func getImageIndex(index:Int) {
        //                print(index)
        //                imageZoomBackground.isHidden = false
    }
    
    
    func getIndex(index:Int)  {
        let indexItem = IndexPath(item: index, section: 0)
        detailHomeCollection.scrollToItem(at: indexItem, at: .centeredHorizontally, animated: true)
        if index == 0 {
            surView.isHidden = false
        }else {
            surView.isHidden = true
        }
    }
    override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
        imageZoomBackground.imageZoomCollectionHeightConstraint?.constant = self.view.frame.height
        imageZoomBackground.layoutIfNeeded()
        imageZoomBackground.imageZoomCollection.collectionViewLayout.invalidateLayout()
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        navBarSlideView.viewBarHorizontalConstraint?.constant = scrollView.contentOffset.x / 2
    }
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let index = targetContentOffset.pointee.x / view.frame.width
        let indexPath = IndexPath(item: Int(index), section: 0)
        navBarSlideView.navigationCollection.selectItem(at: indexPath, animated: true, scrollPosition: UICollectionViewScrollPosition())
        
        if index == 0{
            surView.isHidden = false
        }else {
            surView.isHidden = true
        }
    }
    
    func setShareLink() {
        guard let link = URL(string: "https://kouva.net/search/home?id="+homeSelected.homeID) else { return }
         let dynamicLinksDomainURIPrefix = "https://kouva.page.link"
         
         guard let linkBuilder = DynamicLinkComponents(link: link, domainURIPrefix: dynamicLinksDomainURIPrefix) else {return}
         
         linkBuilder.iOSParameters = DynamicLinkIOSParameters(bundleID: "com.wedevnow.kouva")
         linkBuilder.iOSParameters?.appStoreID = "1469657479"
         linkBuilder.iOSParameters?.minimumAppVersion = "1.0.2"
         
         linkBuilder.androidParameters = DynamicLinkAndroidParameters(packageName: "com.wedevnow.kouva")
         
         linkBuilder.socialMetaTagParameters = DynamicLinkSocialMetaTagParameters()
         linkBuilder.socialMetaTagParameters?.title = homeSelected.type
         switch homeSelected.possessionDuBien {
         case "location":
             if homeSelected.piece == 1 {
                 linkBuilder.socialMetaTagParameters?.descriptionText = "à louer \(homeSelected.commune!) \(homeSelected.secteur!) à \(homeSelected.prix!)"
             } else {
                 linkBuilder.socialMetaTagParameters?.descriptionText = "\(homeSelected.piece!) pièces à louer \(homeSelected.commune!) \(homeSelected.secteur!) à \(homeSelected.prix!)"
             }
         case "vente":
             if homeSelected.piece == 1 {
                 linkBuilder.socialMetaTagParameters?.descriptionText = "à vendre \(homeSelected.commune!) \(homeSelected.secteur!) à \(homeSelected.prix!)"
             } else {
                 linkBuilder.socialMetaTagParameters?.descriptionText = "\(homeSelected.piece!) pièces à vendre \(homeSelected.commune!) \(homeSelected.secteur!) à \(homeSelected.prix!)"
             }
         default:
             break
         }
         
         linkBuilder.socialMetaTagParameters?.imageURL = URL(string: homeSelected.imagePrincipale.downloadURL)
        
         linkBuilder.shorten { (url, wasr, err) in
             guard let url = url else { return }
             print("link",url)
            self.urlLink = url
         }
    }
    @objc func shareHome(){
        guard let url = urlLink else { return }
        let activityVC = UIActivityViewController(activityItems: [url], applicationActivities: nil)
        activityVC.popoverPresentationController?.sourceView = self.view
        self.present(activityVC, animated: true, completion: nil)
    }
    
    @objc func LikeHandle() {
        if homeSelected.isLiked {
            homeLikeBtn.setImage(#imageLiteral(resourceName: "unlike").withRenderingMode(.alwaysOriginal), for: .normal)
            DataBaseService.instance.userWhishCollection.document(homeSelected.homeID).delete()
            self.homeSelected.isLiked = false
        } else {
            if UserDefaults.standard.value(forKey: "userID") != nil {
                homeLikeBtn.setImage(#imageLiteral(resourceName: "like").withRenderingMode(.alwaysOriginal), for: .normal)
                DataBaseService.instance.userWhishCollection.document(self.homeSelected.homeID).setData(["state":"wish","createdAt": FieldValue.serverTimestamp()]) { (error) in
                    self.homeSelected.isLiked = true
                }
            } else {
                let vc = UserLoginViewController()
                vc.dissMissViewBtn.isHidden = false
                self.present(vc, animated: true, completion: nil)
            }
        }
        
    }
    
    @objc func DismissView() {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func SetUpViewConstaints()  {
        
        DataBaseService.instance.checkIfHomeIsLiked(likeBtn: homeLikeBtn, home: homeSelected)
        // SET INFO IN DIFFERENTES TYPE OF VIEWS
        
        switch homeSelected.type {
        case "villa":
            infoVillaView.setInfo(home: homeSelected)
            break
        case "appartement":
            infoAppartementView.setInfo(home: homeSelected)
            break
        case "studio":
            infoStudioView.setInfo(home: homeSelected)
            break
        case "magasin":
            infoMagasinView.setInfo(home: homeSelected)
            break
        default:
            break
        }
        
        compagnyLogoImgView.loadImageUsingCacheWithUrlString2(urlProfile: "https://kouva.net/images/isis.png")
        
        /// SET UP IMAGE PRINCIPALE AND GALLERY
        imageSlideView.data = homeSelected.imageGallerie
        imageZoomBackground.data = homeSelected.imageGallerie
        GalleryV.data = homeSelected.imageGallerie
        imageData = homeSelected.imageGallerie
        view.addSubview(imageSlideView)
        imageSlideView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        if view.frame.height == 896.0 || view.frame.height == 812.0 {
            imageSlideView.heightAnchor.constraint(equalToConstant: view.frame.height / 3.5).isActive = true
        } else {
            imageSlideView.heightAnchor.constraint(equalToConstant: view.frame.height / 2.6).isActive = true
        }
        imageSlideView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        imageSlideView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        
        view.addSubview(compagnyLogoImgView)
        compagnyLogoImgView.bottomAnchor.constraint(equalTo: imageSlideView.bottomAnchor, constant: -5).isActive = true
        compagnyLogoImgView.leftAnchor.constraint(equalTo: imageSlideView.leftAnchor, constant: 10).isActive = true
        compagnyLogoImgView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        compagnyLogoImgView.widthAnchor.constraint(equalTo: imageSlideView.widthAnchor, multiplier: 1/4).isActive = true
        
        
        let dissMissViewBtn = UIButton(type: .system)
        dissMissViewBtn.setImage(#imageLiteral(resourceName: "Back Chevron").withRenderingMode(.alwaysOriginal), for: .normal)
        dissMissViewBtn.addTarget(self, action: #selector(DismissView), for: .touchUpInside)
        dissMissViewBtn.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(dissMissViewBtn)
        dissMissViewBtn.widthAnchor.constraint(equalToConstant: 25).isActive = true
        dissMissViewBtn.heightAnchor.constraint(equalToConstant: 20).isActive = true
        dissMissViewBtn.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 15).isActive = true
        dissMissViewBtn.topAnchor.constraint(equalTo: view.topAnchor, constant: 40).isActive = true
        
        
        view.addSubview(homeLikeBtn)
        homeLikeBtn.topAnchor.constraint(equalTo: dissMissViewBtn.topAnchor, constant: 0).isActive = true
        homeLikeBtn.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -10).isActive = true
        homeLikeBtn.widthAnchor.constraint(equalToConstant: 35).isActive = true
        homeLikeBtn.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        view.addSubview(shareHomeBtn)
        shareHomeBtn.topAnchor.constraint(equalTo: homeLikeBtn.topAnchor).isActive = true
        shareHomeBtn.rightAnchor.constraint(equalTo: homeLikeBtn.leftAnchor, constant: -15).isActive = true
        shareHomeBtn.widthAnchor.constraint(equalToConstant: 35).isActive = true
        shareHomeBtn.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        view.addSubview(navBarSlideView)
        navBarSlideView.topAnchor.constraint(equalTo: imageSlideView.bottomAnchor, constant: 0).isActive = true
        navBarSlideView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        navBarSlideView.heightAnchor.constraint(equalToConstant: view.frame.width / 8).isActive = true
        navBarSlideView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        view.addSubview(detailHomeCollection)
        
        let detailHeigh:CGFloat = (view.frame.height) - (view.frame.width / 8) - (view.frame.height / 3) + 35
        detailHomeCollection.topAnchor.constraint(equalTo: navBarSlideView.bottomAnchor).isActive = true
        detailHomeCollection.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        detailHomeCollection.heightAnchor.constraint(equalToConstant: detailHeigh).isActive = true
        detailHomeCollection.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        
        view.addSubview(surView)
        switch view.frame.height {
        case 812.0:
            surView.heightAnchor.constraint(equalToConstant: (view.frame.width / 5) + 20).isActive = true
            surView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
            break
        default:
            surView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
            surView.heightAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1/5).isActive = true
            break
        }
        
        surView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        surView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        let barSurView = UIView()
        barSurView.backgroundColor = UIColor.lightGray
        barSurView.translatesAutoresizingMaskIntoConstraints = false
        surView.addSubview(barSurView)
        barSurView.topAnchor.constraint(equalTo: surView.topAnchor).isActive = true
        barSurView.widthAnchor.constraint(equalTo: surView.widthAnchor).isActive = true
        barSurView.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
        barSurView.centerXAnchor.constraint(equalTo: surView.centerXAnchor).isActive = true
        
        let priceHomeLabel = UILabel()
        priceHomeLabel.font = UIFont(name: "Avenir-Black", size: 22)
        priceHomeLabel.text = "360 000 F"
        priceHomeLabel.textColor = AppColor.green()
        priceHomeLabel.textAlignment = .left
        priceHomeLabel.translatesAutoresizingMaskIntoConstraints = false
        surView.addSubview(priceHomeLabel)
        priceHomeLabel.topAnchor.constraint(equalTo: surView.topAnchor, constant: 10).isActive = true
        priceHomeLabel.leftAnchor.constraint(equalTo: surView.leftAnchor, constant: 15).isActive = true
        priceHomeLabel.heightAnchor.constraint(equalToConstant: 24).isActive = true
        
        let dureLabel = UILabel()
        dureLabel.font = UIFont(name: "Avenir-Heavy", size: 14)
        dureLabel.text = "par mois"
        dureLabel.textColor = AppColor.ClearColor()
        dureLabel.textAlignment = .left
        dureLabel.translatesAutoresizingMaskIntoConstraints = false
        surView.addSubview(dureLabel)
        //        let dureLabelTopConstraint =  dureLabel.topAnchor.constraint(equalTo: surView.topAnchor, constant: 10)
        //        dureLabelTopConstraint.isActive = true
        dureLabel.leftAnchor.constraint(equalTo: priceHomeLabel.rightAnchor, constant: 5).isActive = true
        dureLabel.centerYAnchor.constraint(equalTo: priceHomeLabel.centerYAnchor).isActive = true
        dureLabel.heightAnchor.constraint(equalToConstant: 16).isActive = true
        
        
        let secteurHomeLabel = UILabel()
        secteurHomeLabel.font = UIFont(name: "Avenir-Light", size: 12)
        secteurHomeLabel.text = "4 Pieces"
        secteurHomeLabel.text = homeSelected.commune + " - " + homeSelected.secteur
        secteurHomeLabel.textColor = AppColor.ClearColor()
        secteurHomeLabel.textAlignment = .left
        secteurHomeLabel.translatesAutoresizingMaskIntoConstraints = false
        surView.addSubview(secteurHomeLabel)
        let secteurHomeLabelTopConstraint = secteurHomeLabel.topAnchor.constraint(equalTo: priceHomeLabel.bottomAnchor, constant: 5)
        secteurHomeLabelTopConstraint.isActive = true
        secteurHomeLabel.leftAnchor.constraint(equalTo: surView.leftAnchor, constant: 15).isActive = true
        secteurHomeLabel.heightAnchor.constraint(equalToConstant: 13).isActive = true
        
        let nombrePieceHomeLabel = UILabel()
        nombrePieceHomeLabel.font = UIFont(name: "Avenir-Heavy", size: 14)
        nombrePieceHomeLabel.text = "4 Pieces"
        nombrePieceHomeLabel.text = "\(homeSelected.piece!) Pieces"
        nombrePieceHomeLabel.textColor = AppColor.ClearColor()
        nombrePieceHomeLabel.textAlignment = .left
        nombrePieceHomeLabel.translatesAutoresizingMaskIntoConstraints = false
        surView.addSubview(nombrePieceHomeLabel)
        let nombrePieceHomeTopConstraint =  nombrePieceHomeLabel.topAnchor.constraint(equalTo: secteurHomeLabel.bottomAnchor, constant: 5)
        nombrePieceHomeTopConstraint.isActive = true
        nombrePieceHomeLabel.leftAnchor.constraint(equalTo: surView.leftAnchor, constant: 15).isActive = true
        nombrePieceHomeLabel.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        
        switch view.frame.width {
        case 320:
            priceHomeLabel.font = UIFont(name: "Avenir-Black", size: 17)
            dureLabel.font = UIFont(name: "Avenir-Heavy", size: 12)
            secteurHomeLabel.font = UIFont(name: "Avenir-Light", size: 10)
            nombrePieceHomeLabel.font = UIFont(name: "Avenir-Heavy", size: 10)
            secteurHomeLabelTopConstraint.constant = 0
            nombrePieceHomeTopConstraint.constant = 0
            
            if homeSelected.prix.characters.count >= 9 {
                dureLabel.text = "/ mois"
            }
            break
        case 375:
            if homeSelected.prix.characters.count >= 9 {
                dureLabel.text = "/ mois"
            }
            break
        case 414:
            
            break
        default:
            break
        }
        
        priceHomeLabel.text = homeSelected.prix + " CFA"
        
        switch homeSelected.possessionDuBien {
        case "location":
            nombrePieceHomeLabel.text = "\(homeSelected.type!.uppercaseFirstCaracters()) \(homeSelected.piece!) pièces" + " à louer"
            if homeSelected.piece == 1 {
                nombrePieceHomeLabel.text = homeSelected.type.uppercaseFirstCaracters() + " à louer"
            }
            break
        case "vente":
            nombrePieceHomeLabel.text = "\(homeSelected.type!.uppercaseFirstCaracters()) \(homeSelected.piece!) pièces à vendre"
            if homeSelected.piece == 1 {
                nombrePieceHomeLabel.text = homeSelected.type.uppercaseFirstCaracters() + " à vendre"
            }
            break
        default:
            break
        }
        
        if homeSelected.possessionDuBien == "vente" {
            dureLabel.removeFromSuperview()
        }
        
        
        prendreHomeBtn.SetTitleBtnAndFont(title: "Contacter")
        prendreHomeBtn.backgroundColor = AppColor.primaryColor()
        prendreHomeBtn.translatesAutoresizingMaskIntoConstraints = false
        prendreHomeBtn.addTarget(self, action: #selector(VisiterLeBien), for: .touchUpInside)
        surView.addSubview(prendreHomeBtn)
        prendreHomeBtn.rightAnchor.constraint(equalTo: surView.rightAnchor, constant: -10).isActive = true
        switch view.frame.height {
        case 812.0:
            prendreHomeBtn.heightAnchor.constraint(equalTo: surView.heightAnchor, constant: -40).isActive = true
            break
        default:
            prendreHomeBtn.heightAnchor.constraint(equalTo: surView.heightAnchor, constant: -20).isActive = true
            break
        }
        
        prendreHomeBtn.widthAnchor.constraint(equalToConstant: view.frame.width / 2.5).isActive = true
        prendreHomeBtn.centerYAnchor.constraint(equalTo: surView.centerYAnchor, constant: 0).isActive = true
        prendreHomeBtn.layer.cornerRadius = 5
        
        
        view.addSubview(imageZoomBackground)
        view.addContraintAllScreen(view: imageZoomBackground)
    }
    
    @objc func VisiterLeBien() {
        
        if UserDefaults.standard.value(forKey: "userID") != nil {
            homeSelected.isLiked = true
            continuerLaRecher()
        } else {
            let loginVC = UserLoginViewController()
            present(loginVC, animated: true, completion: nil)
        }
    }
    
    func continuerLaRecher() {
        guard let id = homeID else { return }
        DataBaseService.instance.userCall(homeId: id)
        if let numero = self.homeSelected.numero {
            if let phoneCallURL = URL(string: "telprompt://\(numero)") {
                let application:UIApplication = UIApplication.shared
                if (application.canOpenURL(phoneCallURL)) {
                    if #available(iOS 10.0, *) {
                        application.open(phoneCallURL, options: [:], completionHandler: nil)
                    } else {
                        // Fallback on earlier versions
                        application.openURL(phoneCallURL as URL)
                    }
                }
            }
        }
    }
    
}


