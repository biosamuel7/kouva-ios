//
//  FilterViewController.swift
//  Kouva
//
//  Created by Bio'S on 03/06/2019.
//  Copyright © 2019 Kouva. All rights reserved.
//

//import UIKit
//import FirebaseDatabase
//
//class FilterViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITextFieldDelegate {
//    
//    var textFieldCollection: UICollectionView = {
//        var layout = UICollectionViewFlowLayout()
//        layout.scrollDirection = .vertical
//        layout.sectionInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
//        layout.minimumLineSpacing = 3
//        layout.minimumInteritemSpacing = 1
//        var collection = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
//        collection.register(TextFieldCell.self, forCellWithReuseIdentifier: "cell")
//        collection.backgroundColor = UIColor.white
//        collection.isScrollEnabled = false
//        collection.translatesAutoresizingMaskIntoConstraints = false
//        return collection
//    }()
//    let searchBtn: UIButton = {
//        let btn = UIButton()
//        btn.setTitle("Suivant", for: .normal)
//        btn.backgroundColor = primaryColor
//        btn.translatesAutoresizingMaskIntoConstraints = false
//        return btn
//    }()
//    
//    let scroolView: UIScrollView = {
//        let view = UIScrollView()
//        view.backgroundColor = UIColor.white
//        view.translatesAutoresizingMaskIntoConstraints = false
//        return view
//    }()
//    
//    let BackgroundView: UIView = {
//        let v = UIView()
//        v.backgroundColor = UIColor.black
//        v.alpha = 0.2
//        v.isHidden = true
//        v.translatesAutoresizingMaskIntoConstraints = false
//        return v
//    }()
//    var PossessionDuBienView: FilterTypeDeBienView = {
//        let view = FilterTypeDeBienView()
//        view.backgroundColor = UIColor.white
//        view.Table = ["Louer","Acheter"]
//        view.toItem = 0
//        let index = IndexPath(item: 0, section: 0)
//        view.collectionView.selectItem(at: index, animated: false, scrollPosition: .centeredHorizontally)
//        view.translatesAutoresizingMaskIntoConstraints = false
//        return view
//    }()
//    var TypeDeBienView: FilterTypeDeBienView = {
//        let view = FilterTypeDeBienView()
//        view.toItem = 1
//        view.backgroundColor = UIColor.white
//        view.translatesAutoresizingMaskIntoConstraints = false
//        return view
//    }()
//    lazy var ComoditeView: FilterComoditeView = {
//        let view = FilterComoditeView(frame: self.view.frame)
//        view.backgroundColor = UIColor.white
//        view.translatesAutoresizingMaskIntoConstraints = false
//        return view
//    }()
//    lazy var CompteurChooseV: CompteurChooseView = {
//        let v = CompteurChooseView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height / 5))
//        v.backgroundColor = UIColor.white
//        v.translatesAutoresizingMaskIntoConstraints = false
//        return v
//    }()
//    var BudgetChooseV: FilterBudgetView = {
//        let view = FilterBudgetView()
//        view.backgroundColor = UIColor.white
//        view.translatesAutoresizingMaskIntoConstraints = false
//        return view
//    }()
//    
//    var progressView: UIActivityIndicatorView = {
//        let progress = UIActivityIndicatorView()
//        progress.color = UIColor.white
//        progress.translatesAutoresizingMaskIntoConstraints = false
//        return progress
//    }()
//    
//    var PossessionDuBienY:NSLayoutConstraint?
//    var TypeDeBienY:NSLayoutConstraint?
//    var ComoditeViewY:NSLayoutConstraint?
//    var CompteurChooseVY:NSLayoutConstraint?
//    var BudgetChooseVY:NSLayoutConstraint?
//    
//    var AllViewChooseY:CGFloat!
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        view.backgroundColor = primaryColor
//        UIApplication.shared.statusBarStyle = .lightContent
//        scroolView.backgroundColor = primaryColor
//        
//        view.addSubview(scroolView)
//        view.addContraintAllScreen(view: scroolView)
//        
//        var scrolHeigh:CGFloat = 0
//        switch view.frame.width {
//        case 320:
//            scrolHeigh = 830
//            break
//        case 375:
//            scrolHeigh = 860
//            break
//        case 414:
//            scrolHeigh = 880
//            break
//        default:
//            break
//        }
//        
//        AllViewChooseY = view.frame.height
//        
//        scroolView.contentSize = CGSize(width: view.frame.width, height: scrolHeigh)
//        
//        SetUpConstraints()
//        
//        textFieldCollection.delegate = self
//        textFieldCollection.dataSource = self
//        
//        SetUpChooseView()
//        
//        BackgroundView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(HiddenView)))
//        
//    }
//    func numberOfSections(in collectionView: UICollectionView) -> Int {
//        return 1
//    }
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return 6
//    }
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! TextFieldCell
//        
//        cell.input.delegate = self
//        cell.label.textColor = titleGrayColor
//        cell.input.textColor = titleGrayMediumColor
//        cell.viewBar.backgroundColor = UIColor.lightGray
//        cell.input.tag = indexPath.row
//        //  cell.input.delegate = self
//        
//        switch indexPath.row {
//        case 0:
//            cell.label.text = "Bien à"
//            cell.input.text = "Location"
//            cell.input.returnKeyType = .continue
//            break
//        case 1:
//            cell.label.text = "Type de biens"
//            cell.input.placeholder = "Choisissez le type OBLIGATOIRE"
//            cell.input.returnKeyType = .continue
//            break
//        case 2:
//            cell.label.text = "Lieu"
//            cell.input.placeholder = "Choisissez le lieu"
//            cell.input.returnKeyType = .continue
//            break
//        case 3:
//            cell.label.text = "Nombre de pieces OBLIGATOIRE"
//            cell.input.text = "3"
//            cell.input.returnKeyType = .continue
//            break
//        case 4:
//            cell.label.text = "Les comodites"
//            cell.input.placeholder = "Selectionner toutes les comodites possibles"
//            break
//            
//        case 5:
//            cell.label.text = "Votre budget"
//            cell.input.placeholder = "Quelle est votre budget ? OBLIGATOIRE"
//            cell.input.keyboardType = .numberPad
//            cell.input.returnKeyType = .continue
//            break
//            
//        default:
//            break
//        }
//        
//        return cell
//    }
//    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        return CGSize(width: collectionView.frame.width - (2 * 15) - 10, height: 100)
//    }
//    @objc func DismissView() {
//        self.dismiss(animated: true, completion: nil)
//    }
//    func textFieldDidBeginEditing(_ textField: UITextField) {
//        textField.resignFirstResponder()
//        BackgroundView.isHidden = false
//        
//        if textField.tag == 0 {
//            self.PossessionDuBienY?.constant = 0
//            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
//                self.view.layoutIfNeeded()
//                self.PossessionDuBienView.VC = self
//            }, completion: nil)
//        }
//        
//        if textField.tag == 1 {
//            self.TypeDeBienY?.constant = 0
//            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
//                self.view.layoutIfNeeded()
//                self.TypeDeBienView.VC = self
//            }, completion: nil)
//        }
//        
//        //set value for nombre of room and toilet ....
//        SetValueToPieceCompte(textField: textField, tag: 3)
//        
//        if textField.tag == 4 {
//            self.ComoditeViewY?.constant = 0
//            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
//                self.view.layoutIfNeeded()
//                self.ComoditeView.VC = self
//            }, completion: nil)
//        }
//        
//        if textField.tag == 5 {
//            self.BudgetChooseVY?.constant = 0
//            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
//                self.view.layoutIfNeeded()
//                self.BudgetChooseV.VC = self
//            }, completion: nil)
//            
//        }
//        
//    }
//    @objc func HiddenView() {
//        
//        PossessionDuBienY?.constant = AllViewChooseY
//        TypeDeBienY?.constant = AllViewChooseY
//        CompteurChooseVY?.constant = AllViewChooseY
//        ComoditeViewY?.constant = AllViewChooseY
//        BudgetChooseVY?.constant = AllViewChooseY
//        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
//            self.view.layoutIfNeeded()
//            self.BackgroundView.isHidden = true
//        }, completion: nil)
//    }
//    
//    func SetValueToPieceCompte(textField:UITextField, tag:Int) {
//        if textField.tag == tag {
//            self.CompteurChooseVY?.constant = 0
//            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
//                self.view.layoutIfNeeded()
//                self.CompteurChooseV.VC = self
//                self.CompteurChooseV.toItem = tag
//                let indexPath = IndexPath(item: tag, section: 0)
//                if let cell = self.textFieldCollection.cellForItem(at: indexPath) as? TextFieldCell {
//                    if let value = cell.input.text {
//                        self.CompteurChooseV.SetCountValue(value: value)
//                    }
//                }
//            }, completion: nil)
//            
//        }
//    }
//    
//    //MARK: Set up all view choose filter
//    func SetUpChooseView() {
//        
//        view.addSubview(BackgroundView)
//        view.addContraintAllScreen(view: BackgroundView)
//        
//        view.addSubview(PossessionDuBienView)
//        PossessionDuBienY = PossessionDuBienView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: AllViewChooseY)
//        PossessionDuBienY?.isActive = true
//        PossessionDuBienView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
//        PossessionDuBienView.heightAnchor.constraint(equalToConstant: 150).isActive = true
//        PossessionDuBienView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
//        
//        view.addSubview(TypeDeBienView)
//        TypeDeBienY = TypeDeBienView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: AllViewChooseY)
//        TypeDeBienY?.isActive = true
//        TypeDeBienView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
//        TypeDeBienView.heightAnchor.constraint(equalToConstant: view.frame.height / 2).isActive = true
//        TypeDeBienView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
//        
//        view.addSubview(ComoditeView)
//        ComoditeViewY = ComoditeView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: AllViewChooseY)
//        ComoditeViewY?.isActive = true
//        ComoditeView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
//        ComoditeView.heightAnchor.constraint(equalToConstant: view.frame.height).isActive = true
//        ComoditeView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
//        ComoditeView.valideComodite.addTarget(self, action: #selector(HiddenView), for: .touchUpInside)
//        
//        view.addSubview(CompteurChooseV)
//        CompteurChooseVY = CompteurChooseV.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: AllViewChooseY)
//        CompteurChooseVY?.isActive = true
//        CompteurChooseV.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
//        CompteurChooseV.heightAnchor.constraint(equalToConstant: view.frame.height / 5).isActive = true
//        CompteurChooseV.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
//        
//        view.addSubview(BudgetChooseV)
//        BudgetChooseVY = BudgetChooseV.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: AllViewChooseY)
//        BudgetChooseVY?.isActive = true
//        BudgetChooseV.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
//        BudgetChooseV.heightAnchor.constraint(equalToConstant: view.frame.height / 2).isActive = true
//        BudgetChooseV.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
//        
//    }
//    @objc func MakeResearchFunc() {
//        var possession:String!
//        var typeDeBien:String?
//        var piece:String?
//        var budget:String?
//        
//        if let cell = textFieldCollection.cellForItem(at: IndexPath(item: 0, section: 0)) as? TextFieldCell {
//            if let value = cell.input.text {
//                possession = value
//            }
//        }
//        if let cell = textFieldCollection.cellForItem(at: IndexPath(item: 1, section: 0)) as? TextFieldCell {
//            if let value = cell.input.text {
//                typeDeBien = value
//            }
//        }
//        
//        if let cell = textFieldCollection.cellForItem(at: IndexPath(item: 3, section: 0)) as? TextFieldCell {
//            if let value = cell.input.text {
//                piece = value
//            }
//        }
//        
//        if let cell = textFieldCollection.cellForItem(at: IndexPath(item: 5, section: 0)) as? TextFieldCell {
//            if let value = cell.input.text {
//                budget = value
//            }
//        }
//        
//        if !typeDeBien!.isEmpty && !piece!.isEmpty && !budget!.isEmpty {
//            //            progressView.startAnimating()
//            //            searchBtn.SetTitleBtnAndFont(title: "  ")
//            //            var dataHome = [HomeModel]()
//            //            DataBase.instance
//            //                .homeListingsRef
//            //                .child(possession)
//            //                .child(typeDeBien!)
//            //                .queryOrdered(byChild: "piece")
//            //                .queryEqual(toValue: piece!)
//            //                .observe(.value, with: { (snapshot) in
//            //                    if snapshot.exists() {
//            //                        for item in snapshot.children {
//            //                            let datas = item as! FIRDataSnapshot
//            //                            let homeData = datas.value as! [String:Any]
//            //                            if let homes = HomeModel(data: homeData){
//            //                                //                            let prix = Int(homes.prix)
//            //                                let prix = Int(homes.prix.components(separatedBy: .whitespaces).joined())
//            //                                if 240000 >= prix! || prix!  <= 300000 {
//            //                                    homes.homeID = datas.ref.key
//            //                                    homes.type = typeDeBien!
//            //                                    homes.possessionDuBien = possession!
//            //                                    dataHome.append(homes)
//            //                                }
//            //                                print(dataHome.count)
//            //
//            //                            }
//            //                        }
//            //                        DispatchQueue.main.async {
//            //                            self.textFieldCollection.reloadData()
//            //                        }
//            //                    }
//            //
//            //                })
//            //            let vc = FetchHomeVC()
//            //            self.navigationController?.pushViewController(vc, animated: true)
//        } else {
//            Alertes.AlerteError(error: "Remplissez les champs Obligatoires", VC: self)
//        }
//    }
//}


