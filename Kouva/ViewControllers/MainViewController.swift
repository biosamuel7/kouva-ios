//
//  MainViewController.swift
//  Kouva
//
//  Created by Bio'S on 03/06/2019.
//  Copyright © 2019 Kouva. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    
    
    let mainCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        
        let collection = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        collection.backgroundColor = .white
        collection.translatesAutoresizingMaskIntoConstraints = false
        return collection
    }()
    
    lazy var oasView: OASView = {
        let v = OASView(frame: self.view.frame)
        v.mainViewController = self
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    var homeOASData = [HomeModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.white
        self.navigationController?.navigationBar.tintColor = AppColor.primaryColor()
        UIApplication.shared.statusBarStyle = .default
        
        
        SetUpConstraintView()
        
        mainCollectionView.register(mainCollectionCell.self, forCellWithReuseIdentifier: "cell")
        mainCollectionView.register(LocationHeaderView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "header")
        mainCollectionView.delegate = self
        mainCollectionView.dataSource = self
        
    }
    
    
    func SetUpConstraintView() {
        view.addSubview(mainCollectionView)
        view.addContraintAllScreen(view: mainCollectionView)
        
        let filteView = UIView()
        filteView.backgroundColor = AppColor.primaryColor()
        filteView.isHidden = true
        filteView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(filteView)
        var topFilter:CGFloat  = -60
        if view.frame.height == 812.0{
            topFilter = -100
        }
        filteView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: topFilter).isActive = true
        filteView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        filteView.widthAnchor.constraint(equalToConstant: view.frame.width / 2.5).isActive = true
        filteView.heightAnchor.constraint(equalToConstant: (view.frame.width / 4) / 2).isActive = true
        filteView.layer.cornerRadius = ((view.frame.width / 4) / 2) / 2
    }
}

extension MainViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 3
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! mainCollectionCell
        cell.mainViewController = self
        
        switch indexPath.section {
        case 0:
            cell.dataSection = Constants.LOCATION_BIEN_TABLE
            cell.sectionPosition = indexPath.section
            cell.sectionCollection.selectItem(at: IndexPath(item: 0, section: 0), animated: true, scrollPosition: .left)
        case 1:
            cell.dataSection = Constants.VENTE_BIEN_TABLE
            cell.sectionPosition = indexPath.section
            cell.sectionCollection.selectItem(at: IndexPath(item: 0, section: 0), animated: true, scrollPosition: .left)
        case 2:
                cell.sectionCollection.isHidden = true
                cell.addSubview(oasView)
                cell.addContraintAllScreen(view: oasView)
                cell.viewBar.isHidden = true
            
        default: break
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        // la hauteur pour la cellule des types de biens
        var height = view.frame.height / 5
        
        switch (view.frame.width,view.frame.height){
        case (375.0,667.0): // for iphone 6,7,8,S
            height = view.frame.height / 4.5
            break
        case (414.0,736.0): // for iphone 6,7,8,S
            height = view.frame.height / 5
            break
        case (375.0,812.0): // for iphone X,Xs
            height = view.frame.height / 5.6
            break
        case (414.0,896.0)://for iphone XS Max
            height = view.frame.height / 6
            break
        case (320.0,568.0): // for iphone SE
            height = view.frame.height / 4
            break
        default:break
        }
        
        // la hauteur pour la cellule opportinite
        if indexPath.section == 2 {
            height = view.frame.height / 3.5
            
            switch (view.frame.width,view.frame.height) {
                case (414.0,896.0)://for iphone XS Max
                    height = view.frame.height / 4
                    break
                case (375.0,667.0): // for iphone 6,7,8,S
                    height = view.frame.height / 3
                    break
                case (320.0,568.0): // for iphone SE
                    height = view.frame.height / 2.5
                    break
            case (414.0,736.0): // for iphone 6,7,8,S
                height = view.frame.height / 3
                break
                default: break
            }
        }
        
        return CGSize(width: collectionView.frame.width, height: height)
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "header", for: indexPath) as! LocationHeaderView
        header.headerLocationLabel.text = Constants.MAIN_HEADER_TABLE[indexPath.section]
        return header
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 36)
    }
    
    
}

