//
//  UserAddNumberView.swift
//  Kouva
//
//  Created by Bio'S on 03/06/2019.
//  Copyright © 2019 Kouva. All rights reserved.
//

import UIKit
import Firebase

class codeCell: UICollectionViewCell {
    
    var codeTextField: UITextField = {
        let field = UITextField()
        field.textAlignment = .center
        field.keyboardType = .numberPad
        field.textColor = AppColor.black()
        field.layer.cornerRadius = 4
        field.layer.borderColor = AppColor.black().cgColor
        field.layer.borderWidth = 1
        field.translatesAutoresizingMaskIntoConstraints = false
        return field
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(codeTextField)
        addContraintAllScreen(view: codeTextField)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class numCell: UICollectionViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! codeCell
        cell.codeTextField.tag = indexPath.item
        cell.codeTextField.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (codeVerificationCollectionView.frame.width / 6) - 10, height: collectionView.frame.height - 10)
    }
    
    // active cursor on next textfield when before is filled
    @objc func textFieldDidChange(textField: UITextField) {
        let item = textField.tag
        guard let text = textField.text else { return }
        
        switch item {
        case 0:
            if text.count == 1 {
                if let _codeCell = codeVerificationCollectionView.cellForItem(at: IndexPath(item: item + 1, section: 0)) as? codeCell {
                    _codeCell.codeTextField.becomeFirstResponder()
                }
            }
            break
        case 1:
            if text.isEmpty {
                if let _codeCell = codeVerificationCollectionView.cellForItem(at: IndexPath(item: item - 1, section: 0)) as? codeCell {
                    _codeCell.codeTextField.text = nil
                    _codeCell.codeTextField.becomeFirstResponder()
                }
            } else {
                if let _codeCell = codeVerificationCollectionView.cellForItem(at: IndexPath(item: item + 1, section: 0)) as? codeCell {
                    _codeCell.codeTextField.becomeFirstResponder()
                }
            }
            
            break
        case 2:
            if text.isEmpty {
                if let _codeCell = codeVerificationCollectionView.cellForItem(at: IndexPath(item: item - 1, section: 0)) as? codeCell {
                    _codeCell.codeTextField.text = nil
                    _codeCell.codeTextField.becomeFirstResponder()
                }
            } else {
                if let _codeCell = codeVerificationCollectionView.cellForItem(at: IndexPath(item: item + 1, section: 0)) as? codeCell {
                    _codeCell.codeTextField.becomeFirstResponder()
                }
            }
        
            break
        case 3:
            if text.isEmpty {
                if let _codeCell = codeVerificationCollectionView.cellForItem(at: IndexPath(item: item - 1, section: 0)) as? codeCell {
                    _codeCell.codeTextField.text = nil
                    _codeCell.codeTextField.becomeFirstResponder()
                }
            } else {
                if let _codeCell = codeVerificationCollectionView.cellForItem(at: IndexPath(item: item + 1, section: 0)) as? codeCell {
                    _codeCell.codeTextField.becomeFirstResponder()
                }
            }
            
            break
        case 4:
            if text.isEmpty {
                if let _codeCell = codeVerificationCollectionView.cellForItem(at: IndexPath(item: item - 1, section: 0)) as? codeCell {
                    _codeCell.codeTextField.text = nil
                    _codeCell.codeTextField.becomeFirstResponder()
                }
            } else {
                if let _codeCell = codeVerificationCollectionView.cellForItem(at: IndexPath(item: item + 1, section: 0)) as? codeCell {
                    _codeCell.codeTextField.becomeFirstResponder()
                }
            }
            
            break
        case 5:
            self.endEditing(true)
            progressView.startAnimating()
            codeVerificationBtn.SetTitleBtnAndFont(title: "")
            var codeTapByUser = ""
            for n in 0 ... 5 {
                if let _codeCell = codeVerificationCollectionView.cellForItem(at: IndexPath(item: n, section: 0)) as? codeCell {
                    codeTapByUser = codeTapByUser + _codeCell.codeTextField.text!
                }
            }
            
            NotificationCenter.default.post(name: .didSignUserPhoneNumberByVerificationCode, object: nil, userInfo: ["code":codeTapByUser])
            break
        default:
            break
        }
    }
    
    
    var codeVerificationCollectionView: UICollectionView = {
        var layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        var collection = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        collection.register(codeCell.self, forCellWithReuseIdentifier: "cell")
        collection.backgroundColor = UIColor.white
        collection.translatesAutoresizingMaskIntoConstraints = false
        return collection
    }()
    
    
    let subTitle: UITextView = {
        let text = UITextView()
        text.text = "Enregistrer votre numero pour Kouva puisse rester en contacter avec vous ."
        text.textAlignment = .center
        text.font = UIFont(name: "Avenir-Medium", size: 15)
        text.textColor = AppColor.black()
        text.isScrollEnabled = false
        text.isEditable = false
        text.translatesAutoresizingMaskIntoConstraints = false
        return text
    }()
    
    
    let headerTitle: UILabel = {
        let label = UILabel()
        label.text = "Contact"
        label.font = UIFont(name: "Avenir-Black", size: 30)
        label.textColor = AppColor.black()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    var numberTextField: UITextField = {
        let field = UITextField()
        field.textColor = AppColor.black()
        field.backgroundColor = UIColor.white
        field.keyboardType = .phonePad
        field.translatesAutoresizingMaskIntoConstraints = false
        return field
    }()
    
    let numberTextFieldBar = UIView()
    let label = UILabel()
    let verificationBtn = UIButton()
    
    let codeVerificationBtn: UIButton = {
        let btn = UIButton()
        btn.backgroundColor = AppColor.primaryColor()
        btn.SetTitleBtnAndFont(title: "Terminer")
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    var progressView: UIActivityIndicatorView = {
        let loadView = UIActivityIndicatorView()
        loadView.color = UIColor.white
        loadView.translatesAutoresizingMaskIntoConstraints = false
        return loadView
    }()
    
    let resendCodeBtn:UIButton = {
        let btn = UIButton()
        btn.backgroundColor = .white
        btn.SetTitleBtnAndFont(title: "Renvoyer le code", size: 12, titleColor: AppColor.primaryColor(), font: "Avenir-Heavy")
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let countTimer: UILabel = {
        let label = UILabel()
        label.text = "0:59"
        label.font = UIFont(name: "Avenir-Medium", size: 15)
        label.textColor = AppColor.black()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    // hidden user phone number view when code verification view is shown
    func hiddenInputNumber() {
        numberTextFieldBar.isHidden = true
        label.isHidden = true
        numberTextField.isHidden = true
        verificationBtn.isHidden = true
    }
    
    // hidden code verification view when phone number view is shown
    func hiddenCodeViews() {
        codeVerificationCollectionView.isHidden = true
        resendCodeBtn.isHidden = true
        codeVerificationBtn.isHidden = true
        countTimer.isHidden = true
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(headerTitle)
        headerTitle.topAnchor.constraint(equalTo: topAnchor, constant: 90).isActive = true
        headerTitle.leftAnchor.constraint(equalTo: leftAnchor, constant: 40).isActive = true
        
        addSubview(subTitle)
        subTitle.topAnchor.constraint(equalTo: headerTitle.bottomAnchor, constant: 10).isActive = true
        subTitle.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        subTitle.heightAnchor.constraint(equalToConstant: 60).isActive = true
        subTitle.widthAnchor.constraint(equalTo: widthAnchor, constant: -70).isActive = true
        
        label.text = "Numéro"
        label.textColor = AppColor.titleGrayColor()
        label.textAlignment = .left
        label.font = UIFont(name: "Avenir-Heavy", size: 15)
        label.translatesAutoresizingMaskIntoConstraints = false
        addSubview(label)
        label.topAnchor.constraint(equalTo: subTitle.bottomAnchor, constant: 40).isActive = true
        label.leftAnchor.constraint(equalTo: headerTitle.leftAnchor).isActive = true
        
        
        let codePortailLabel = UILabel()
        codePortailLabel.text = "+225"
        codePortailLabel.font = UIFont(name: "Avenir-Medium", size: 18)
        codePortailLabel.textColor = AppColor.titleGrayColor()
        codePortailLabel.translatesAutoresizingMaskIntoConstraints = false
        addSubview(codePortailLabel)
        codePortailLabel.topAnchor.constraint(equalTo: label.topAnchor, constant: 30).isActive = true
        codePortailLabel.leftAnchor.constraint(equalTo: label.leftAnchor, constant: 0).isActive = true
        
        addSubview(numberTextField)
        numberTextField.widthAnchor.constraint(equalTo: subTitle.widthAnchor, multiplier: 2/3, constant: 10).isActive = true
        numberTextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
        numberTextField.rightAnchor.constraint(equalTo: subTitle.rightAnchor, constant: 0).isActive = true
        numberTextField.topAnchor.constraint(equalTo: label.bottomAnchor, constant: 0).isActive = true
        
        
        numberTextFieldBar.backgroundColor = UIColor.lightGray
        numberTextFieldBar.translatesAutoresizingMaskIntoConstraints = false
        addSubview(numberTextFieldBar)
        numberTextFieldBar.topAnchor.constraint(equalTo: numberTextField.bottomAnchor).isActive = true
        numberTextFieldBar.widthAnchor.constraint(equalTo: numberTextField.widthAnchor).isActive = true
        numberTextFieldBar.centerXAnchor.constraint(equalTo: numberTextField.centerXAnchor).isActive = true
        numberTextFieldBar.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
        
        
        verificationBtn.SetTitleBtnAndFont(title: "Vérifier")
        verificationBtn.backgroundColor = AppColor.primaryColor()
        verificationBtn.translatesAutoresizingMaskIntoConstraints = false
        addSubview(verificationBtn)
        verificationBtn.topAnchor.constraint(equalTo: numberTextFieldBar.bottomAnchor, constant: 60).isActive = true
        verificationBtn.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 1/3, constant: 0).isActive = true
        verificationBtn.heightAnchor.constraint(equalTo: verificationBtn.widthAnchor, multiplier: 1/3, constant: 10).isActive = true
        verificationBtn.rightAnchor.constraint(equalTo: numberTextFieldBar.rightAnchor, constant: 0).isActive = true
        verificationBtn.layer.cornerRadius = 5
        
        
        addSubview(codeVerificationBtn)
        codeVerificationBtn.topAnchor.constraint(equalTo: numberTextFieldBar.bottomAnchor, constant: 60).isActive = true
        codeVerificationBtn.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 1/3, constant: 0).isActive = true
        codeVerificationBtn.heightAnchor.constraint(equalTo: verificationBtn.widthAnchor, multiplier: 1/3, constant: 10).isActive = true
        codeVerificationBtn.rightAnchor.constraint(equalTo: numberTextFieldBar.rightAnchor, constant: 0).isActive = true
        codeVerificationBtn.layer.cornerRadius = 5
        
        codeVerificationBtn.addSubview(progressView)
        codeVerificationBtn.addContraintAllScreen(view: progressView)
        
        
        codeVerificationCollectionView.delegate = self
        codeVerificationCollectionView.dataSource = self
        addSubview(codeVerificationCollectionView)
        codeVerificationCollectionView.topAnchor.constraint(equalTo: label.topAnchor).isActive = true
        codeVerificationCollectionView.widthAnchor.constraint(equalTo: widthAnchor, constant: -60).isActive = true
        codeVerificationCollectionView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        codeVerificationCollectionView.heightAnchor.constraint(equalTo: numberTextField.heightAnchor, constant: 30).isActive = true
        
        addSubview(resendCodeBtn)
        resendCodeBtn.topAnchor.constraint(equalTo: codeVerificationCollectionView.bottomAnchor, constant: 10).isActive = true
        resendCodeBtn.leftAnchor.constraint(equalTo: codeVerificationCollectionView.leftAnchor).isActive = true
        resendCodeBtn.widthAnchor.constraint(equalTo: codeVerificationCollectionView.widthAnchor, multiplier: 1/3, constant: 0).isActive = true
        resendCodeBtn.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        addSubview(countTimer)
        countTimer.centerYAnchor.constraint(equalTo: resendCodeBtn.centerYAnchor).isActive = true
        countTimer.rightAnchor.constraint(equalTo: codeVerificationCollectionView.rightAnchor, constant: -10).isActive = true
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class UserAddNumberView: UIView, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITextFieldDelegate {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! numCell
        cell.verificationBtn.addTarget(self, action: #selector(selectCodeVerificationView), for: .touchUpInside)
        cell.resendCodeBtn.addTarget(self, action: #selector(resendCodeOfVerification), for: .touchUpInside)
        cell.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(resignTextField)))
        if indexPath.item == 0 {
            cell.hiddenCodeViews()
        } else {
            cell.headerTitle.text = "Vérification Code"
            cell.subTitle.text = "Veuillez saisir le code de vérification envoyé à +225 "
            cell.hiddenInputNumber()
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    
    //  resend code verification to user number by sms and make all textfield empty
    @objc func resendCodeOfVerification() {
        print("code resended")
        sendCodeToUserPhoneNumberBySms()
        if let _numCell = numberStoreCollectionView.cellForItem(at: IndexPath(item: 1, section: 0)) as? numCell{
            for n in 0 ... 5 {
                if let _codeCell = _numCell.codeVerificationCollectionView.cellForItem(at: IndexPath(item: n, section: 0)) as? codeCell {
                    _codeCell.codeTextField.text = nil
                }
            }
        }
    }
    
    var timer:Timer!
    var time:Int = 60
    //send code to user phone number by sms
    func sendCodeToUserPhoneNumberBySms() {
        Auth.auth().languageCode = "fr"
        if let _numCell = numberStoreCollectionView.cellForItem(at: IndexPath(item: 0, section: 0)) as? numCell {
            if let userNumber = _numCell.numberTextField.text {
                PhoneAuthProvider.provider().verifyPhoneNumber("+225\(userNumber)", uiDelegate: nil) { (verificationID, error) in
                    if (error != nil) {
                        print("send code user",error!)
                        return
                    }
                    
                    self.time = 60 // renitial time count each time user want to send code sms
                    self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.sablierDecount), userInfo: nil, repeats: true)
                    print("debug verification:",verificationID!)
                    UserDefaults.standard.set(verificationID!, forKey: "verificationID")
                    UserDefaults.standard.set("+225\(userNumber)", forKey: "userNumber")
                }
            }
        }
    }
    
    //function provide timer count
    @objc func sablierDecount() {
        if let _numCell = numberStoreCollectionView.cellForItem(at: IndexPath(item: 1, section: 0)) as? numCell {
            time = time - 1
            _numCell.countTimer.text = "\(time/60):\(time%60)"
            
            // stop Timer if second equal 0 (0:0)
            if time % 60  == 0 {
                timer.invalidate()
            }
        }
    }
    // select view of code verification
    @objc func selectCodeVerificationView(){
        resignTextField()
        sendCodeToUserPhoneNumberBySms()
        numberStoreCollectionView.selectItem(at: IndexPath(item: 1, section: 0), animated: true, scrollPosition: .centeredHorizontally)
    }
    
    //Resign textField input number
    @objc func resignTextField() {
        self.endEditing(true)
    }
    //resign textfield while user scroll | set user added in verification view
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        resignTextField()
        let item = Int(scrollView.contentOffset.x / frame.width)
        if item == 1 {
            //take user number added et show it in verification view
            if let _codeVerificationNumCell = numberStoreCollectionView.cellForItem(at: IndexPath(item: 1, section: 0)) as? numCell {
                // select first code textfield to show keyboard
                if let _codeCell = _codeVerificationNumCell.codeVerificationCollectionView.cellForItem(at: IndexPath(item: 0, section: 0)) as? codeCell {
                    _codeCell.codeTextField.becomeFirstResponder()
                }

                // print user number phone enter in view
                if let _numCell = numberStoreCollectionView.cellForItem(at: IndexPath(item: 0, section: 0)) as? numCell {
                    if let userNumber = _numCell.numberTextField.text {
                        _codeVerificationNumCell.subTitle.text = "Veuillez saisir le code de vérification envoyé à +225 \(userNumber)"
                    }
                }
            }
        }
        
    }
    
    var numberStoreCollectionView: UICollectionView = {
        var layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        var collection = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        collection.register(numCell.self, forCellWithReuseIdentifier: "cell")
        collection.backgroundColor = UIColor.white
        collection.isPagingEnabled = true
        collection.bounces = false
        collection.translatesAutoresizingMaskIntoConstraints = false
        return collection
    } ()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        numberStoreCollectionView.delegate = self
        numberStoreCollectionView.dataSource = self
        
        addSubview(numberStoreCollectionView)
        addContraintAllScreen(view: numberStoreCollectionView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


