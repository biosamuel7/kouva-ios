//
//  ImageZoomView.swift
//  Kouva
//
//  Created by Bio'S on 03/06/2019.
//  Copyright © 2019 Kouva. All rights reserved.
//

import UIKit

class ImagezoomView: UIView, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    var showDetailHomeVC:ShowDetailHomeViewController?
    let imageZoomCollection: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.scrollDirection = .horizontal
        let collection = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        collection.backgroundColor = UIColor.black
        collection.isPagingEnabled = true
        collection.translatesAutoresizingMaskIntoConstraints = false
        return collection
    }()
    
    var imageZoomCollectionHeightConstraint:NSLayoutConstraint?
    
    var data:[ImageHome] = []
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        imageZoomCollection.register(GalleryCell.self, forCellWithReuseIdentifier: "cell")
        imageZoomCollection.delegate = self
        imageZoomCollection.dataSource = self
        
        SetUpConstraint()
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! GalleryCell
        cell.imageGallery.contentMode = .scaleAspectFill
        let url = data[indexPath.row]
        cell.imageGallery.loadImageUsingCache(withPath: url.path)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    func SetUpConstraint() {
        
        addSubview(imageZoomCollection)
        imageZoomCollection.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        imageZoomCollection.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        imageZoomCollectionHeightConstraint = imageZoomCollection.heightAnchor.constraint(equalToConstant: frame.height/3)
        imageZoomCollectionHeightConstraint?.isActive = true
        imageZoomCollection.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
        
        //Dissmiss Zoome image
        let dissMissViewBtn = UIButton(type: .system)
        dissMissViewBtn.setImage(#imageLiteral(resourceName: "deleteView_icon").withRenderingMode(.alwaysOriginal), for: .normal)
        dissMissViewBtn.addTarget(self, action: #selector(HiddenZoomImageView), for: .touchUpInside)
        dissMissViewBtn.translatesAutoresizingMaskIntoConstraints = false
        addSubview(dissMissViewBtn)
        dissMissViewBtn.topAnchor.constraint(equalTo: topAnchor, constant: 27).isActive = true
        dissMissViewBtn.widthAnchor.constraint(equalToConstant: 16).isActive = true
        dissMissViewBtn.heightAnchor.constraint(equalToConstant: 16).isActive = true
        dissMissViewBtn.leftAnchor.constraint(equalTo: leftAnchor, constant: 15).isActive = true
    }
    @objc func HiddenZoomImageView() {
        let value = UIInterfaceOrientation.landscapeLeft.isPortrait.hashValue
        UIDevice.current.setValue(value, forKey: "orientation")
        UIViewController.attemptRotationToDeviceOrientation()
        showDetailHomeVC?.imageZoomBackground.isHidden = true
        
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
