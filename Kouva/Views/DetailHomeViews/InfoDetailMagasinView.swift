//
//  InfoDetailMagasinView.swift
//  Kouva
//
//  Created by Bio'S on 03/06/2019.
//  Copyright © 2019 Kouva. All rights reserved.
//

import UIKit
import GoogleMaps

class InfoDetailMagasinView: UIView, UIScrollViewDelegate {
    
    
    var homeSelectionned:HomeModel!
    let infoScroolView: UIScrollView = {
        let v = UIScrollView()
        v.backgroundColor = UIColor.white
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    let aboutHomeTextView: UITextView = {
        let textView = UITextView()
        textView.text = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        textView.font = UIFont(name: "Avenir-Roman", size: 15)
        textView.textColor = AppColor.ClearColor()
        textView.isEditable = false
        textView.isScrollEnabled = false
        textView.translatesAutoresizingMaskIntoConstraints = false
        return textView
    }()
    let readMoreBtn: UIButton = {
        let btn = UIButton()
        btn.backgroundColor = UIColor.white
        btn.SetTitleBtnAndFont(title: "afficher", size: 15, titleColor: AppColor.green(), font: "Avenir-Heavy")
        btn.sizeToFit()
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    let dimensionLabel: UILabel = {
        let label = UILabel()
        label.text = "Un appartement de 120 m²"
        label.font = UIFont(name: "Avenir-Roman", size: 15)
        label.textColor = AppColor.ClearColor()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    let pasDePorteLabel: UILabel = {
        let label = UILabel()
        label.text = "Sans pas de porte"
        label.font = UIFont(name: "Avenir-Roman", size: 15)
        label.textColor = AppColor.ClearColor()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let MapView: GMSMapView = {
        let mapView = GMSMapView()
        mapView.setMinZoom(10, maxZoom: 15)
        mapView.isMyLocationEnabled = true
        mapView.translatesAutoresizingMaskIntoConstraints = false
        return mapView
    }()
    var scroollViewheigh:CGFloat = 0
    
    var aboutHomeTextViewConstraintHeigh:NSLayoutConstraint?
    
    let publishlabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.white
        
        var extraHeight:CGFloat = 0
        switch frame.width {
        case 320.0:
            extraHeight = 300
            break
        case 375.0:
            extraHeight = 370
            break
        case 414.0:
            extraHeight = 370
            break
        default:
            break
        }
        scroollViewheigh = (frame.width / 4) + (frame.height / 2) + extraHeight
        addSubview(infoScroolView)
        addContraintAllScreen(view: infoScroolView)
        infoScroolView.contentSize = CGSize(width: frame.width, height: scroollViewheigh)
        infoScroolView.delegate = self
        SetUpConstraint()
        readMoreBtn.addTarget(self, action: #selector(showMoreDescription), for: .touchUpInside)
        
    }
    @objc func showMoreDescription() {
        
        var extraHeight:CGFloat = 0
        switch frame.width {
        case 320.0:
            extraHeight = 400
            break
        case 375.0:
            extraHeight = 490
            break
        case 414.0:
            extraHeight = 490
            break
        default:
            break
        }
        
        let descriptionHeigh = homeSelectionned.description.estimateheight(withConstrainedWidth: frame.width, font: UIFont(name: "Avenir-Roman", size: 15)!) + 40
        
        let mapViewHeigh = (frame.height / 2)
        
        aboutHomeTextViewConstraintHeigh?.constant = descriptionHeigh
        infoScroolView.contentSize.height = mapViewHeigh + extraHeight + descriptionHeigh
        UIView.animate(withDuration: 0.1) {
            self.layoutIfNeeded()
            self.readMoreBtn.isHidden = true
        }
        
    }
    func setInfo(home:HomeModel) {
        let postion = CLLocationCoordinate2D(latitude: home.place.latitude, longitude: home.place.longitude)
        let cam = GMSCameraUpdate.setTarget(postion, zoom: 15.0)
        MapView.moveCamera(cam)
        let circle = GMSCircle(position: postion, radius: 500)
        circle.fillColor = UIColor(r: 141, g: 130, b: 206, alpha: 0.3)
        circle.radius = 300
        circle.strokeColor = AppColor.primaryColor()
        circle.map = MapView
        
        //get the date home stored
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
        let homeDate = home.date.dateValue()
        let timePeriod = Calendar.current.dateComponents(Set<Calendar.Component>([.month, .day,.hour]), from: homeDate, to: Date())
        if timePeriod.month != 0 {
            publishlabel.text = "Il y a \(timePeriod.month!) mois"
        }else if timePeriod.day != 0 {
            publishlabel.text = "Il y a \(timePeriod.day!) jours"
        } else {
            publishlabel.text = "Il y a \(timePeriod.hour!) heures"
        }
        
        
        homeSelectionned = home
        aboutHomeTextView.text = home.description
        dimensionLabel.text = "Un magasin de " + home.superficie
        pasDePorteLabel.text = home.pasDePorte
        
        let descriptionHeigh = home.description.estimateheight(withConstrainedWidth: frame.width, font: UIFont(name: "Avenir-Roman", size: 15)!) + 40
        let aboutTextViewHeigh = frame.width / 4
        
        if descriptionHeigh <= aboutTextViewHeigh {
            readMoreBtn.isHidden = true
        }
    }
    
    func SetUpConstraint() {
        
        let top:CGFloat = 25
        
        //SET UP PUBLISHER INFO
        let publishHeader = UILabel()
        publishHeader.font = UIFont(name: "Avenir-Black", size: 18)
        publishHeader.text = "Publier"
        publishHeader.textColor = AppColor.ClearColor()
        publishHeader.textAlignment = .left
        publishHeader.translatesAutoresizingMaskIntoConstraints = false
        infoScroolView.addSubview(publishHeader)
        publishHeader.topAnchor.constraint(equalTo: infoScroolView.topAnchor, constant: top).isActive = true
        publishHeader.leftAnchor.constraint(equalTo: infoScroolView.leftAnchor, constant: 10).isActive = true
        publishHeader.heightAnchor.constraint(equalToConstant: 20).isActive = true
        publishlabel.font = UIFont(name: "Avenir-Roman", size: 15)
        publishlabel.textColor = AppColor.ClearColor()
        publishlabel.textAlignment = .left
        publishlabel.translatesAutoresizingMaskIntoConstraints = false
        infoScroolView.addSubview(publishlabel)
        publishlabel.topAnchor.constraint(equalTo: publishHeader.bottomAnchor, constant: 10).isActive = true
        publishlabel.leftAnchor.constraint(equalTo: infoScroolView.leftAnchor, constant: 10).isActive = true
        publishlabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        
        //// DIMENSION HOME
        let dimensioHeader = UILabel()
        dimensioHeader.font = UIFont(name: "Avenir-Black", size: 18)
        dimensioHeader.text = "Superficie"
        dimensioHeader.textColor = AppColor.ClearColor()
        dimensioHeader.textAlignment = .left
        dimensioHeader.translatesAutoresizingMaskIntoConstraints = false
        infoScroolView.addSubview(dimensioHeader)
        dimensioHeader.topAnchor.constraint(equalTo: publishlabel.bottomAnchor, constant: top).isActive = true
        dimensioHeader.leftAnchor.constraint(equalTo: infoScroolView.leftAnchor, constant: 10).isActive = true
        dimensioHeader.heightAnchor.constraint(equalToConstant: 20).isActive = true
        infoScroolView.addSubview(dimensionLabel)
        dimensionLabel.topAnchor.constraint(equalTo: dimensioHeader.bottomAnchor, constant: 10).isActive = true
        dimensionLabel.widthAnchor.constraint(equalTo: infoScroolView.widthAnchor).isActive = true
        dimensionLabel.leftAnchor.constraint(equalTo: dimensioHeader.leftAnchor).isActive = true
        
        
        let pasDePorteHeader = UILabel()
        pasDePorteHeader.font = UIFont(name: "Avenir-Black", size: 18)
        pasDePorteHeader.text = "Pas de porte"
        pasDePorteHeader.textColor = AppColor.ClearColor()
        pasDePorteHeader.textAlignment = .left
        pasDePorteHeader.translatesAutoresizingMaskIntoConstraints = false
        infoScroolView.addSubview(pasDePorteHeader)
        pasDePorteHeader.topAnchor.constraint(equalTo: dimensionLabel.bottomAnchor, constant: top).isActive = true
        pasDePorteHeader.leftAnchor.constraint(equalTo: infoScroolView.leftAnchor, constant: 10).isActive = true
        pasDePorteHeader.heightAnchor.constraint(equalToConstant: 20).isActive = true
        infoScroolView.addSubview(pasDePorteLabel)
        pasDePorteLabel.topAnchor.constraint(equalTo: pasDePorteHeader.bottomAnchor, constant: 10).isActive = true
        pasDePorteLabel.widthAnchor.constraint(equalTo: infoScroolView.widthAnchor).isActive = true
        pasDePorteLabel.leftAnchor.constraint(equalTo: dimensioHeader.leftAnchor).isActive = true
        
        //// ABOUT HOME
        let aboutHeader = UILabel()
        aboutHeader.font = UIFont(name: "Avenir-Black", size: 18)
        aboutHeader.text = "A propos"
        aboutHeader.textColor = AppColor.ClearColor()
        aboutHeader.textAlignment = .left
        aboutHeader.translatesAutoresizingMaskIntoConstraints = false
        infoScroolView.addSubview(aboutHeader)
        aboutHeader.topAnchor.constraint(equalTo: pasDePorteLabel.bottomAnchor, constant: top).isActive = true
        aboutHeader.leftAnchor.constraint(equalTo: infoScroolView.leftAnchor, constant: 10).isActive = true
        aboutHeader.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        infoScroolView.addSubview(aboutHomeTextView)
        aboutHomeTextView.topAnchor.constraint(equalTo: aboutHeader.bottomAnchor, constant: 10).isActive = true
        aboutHomeTextView.widthAnchor.constraint(equalTo: infoScroolView.widthAnchor, constant: -3).isActive = true
        aboutHomeTextView.centerXAnchor.constraint(equalTo: infoScroolView.centerXAnchor).isActive = true
        aboutHomeTextViewConstraintHeigh = aboutHomeTextView.heightAnchor.constraint(equalToConstant: frame.width / 4)
        aboutHomeTextViewConstraintHeigh?.isActive = true
        infoScroolView.addSubview(readMoreBtn)
        
        var readMoreYposition:CGFloat = 0
        
        switch Int(frame.width) {
        case 320:
            readMoreYposition = -15
            break
        case 375:
            readMoreYposition = -27
            break
        case 414:
            readMoreYposition = -17
            break
        default:
            break
        }
        readMoreBtn.bottomAnchor.constraint(equalTo: aboutHomeTextView.bottomAnchor, constant: readMoreYposition).isActive = true
        readMoreBtn.rightAnchor.constraint(equalTo: aboutHomeTextView.rightAnchor).isActive = true
        readMoreBtn.heightAnchor.constraint(equalToConstant: 14).isActive = true
        
        
        //// MAP
        let mapHeader = UILabel()
        mapHeader.font = UIFont(name: "Avenir-Black", size: 18)
        mapHeader.text = "Localisation"
        mapHeader.textColor = AppColor.ClearColor()
        mapHeader.textAlignment = .left
        mapHeader.translatesAutoresizingMaskIntoConstraints = false
        infoScroolView.addSubview(mapHeader)
        mapHeader.topAnchor.constraint(equalTo: aboutHomeTextView.bottomAnchor, constant: 10).isActive = true
        mapHeader.leftAnchor.constraint(equalTo: infoScroolView.leftAnchor, constant: 10).isActive = true
        mapHeader.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        infoScroolView.addSubview(MapView)
        MapView.topAnchor.constraint(equalTo: mapHeader.bottomAnchor, constant: 20).isActive = true
        MapView.widthAnchor.constraint(equalTo: infoScroolView.widthAnchor, constant: 0).isActive = true
        MapView.heightAnchor.constraint(equalTo: infoScroolView.heightAnchor, multiplier: 1/2).isActive = true
        MapView.centerXAnchor.constraint(equalTo: infoScroolView.centerXAnchor).isActive = true
        
        
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //        print(scrollView.contentOffset.y)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}



