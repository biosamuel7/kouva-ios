//
//  SlideImageCollection.swift
//  Kouva
//
//  Created by Bio'S on 03/06/2019.
//  Copyright © 2019 Kouva. All rights reserved.
//

import UIKit
import Firebase

class SlideImageCollection: UIView, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {
    
    
    
    var showDetailHomeVC:ShowDetailHomeViewController?
    let imageSlideCollection: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.scrollDirection = .horizontal
        let collection = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        collection.backgroundColor = UIColor.white
        collection.isPagingEnabled = true
        collection.translatesAutoresizingMaskIntoConstraints = false
        return collection
    }()
    
    var data:[ImageHome] = []
    var timer:Timer?
    var count:Int = 0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.red
        
        imageSlideCollection.register(GalleryCell.self, forCellWithReuseIdentifier: "cell")
        imageSlideCollection.delegate = self
        imageSlideCollection.dataSource = self
        SetUpConstraint()
        
        timer = Timer.scheduledTimer(timeInterval: 3.5, target: self, selector: #selector(slideImageTimer), userInfo: nil, repeats: true)
        
    }
    @objc func slideImageTimer() {
        count = count + 1
        if count < data.count {
            let indexPath = IndexPath(item: count, section: 0)
            imageSlideCollection.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        }else {
            count = 0
            let indexPath = IndexPath(item: count, section: 0)
            imageSlideCollection.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)
        }
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let index = scrollView.contentOffset.x / frame.width
        count = Int(index)
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! GalleryCell
        cell.imageGallery.contentMode = .scaleAspectFill
        let url = data[indexPath.row]
        cell.imageGallery.loadImageUsingCache(withPath: url.path)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        showDetailHomeVC?.getImageIndex(index: indexPath.row)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    func SetUpConstraint() {
        addSubview(imageSlideCollection)
        addContraintAllScreen(view: imageSlideCollection)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

