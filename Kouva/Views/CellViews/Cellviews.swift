//
//  Cellviews.swift
//  Kouva
//
//  Created by Bio'S on 03/06/2019.
//  Copyright © 2019 Kouva. All rights reserved.
//

import UIKit
import Firebase

class NombreDePieceCell: UICollectionViewCell {
    var nombreDePiceLabel: UILabel = {
        let label = UILabel()
        label.text = "1"
        label.textColor = AppColor.primaryColor()
        label.font = UIFont(name: "Avenir-Black", size: 15)
        //        label.sizeToFit()
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        
        addSubview(nombreDePiceLabel)
        nombreDePiceLabel.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        nombreDePiceLabel.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        contentView.backgroundColor = .white
        contentView.layer.borderColor = AppColor.primaryColor().cgColor
        contentView.layer.borderWidth = 1
        contentView.layer.cornerRadius = 5
    }
    override var isSelected: Bool {
        didSet {
            UIView.animate(withDuration: 0.3) {
                self.contentView.backgroundColor = self.isSelected ?  AppColor.primaryColor() : UIColor.white
                self.nombreDePiceLabel.textColor = self.isSelected ? UIColor.white : AppColor.primaryColor()
                self.contentView.layer.borderColor = self.isSelected ? UIColor.white.cgColor : AppColor.primaryColor().cgColor
            }
        }
        
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class HomeInfoCell: UICollectionViewCell {
    var iconHome: UIImageView = {
        let img = UIImageView()
        img.image = #imageLiteral(resourceName: "Bed 2")
        img.contentMode = .scaleAspectFit
        img.clipsToBounds = true
        img.translatesAutoresizingMaskIntoConstraints = false
        return img
    }()
    var iconName: UILabel = {
        let label = UILabel()
        label.text = "Douche"
        label.textColor = UIColor(white: 1.0, alpha: 0.3)
        label.font = UIFont.systemFont(ofSize: 9, weight: UIFont.Weight.regular)
        label.sizeToFit()
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    var nombrePiece: UILabel = {
        let label = UILabel()
        label.text = "3"
        label.textColor = UIColor.yellow
        label.font = UIFont(name: "Avenir-Black", size: 13)
        label.sizeToFit()
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = AppColor.primaryColor()
        addSubview(iconHome)
        addSubview(iconName)
        addSubview(nombrePiece)
        
        
        iconHome.topAnchor.constraint(equalTo: topAnchor, constant: 10).isActive = true
        iconHome.widthAnchor.constraint(equalToConstant: bounds.width / 3).isActive = true
        iconHome.heightAnchor.constraint(equalToConstant: bounds.height / 3).isActive = true
        iconHome.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        
        iconName.topAnchor.constraint(equalTo: iconHome.bottomAnchor, constant: 5).isActive = true
        iconName.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
        iconName.heightAnchor.constraint(equalToConstant: 10).isActive = true
        iconName.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        
        nombrePiece.topAnchor.constraint(equalTo: iconName.bottomAnchor, constant: 10).isActive = true
        nombrePiece.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
        nombrePiece.heightAnchor.constraint(equalToConstant: 20).isActive = true
        nombrePiece.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class mainCollectionCell: UICollectionViewCell, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UICollectionViewDelegate {
    
    var mainViewController: MainViewController?
    let sectionCollection: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 10)
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 10
        layout.minimumInteritemSpacing = 0
        
        let collection = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        collection.backgroundColor = .white
        collection.translatesAutoresizingMaskIntoConstraints = false
        collection.showsHorizontalScrollIndicator = false
        return collection
    }()
    let viewBar = UIView()
    
    var dataSection:[String] = []
    var sectionPosition:Int = 0
    
    var dataHomeOAS = [HomeModel]()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        addSubview(sectionCollection)
        addContraintAllScreen(view: sectionCollection)
        
        sectionCollection.register(TypeDeBienCell.self, forCellWithReuseIdentifier: "cell")
        sectionCollection.delegate = self
        sectionCollection.dataSource = self
        
        viewBar.backgroundColor = UIColor(r: 213, g: 213, b: 217)
        viewBar.translatesAutoresizingMaskIntoConstraints = false
        addSubview(viewBar)
        viewBar.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10).isActive = true
        viewBar.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        viewBar.heightAnchor.constraint(equalToConstant: 1).isActive = true
        viewBar.widthAnchor.constraint(equalTo: widthAnchor, constant: -50).isActive = true
        
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSection.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! TypeDeBienCell
        cell.titleLocationVenteLabel.text = dataSection[indexPath.item]
    
        if frame.width == 320.0 {
            cell.titleLocationVenteLabel.font = UIFont(name: "Avenir-Medium", size: 13)
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let listingHomeVC = ListingHomesViewController()
        switch sectionPosition {
        case 0:
            listingHomeVC.homeType = TypeDebienModel(possession: "location", type: dataSection[indexPath.row].lowercased())
        case 1:
            listingHomeVC.homeType = TypeDebienModel(possession: "vente", type: dataSection[indexPath.row].lowercased())
        default: break
            
        }
        
        mainViewController?.navigationController?.pushViewController(listingHomeVC, animated: true)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
//        var width = frame.width / 2.5
        
//        if sectionPosition == 2 {
//            width = width + 100
//            if frame.height == 348.8 {
//                width = frame.width / 1.5
//            }
//        }
        return CGSize(width: frame.width / 2.5, height: collectionView.frame.height)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class TypeDeBienCell: UICollectionViewCell {
    
    var home: HomeModel? {
        didSet {
            if let home = home {
                prixOpportinityLabel.text = home.prix + " FCFA"
                secteurOpportinityLabel.text = home.commune + " - " + home.secteur
                switch home.type {
                case "magasin":
                    typeDebienOpportinityLabel.text = (home.possessionDuBien == "location") ? "Magasin à louer" : "Magasin à vendre"
                case "studio":
                    typeDebienOpportinityLabel.text = (home.possessionDuBien == "location") ? "Studio à louer" : "Studio à vendre"
                default :
                    typeDebienOpportinityLabel.text = (home.possessionDuBien == "location") ? "\(home.type.uppercaseFirstCaracters()) \(home.piece!) pièces à louer" : "\(home.type.uppercaseFirstCaracters()) \(home.piece!) pièces à vendre"
                }
                imageHomeOpportinityImageView.loadImageUsingCache(withPath: home.imagePrincipale.path)
            }
        }
    }
    
    //LOCATION AND VENTE VIEW
    let locationVenteView:UIView = {
        let v = UIView()
        v.backgroundColor = UIColor(r: 241, g: 241, b: 241)
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    let iconlocationVenteIMGView:UIImageView = {
        let img = UIImageView()
        img.image = #imageLiteral(resourceName: "newspaper").withRenderingMode(.alwaysOriginal)
        img.translatesAutoresizingMaskIntoConstraints = false
        return img
    }()
    
    let titleLocationVenteLabel: UILabel = {
        let label = UILabel()
        label.text = "Appartement"
        label.textColor = UIColor(r: 46, g: 47, b: 65)
        label.font = UIFont(name: "Avenir-Medium", size: 15)
        //label.attributedText = NSMutableAttributedString(string: label.text!, attributes: [])
        label.sizeToFit()
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    
    // OPPORTINITY VIEW
    let opportinityView: UIView = {
        let v = UIView()
        v.isHidden = true
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    var imageHomeOpportinityImageView: UIImageView = {
        let img = UIImageView()
        img.contentMode = .scaleAspectFill
        img.image = #imageLiteral(resourceName: "placeholder")
        img.clipsToBounds = true
        img.translatesAutoresizingMaskIntoConstraints = false
        return img
    }()
    var homeLikeBtn: UIButton = {
        let btn = UIButton(type: UIButtonType.system)
        btn.setImage(#imageLiteral(resourceName: "unlike").withRenderingMode(.alwaysOriginal), for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    var secteurOpportinityLabel: UILabel = {
        let label = UILabel()
        label.text = "Cocody - Angre 8e Tranche"
        label.textColor = AppColor.ClearColor()
        label.font = UIFont(name: "Avenir-Light", size: 15)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    var prixOpportinityLabel: UILabel = {
        let label = UILabel()
        label.text = "400 000 F"
        label.textColor = AppColor.priceColor()
        label.font = UIFont(name: "Avenir-Black", size: 15)
        label.sizeToFit()
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    var typeDebienOpportinityLabel: UILabel = {
        let label = UILabel()
        label.text = "Appartement a louer"
        label.textColor = .black
        label.font = UIFont(name: "Avenir-Medium", size: 12)
        label.sizeToFit()
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    
    func showOpportunityASaisirCell() {
        locationVenteView.isHidden = true
        opportinityView.isHidden = false
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        SetupAllViewConstraints()
        
    }
    func SetupAllViewConstraints() {
        //VENTE AND LOCATION VIEW CONSTRAINTS
        addSubview(locationVenteView)
        locationVenteView.heightAnchor.constraint(equalToConstant: frame.height - 50).isActive = true
        locationVenteView.widthAnchor.constraint(equalTo: widthAnchor, constant: 0).isActive = true
        locationVenteView.topAnchor.constraint(equalTo: topAnchor, constant: 10).isActive = true
        locationVenteView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        locationVenteView.layer.cornerRadius = 6
        
        locationVenteView.addSubview(iconlocationVenteIMGView)
        iconlocationVenteIMGView.topAnchor.constraint(equalTo: locationVenteView.topAnchor, constant: 20).isActive = true
        iconlocationVenteIMGView.leftAnchor.constraint(equalTo: locationVenteView.leftAnchor, constant: 20).isActive = true
        iconlocationVenteIMGView.heightAnchor.constraint(equalToConstant: frame.width / 8).isActive = true
        iconlocationVenteIMGView.widthAnchor.constraint(equalToConstant: frame.width / 8).isActive = true
        
        locationVenteView.addSubview(titleLocationVenteLabel)
        titleLocationVenteLabel.leftAnchor.constraint(equalTo: locationVenteView.leftAnchor, constant: 20).isActive = true
        if(frame.height == 130.0){
            titleLocationVenteLabel.bottomAnchor.constraint(equalTo: locationVenteView.bottomAnchor, constant: -10).isActive = true
        }else {
            titleLocationVenteLabel.bottomAnchor.constraint(equalTo: locationVenteView.bottomAnchor, constant: -20).isActive = true
        }
        
        
        /// OPPORTINY CONSTRAINS
        addSubview(opportinityView)
        addContraintAllScreen(view: opportinityView)
        
        opportinityView.addSubview(imageHomeOpportinityImageView)
        imageHomeOpportinityImageView.topAnchor.constraint(equalTo: opportinityView.topAnchor).isActive = true
        imageHomeOpportinityImageView.centerXAnchor.constraint(equalTo: opportinityView.centerXAnchor).isActive = true
        imageHomeOpportinityImageView.widthAnchor.constraint(equalTo: opportinityView.widthAnchor).isActive = true
        imageHomeOpportinityImageView.heightAnchor.constraint(equalTo: opportinityView.heightAnchor, multiplier: 2/3).isActive = true
        
        opportinityView.addSubview(homeLikeBtn)
        homeLikeBtn.topAnchor.constraint(equalTo: opportinityView.topAnchor, constant: 10).isActive = true
        homeLikeBtn.rightAnchor.constraint(equalTo: opportinityView.rightAnchor, constant: -10).isActive = true
        homeLikeBtn.heightAnchor.constraint(equalToConstant: 20).isActive = true
        homeLikeBtn.widthAnchor.constraint(equalToConstant: 25).isActive = true
        
        opportinityView.addSubview(prixOpportinityLabel)
        prixOpportinityLabel.topAnchor.constraint(equalTo: imageHomeOpportinityImageView.bottomAnchor, constant: 5).isActive = true
        prixOpportinityLabel.widthAnchor.constraint(equalTo: opportinityView.widthAnchor).isActive = true
        
        opportinityView.addSubview(secteurOpportinityLabel)
        secteurOpportinityLabel.topAnchor.constraint(equalTo: prixOpportinityLabel.bottomAnchor, constant: 5).isActive = true
        secteurOpportinityLabel.widthAnchor.constraint(equalTo: opportinityView.widthAnchor).isActive = true
        
        opportinityView.addSubview(typeDebienOpportinityLabel)
        typeDebienOpportinityLabel.topAnchor.constraint(equalTo: secteurOpportinityLabel.bottomAnchor, constant: 5).isActive = true
        typeDebienOpportinityLabel.widthAnchor.constraint(equalTo: opportinityView.widthAnchor).isActive = true
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override var isSelected: Bool {
        didSet {
            UIView.animate(withDuration: 0.3) {
                self.locationVenteView.backgroundColor = self.isSelected ? AppColor.primaryColor() : UIColor(r: 241, g: 241, b: 241)
                self.titleLocationVenteLabel.textColor = self.isSelected ? .white : UIColor(r: 46, g: 47, b: 65)
                self.iconlocationVenteIMGView.image = self.isSelected ? UIImage(named: "newspaper_active")?.withRenderingMode(.alwaysOriginal) : UIImage(named: "newspaper")?.withRenderingMode(.alwaysOriginal)
            }
        }
        
    }
    
}

class PuceViewCell: UICollectionViewCell {
    
    var puceIcon: UIImageView = {
        let img = UIImageView()
        img.image = #imageLiteral(resourceName: "puce")
        img.translatesAutoresizingMaskIntoConstraints = false
        return img
    }()
    
    var label: UILabel = {
        let label = UILabel()
        label.text = "160 000"
        label.textColor = AppColor.ClearColor()
        label.font = UIFont(name: "Avenir-Book", size: 20)
        label.sizeToFit()
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    let viewBar: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor.lightGray
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.white
        addSubview(puceIcon)
        addSubview(label)
        addSubview(viewBar)
        
        switch Int(frame.width) {
        case 280:
            label.font = UIFont(name: "Avenir-Book", size: 15)
            break
        case 335:
            label.font = UIFont(name: "Avenir-Book", size: 17)
        default:
            break
        }
        
        puceIcon.widthAnchor.constraint(equalToConstant: 20).isActive = true
        puceIcon.heightAnchor.constraint(equalToConstant: 20).isActive = true
        puceIcon.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        puceIcon.rightAnchor.constraint(equalTo: rightAnchor, constant: -10).isActive = true
        
        label.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        label.rightAnchor.constraint(equalTo: puceIcon.leftAnchor, constant: -5).isActive = true
        label.leftAnchor.constraint(equalTo: leftAnchor, constant: 5).isActive = true
        
        
        viewBar.topAnchor.constraint(equalTo: bottomAnchor).isActive = true
        viewBar.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
        viewBar.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        viewBar.heightAnchor.constraint(equalToConstant: 0.3).isActive = true
        
    }
    override var isSelected: Bool {
        didSet {
            UIView.animate(withDuration: 0.3) {
                self.puceIcon.image = self.isSelected ? #imageLiteral(resourceName: "puce active") : #imageLiteral(resourceName: "puce")
            }
        }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class FetchHomeCell: UICollectionViewCell {
    var imageHome: UIImageView = {
        let img = UIImageView()
        img.contentMode = .scaleAspectFill
        img.image = #imageLiteral(resourceName: "placeholder")
        img.clipsToBounds = true
        img.translatesAutoresizingMaskIntoConstraints = false
        return img
    }()
    var homeLikeBtn: UIButton = {
        let btn = UIButton(type: UIButtonType.system)
        btn.setImage(#imageLiteral(resourceName: "unlike").withRenderingMode(.alwaysOriginal), for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    let viewBar: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor.lightGray
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(imageHome)
        addSubview(homeLikeBtn)
        addSubview(viewBar)
        
        imageHome.topAnchor.constraint(equalTo: topAnchor).isActive = true
        imageHome.widthAnchor.constraint(equalTo: widthAnchor, constant: -40).isActive = true
        imageHome.heightAnchor.constraint(equalTo: heightAnchor, constant: -10).isActive = true
        imageHome.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        
        homeLikeBtn.topAnchor.constraint(equalTo: topAnchor, constant: 2).isActive = true
        homeLikeBtn.rightAnchor.constraint(equalTo: imageHome.rightAnchor, constant: -2).isActive = true
        homeLikeBtn.heightAnchor.constraint(equalToConstant: 30).isActive = true
        homeLikeBtn.widthAnchor.constraint(equalToConstant: 35).isActive = true
        
        viewBar.topAnchor.constraint(equalTo: bottomAnchor).isActive = true
        viewBar.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
        viewBar.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        viewBar.heightAnchor.constraint(equalToConstant: 0.3).isActive = true
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class ListHomeViewCell: UICollectionViewCell {
    
    var imageHome: UIImageView = {
        let img = UIImageView()
        img.contentMode = .scaleAspectFill
        img.image = #imageLiteral(resourceName: "placeholder")
        img.clipsToBounds = true
        img.translatesAutoresizingMaskIntoConstraints = false
        return img
    }()
    var homeLikeBtn: UIButton = {
        let btn = UIButton(type: UIButtonType.system)
        btn.setImage(#imageLiteral(resourceName: "unlike").withRenderingMode(.alwaysOriginal), for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    var secteurLabel: UILabel = {
        let label = UILabel()
        label.text = "Cocody - Angre 8e Tranche"
        label.textColor = AppColor.ClearColor()
        label.font = UIFont(name: "Avenir-Light", size: 17)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    var backgroundViewFetch: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    let viewBar: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor.lightGray
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        //        backgroundColor = primaryColor
        addSubview(imageHome)
        addSubview(homeLikeBtn)
        addSubview(backgroundViewFetch)
        backgroundViewFetch.addSubview(secteurLabel)
        addSubview(viewBar)
        
        //        var priceTitleSize:CGFloat = 20
        //        var nombreDpSize:CGFloat = 13
        var secteurTitleSize:CGFloat = 15
        
        
        switch Int(frame.width) {
        case 266:
            //            priceTitleSize = 15
            //            nombreDpSize = 10
            secteurTitleSize = 13
            break
        case 345:
            secteurTitleSize = 17
            break
        default:
            break
        }
        secteurLabel.font = UIFont(name: "Avenir-Light", size: secteurTitleSize)
        
        imageHome.topAnchor.constraint(equalTo: topAnchor, constant: 5).isActive = true
        imageHome.widthAnchor.constraint(equalTo: widthAnchor, constant: -40).isActive = true
        imageHome.heightAnchor.constraint(equalTo: heightAnchor, constant: -35).isActive = true
        imageHome.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        
        homeLikeBtn.topAnchor.constraint(equalTo: imageHome.topAnchor, constant: 2).isActive = true
        homeLikeBtn.rightAnchor.constraint(equalTo: imageHome.rightAnchor, constant: -2).isActive = true
        homeLikeBtn.heightAnchor.constraint(equalToConstant: 30).isActive = true
        homeLikeBtn.widthAnchor.constraint(equalToConstant: 35).isActive = true
        
        secteurLabel.topAnchor.constraint(equalTo: imageHome.bottomAnchor, constant: 5).isActive = true
        secteurLabel.leftAnchor.constraint(equalTo: imageHome.leftAnchor, constant: 0).isActive = true
        
        viewBar.topAnchor.constraint(equalTo: bottomAnchor).isActive = true
        viewBar.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
        viewBar.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        viewBar.heightAnchor.constraint(equalToConstant: 0.3).isActive = true
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class FetchHomeWhitInfoCell: UICollectionViewCell {
    
    var home: HomeModel? {
        didSet {
            if let home = home {
                prixLabel.text = home.prix + " FCFA"
                secteurLabel.text = home.commune + " - " + home.secteur
                switch home.type {
                case "magasin":
                    nombreDPieceLabel.text = (home.possessionDuBien == "location") ? "Magasin à louer" : "Magasin à vendre"
                case "studio":
                    nombreDPieceLabel.text = (home.possessionDuBien == "location") ? "Studio à louer" : "Studio à vendre"
                default :
                    nombreDPieceLabel.text = (home.possessionDuBien == "location") ? "\(home.type.uppercaseFirstCaracters()) \(home.piece!) pièces à louer" : "\(home.type.uppercaseFirstCaracters()) \(home.piece!) pièces à vendre"
                }
                imageHome.loadImageUsingCache(withPath: home.imagePrincipale.path)
                compagnyLogoImgView.loadImageUsingCacheWithUrlString2(urlProfile: "https://kouva.net/images/isis.png")
            }
        }
    }
    
    var compagnyLogoImgView: UIImageView = {
        let img = UIImageView()
        img.contentMode = .scaleAspectFill
        img.backgroundColor = .white
        img.clipsToBounds = true
        img.isHidden = true
        img.translatesAutoresizingMaskIntoConstraints = false
        return img
    }()
    
    var imageHome: UIImageView = {
        let img = UIImageView()
        img.contentMode = .scaleAspectFill
        img.image = #imageLiteral(resourceName: "placeholder")
        img.clipsToBounds = true
        img.translatesAutoresizingMaskIntoConstraints = false
        return img
    }()

    var homeLikeBtn: UIButton = {
        let btn = UIButton(type: UIButtonType.system)
        btn.setImage(#imageLiteral(resourceName: "unlike").withRenderingMode(.alwaysOriginal), for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    var secteurLabel: UILabel = {
        let label = UILabel()
        label.text = "Cocody - Angre 8e Tranche"
        label.textColor = AppColor.ClearColor()
        label.font = UIFont(name: "Avenir-Light", size: 17)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    var prixLabel: UILabel = {
        let label = UILabel()
        label.text = "400 000 F"
        label.textColor = UIColor.black
        label.font = UIFont(name: "Avenir-Heavy", size: 15)
        label.sizeToFit()
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    var nombreDPieceLabel: UILabel = {
        let label = UILabel()
        label.text = "4 Pieces"
        label.textColor = AppColor.primaryColor()
        label.font = UIFont(name: "Avenir-Black", size: 10)
        label.sizeToFit()
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    var backgroundViewFetch: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    let viewBar: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor.lightGray
        v.isHidden = true
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(imageHome)
        addSubview(homeLikeBtn)
        addSubview(backgroundViewFetch)
        backgroundViewFetch.addSubview(secteurLabel)
        backgroundViewFetch.addSubview(nombreDPieceLabel)
        backgroundViewFetch.addSubview(prixLabel)
        addSubview(viewBar)
        addSubview(compagnyLogoImgView)
        
        var priceTitleSize:CGFloat = 20
        var nombreDpSize:CGFloat = 13
        var secteurTitleSize:CGFloat = 15
        
        switch Int(frame.width) {
        case 320:
            priceTitleSize = 15
            nombreDpSize = 10
            secteurTitleSize = 13
            break
        case 375:
            priceTitleSize = 18
            nombreDpSize = 13
            secteurTitleSize = 15
            break
        default:
            break
        }
        
        prixLabel.font = UIFont(name: "Avenir-Heavy", size: priceTitleSize)
        nombreDPieceLabel.font = UIFont(name: "Avenir-Black", size: nombreDpSize)
        secteurLabel.font = UIFont(name: "Avenir-Light", size: secteurTitleSize)
        
        imageHome.topAnchor.constraint(equalTo: topAnchor).isActive = true
        imageHome.widthAnchor.constraint(equalTo: widthAnchor, constant: 0).isActive = true
        imageHome.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 3/4).isActive = true
        imageHome.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        
        homeLikeBtn.topAnchor.constraint(equalTo: topAnchor, constant: 2).isActive = true
        homeLikeBtn.rightAnchor.constraint(equalTo: imageHome.rightAnchor, constant: -2).isActive = true
        homeLikeBtn.heightAnchor.constraint(equalToConstant: 30).isActive = true
        homeLikeBtn.widthAnchor.constraint(equalToConstant: 35).isActive = true
        
        compagnyLogoImgView.bottomAnchor.constraint(equalTo: imageHome.bottomAnchor, constant: -5).isActive = true
        compagnyLogoImgView.leftAnchor.constraint(equalTo: imageHome.leftAnchor, constant: 10).isActive = true
        compagnyLogoImgView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        compagnyLogoImgView.widthAnchor.constraint(equalTo: imageHome.widthAnchor, multiplier: 1/4).isActive = true
        
        backgroundViewFetch.topAnchor.constraint(equalTo: imageHome.bottomAnchor).isActive = true
        backgroundViewFetch.heightAnchor.constraint(equalToConstant: 60).isActive = true
        backgroundViewFetch.widthAnchor.constraint(equalTo: imageHome.widthAnchor).isActive = true
        backgroundViewFetch.centerXAnchor.constraint(equalTo: imageHome.centerXAnchor).isActive = true
        
        prixLabel.topAnchor.constraint(equalTo: backgroundViewFetch.topAnchor, constant: 0).isActive = true
        prixLabel.leftAnchor.constraint(equalTo: backgroundViewFetch.leftAnchor, constant: 0).isActive = true
        
        secteurLabel.topAnchor.constraint(equalTo: prixLabel.bottomAnchor, constant: 0).isActive = true
        secteurLabel.leftAnchor.constraint(equalTo: backgroundViewFetch.leftAnchor, constant: 0).isActive = true
        
        nombreDPieceLabel.topAnchor.constraint(equalTo: secteurLabel.bottomAnchor, constant: 0).isActive = true
        nombreDPieceLabel.leftAnchor.constraint(equalTo: backgroundViewFetch.leftAnchor, constant: 0).isActive = true
        
        viewBar.topAnchor.constraint(equalTo: bottomAnchor).isActive = true
        viewBar.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
        viewBar.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        viewBar.heightAnchor.constraint(equalToConstant: 0.3).isActive = true
        
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class LegendCell: UICollectionViewCell {
    var legendLike: UIButton = {
        let btn = UIButton(type: UIButtonType.system)
        btn.setImage(#imageLiteral(resourceName: "unlike").withRenderingMode(.alwaysOriginal), for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    var legendTitle: UILabel = {
        let label = UILabel()
        label.text = "biens a visiter"
        label.textColor = AppColor.ClearColor()
        label.font = UIFont(name: "Avenir-Book", size: 15)
        label.sizeToFit()
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(legendLike)
        legendLike.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        legendLike.leftAnchor.constraint(equalTo: leftAnchor, constant: 0).isActive = true
        legendLike.heightAnchor.constraint(equalToConstant: 25).isActive = true
        legendLike.widthAnchor.constraint(equalToConstant: 30).isActive = true
        
        var leftTitle:CGFloat = 20
        switch frame.width {
        case 135.0:
            leftTitle = 15
            break
        default:
            break
        }
        addSubview(legendTitle)
        legendTitle.leftAnchor.constraint(equalTo: legendLike.rightAnchor, constant: leftTitle).isActive = true
        legendTitle.centerYAnchor.constraint(equalTo: legendLike.centerYAnchor).isActive = true
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


class FavorisHomeCell: UICollectionViewCell {
    
    var imageHome: UIImageView = {
        let img = UIImageView()
        img.contentMode = .scaleAspectFill
        img.image = #imageLiteral(resourceName: "placeholder")
        img.clipsToBounds = true
        img.translatesAutoresizingMaskIntoConstraints = false
        return img
    }()
    var homeLikeBtn: UIButton = {
        let btn = UIButton(type: UIButtonType.system)
        btn.setImage(#imageLiteral(resourceName: "unlike").withRenderingMode(.alwaysOriginal), for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    var secteurLabel: UILabel = {
        let label = UILabel()
        label.text = "Cocody - Angre 8e Tranche"
        label.textColor = AppColor.ClearColor()
        label.font = UIFont(name: "Avenir-Light", size: 17)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    var prixLabel: UILabel = {
        let label = UILabel()
        label.text = "400 000 F"
        label.textColor = AppColor.primaryColor()
        label.font = UIFont(name: "Avenir-Heavy", size: 15)
        label.sizeToFit()
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    var nombreDPieceLabel: UILabel = {
        let label = UILabel()
        label.text = "4 Pieces"
        label.textColor = AppColor.ClearColor()
        label.font = UIFont(name: "Avenir-Black", size: 10)
        label.sizeToFit()
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    var backgroundViewFetch: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    var homeTakenView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(r: 0, g: 0, b: 0, alpha: 0.6)
        view.translatesAutoresizingMaskIntoConstraints = false
        
        let label = UILabel()
        label.text = "déjà pris"
        label.textColor = UIColor.white
        label.font = UIFont(name: "Avenir-Black", size: 40)
        label.sizeToFit()
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(label)
        label.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        label.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(imageHome)
        addSubview(homeLikeBtn)
        addSubview(backgroundViewFetch)
        backgroundViewFetch.addSubview(secteurLabel)
        backgroundViewFetch.addSubview(nombreDPieceLabel)
        backgroundViewFetch.addSubview(prixLabel)
        
        var priceTitleSize:CGFloat = 20
        var nombreDpSize:CGFloat = 13
        var secteurTitleSize:CGFloat = 15
        
        
        switch Int(frame.width) {
        case 266:
            priceTitleSize = 15
            nombreDpSize = 10
            secteurTitleSize = 17
            break
        default:
            break
        }
        
        prixLabel.font = UIFont(name: "Avenir-Heavy", size: priceTitleSize)
        nombreDPieceLabel.font = UIFont(name: "Avenir-Black", size: nombreDpSize)
        secteurLabel.font = UIFont(name: "Avenir-Light", size: secteurTitleSize)
        
        imageHome.topAnchor.constraint(equalTo: topAnchor).isActive = true
        imageHome.widthAnchor.constraint(equalTo: widthAnchor, constant: 0).isActive = true
        imageHome.heightAnchor.constraint(equalTo: heightAnchor, constant: -70).isActive = true
        imageHome.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        
        homeLikeBtn.topAnchor.constraint(equalTo: topAnchor, constant: 2).isActive = true
        homeLikeBtn.rightAnchor.constraint(equalTo: imageHome.rightAnchor, constant: -2).isActive = true
        homeLikeBtn.heightAnchor.constraint(equalToConstant: 30).isActive = true
        homeLikeBtn.widthAnchor.constraint(equalToConstant: 35).isActive = true
        
        backgroundViewFetch.topAnchor.constraint(equalTo: imageHome.bottomAnchor).isActive = true
        backgroundViewFetch.heightAnchor.constraint(equalToConstant: 60).isActive = true
        backgroundViewFetch.widthAnchor.constraint(equalTo: widthAnchor, constant: 0).isActive = true
        backgroundViewFetch.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        
        prixLabel.topAnchor.constraint(equalTo: backgroundViewFetch.topAnchor, constant: 0).isActive = true
        prixLabel.leftAnchor.constraint(equalTo: backgroundViewFetch.leftAnchor, constant: 0).isActive = true
        
        secteurLabel.topAnchor.constraint(equalTo: prixLabel.bottomAnchor, constant: 0).isActive = true
        secteurLabel.leftAnchor.constraint(equalTo: backgroundViewFetch.leftAnchor, constant: 0).isActive = true
        
        nombreDPieceLabel.topAnchor.constraint(equalTo: secteurLabel.bottomAnchor, constant: 0).isActive = true
        nombreDPieceLabel.leftAnchor.constraint(equalTo: backgroundViewFetch.leftAnchor, constant: 0).isActive = true
        
        
        addSubview(homeTakenView)
        addContraintAllScreen(view: homeTakenView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class PaymentCell: UICollectionViewCell {
    
    var Title: UILabel = {
        let label = UILabel()
        label.text = "LOYER"
        label.textColor = UIColor.white
        label.font = UIFont(name: "Source-Sans-Pro-Semiblod", size: 18)
        //label.attributedText = NSMutableAttributedString(string: label.text!, attributes: [])
        label.sizeToFit()
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    var SubTitle: UILabel = {
        let label = UILabel()
        label.text = "360 000"
        label.textColor = UIColor.yellow
        label.font = UIFont(name: "Source-Sans-Pro-Semiblod", size: 18)
        label.sizeToFit()
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = AppColor.primaryColor()
        addSubview(Title)
        addSubview(SubTitle)
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-[title]-[subtitle]-|", options: [], metrics: nil, views: ["title":Title,"subtitle":SubTitle]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-[view]-|", options: [], metrics: nil, views: ["view":Title]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-[view]-|", options: [], metrics: nil, views: ["view":SubTitle]))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class GalleryCell: UICollectionViewCell {
    var imageGallery: UIImageView = {
        let img = UIImageView()
        img.image = #imageLiteral(resourceName: "placeholder")
        img.contentMode = .scaleToFill
        img.translatesAutoresizingMaskIntoConstraints = false
        return img
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(imageGallery)
        
        backgroundColor = UIColor.black
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[view]|", options: [], metrics: nil, views: ["view":imageGallery]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[view]|", options: [], metrics: nil, views: ["view":imageGallery]))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class ImageShowCell: UICollectionViewCell {
    var imageGallery: UIImageView = {
        let img = UIImageView()
        img.contentMode = .scaleAspectFill
        img.clipsToBounds = true
        img.translatesAutoresizingMaskIntoConstraints = false
        return img
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(imageGallery)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class userProfileMenuCell: UICollectionViewCell {
    var puceIcon: UIImageView = {
        let img = UIImageView()
        img.image = UIImage(named: "icon_profile")?.withRenderingMode(.alwaysOriginal)
        img.translatesAutoresizingMaskIntoConstraints = false
        return img
    }()
    
    var label: UILabel = {
        let label = UILabel()
        label.text = "160 000"
        label.textColor = AppColor.black()
        label.font = UIFont(name: "Avenir-Heavy", size: 15)
        label.sizeToFit()
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    var separateView: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor(r: 226, g: 226, b: 226)
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(puceIcon)
        addSubview(label)
        addSubview(separateView)
        
        puceIcon.widthAnchor.constraint(equalToConstant: 6).isActive = true
        puceIcon.heightAnchor.constraint(equalToConstant: 10).isActive = true
        puceIcon.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        puceIcon.rightAnchor.constraint(equalTo: rightAnchor, constant: 0).isActive = true
        
        label.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        label.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        
        separateView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        separateView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        separateView.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
        separateView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

class TextFieldCell: UICollectionViewCell {
    let label: UILabel = {
        let label = UILabel()
        label.text = "EMAIL"
        label.textColor = AppColor.primaryColor()
        label.textAlignment = .left
        label.font = UIFont(name: "Avenir-Heavy", size: 15)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    let showPassword: UIButton = {
        let btn = UIButton()
        btn.setTitle("Afficher", for: .normal)
        btn.setTitleColor(AppColor.primaryColor(), for: .normal)
        btn.isHidden = true
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    let input: UITextField = {
        let Input = UITextField()
        Input.textColor = AppColor.primaryColor()
        Input.backgroundColor = UIColor.white
        Input.translatesAutoresizingMaskIntoConstraints = false
        return Input
    }()
    let viewBar: UIView = {
        let v = UIView()
        v.backgroundColor = AppColor.primaryColor()
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.white
        addSubview(label)
        addSubview(input)
        addSubview(showPassword)
        
        addSubview(viewBar)
        label.topAnchor.constraint(equalTo: topAnchor).isActive = true
        label.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        
        showPassword.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        showPassword.centerYAnchor.constraint(equalTo: label.centerYAnchor).isActive = true
        
        input.topAnchor.constraint(equalTo: label.bottomAnchor).isActive = true
        input.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
        input.heightAnchor.constraint(equalToConstant: 40).isActive = true
        input.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        
        viewBar.topAnchor.constraint(equalTo: input.bottomAnchor).isActive = true
        viewBar.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
        viewBar.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        viewBar.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

class NavigationBarTypeCell: UICollectionViewCell {
    
    let label: UILabel = {
        let label = UILabel()
        label.text = "LOCATION"
        label.textColor = UIColor.lightGray
        label.font = UIFont(name: "Avenir-Black", size: 12)
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override var isHighlighted: Bool {
        didSet {
            label.tintColor = isHighlighted ? AppColor.primaryColor() : UIColor.lightGray
        }
    }
    
    override var isSelected: Bool {
        didSet {
            label.textColor = isSelected ? AppColor.primaryColor() : UIColor.lightGray
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.white
        addSubview(label)
        
        label.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        label.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        
        
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class detailHomeCollectionCell: UICollectionViewCell {
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}



