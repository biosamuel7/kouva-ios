//
//  HeaderViews.swift
//  Kouva
//
//  Created by Bio'S on 04/07/2019.
//  Copyright © 2019 Kouva. All rights reserved.
//

import UIKit

class FavorisHeaderView: UICollectionReusableView {

    let enteteTitleLabel: UILabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        var titleSize:CGFloat = 0
        var subTitleSize:CGFloat = 0
        
        switch frame.width {
        case 320.0:
            titleSize = 20
            subTitleSize = 13
            break
        case 375:
            titleSize = 25
            subTitleSize = 15
            break
        case 414:
            titleSize = 30
            subTitleSize = 17
            break
        default:
            
            break
        }
        enteteTitleLabel.text = "Enregistré"
        enteteTitleLabel.textColor = AppColor.black()
        enteteTitleLabel.textAlignment = .left
        enteteTitleLabel.font = UIFont(name: "Avenir-Black", size: titleSize)
        enteteTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        addSubview(enteteTitleLabel)
        enteteTitleLabel.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true
        enteteTitleLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 30).isActive = true
        
        let subTitleLabel: UILabel = UILabel()
        subTitleLabel.text = "Vous avez aucun enregistrement"
        subTitleLabel.font = UIFont(name: "Avenir-Book", size: subTitleSize)
        subTitleLabel.textColor = AppColor.black()
        subTitleLabel.textAlignment = .left
        subTitleLabel.isHidden = true
        subTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        addSubview(subTitleLabel)
        subTitleLabel.topAnchor.constraint(equalTo: enteteTitleLabel.bottomAnchor, constant: 5).isActive = true
        subTitleLabel.widthAnchor.constraint(equalTo: widthAnchor, constant: -40).isActive = true
        subTitleLabel.leftAnchor.constraint(equalTo: enteteTitleLabel.leftAnchor).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
