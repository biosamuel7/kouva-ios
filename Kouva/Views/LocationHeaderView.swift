//
//  LocationHeaderView.swift
//  Kouva
//
//  Created by Bio'S on 03/06/2019.
//  Copyright © 2019 Kouva. All rights reserved.
//

import UIKit

class LocationHeaderView: UICollectionReusableView {
    
    var headerLocationLabel: UILabel = {
        let label = UILabel()
        label.text = "Cocody"
        label.textColor = .black
        label.font = UIFont(name: "Avenir-Black", size: 15)
        //label.attributedText = NSMutableAttributedString(string: label.text!, attributes: [])
        label.sizeToFit()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        //        backgroundColor = UIColor(r: 240, g: 244, b: 248)
        addSubview(headerLocationLabel)
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-20-[label]|", options: [], metrics: nil, views: ["label":headerLocationLabel]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[label]|", options: [], metrics: nil, views: ["label":headerLocationLabel]))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}


