//
//  NavBarView.swift
//  Kouva
//
//  Created by Bio'S on 03/06/2019.
//  Copyright © 2019 Kouva. All rights reserved.
//

import UIKit

class NavBarView: UIView, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    let navigationCollection: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        let collection = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        collection.backgroundColor = AppColor.primaryColor()
        collection.translatesAutoresizingMaskIntoConstraints = false
        return collection
    }()
    var dimension:CGRect = CGRect.zero
    
    var showDetailHomeVC:ShowDetailHomeViewController?
    
    var table:[String] = []
    
    let viewBar: UIView = {
        let v = UIView()
        v.backgroundColor = AppColor.primaryColor()
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    var viewBarHorizontalConstraint:NSLayoutConstraint?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        navigationCollection.register(NavigationBarTypeCell.self, forCellWithReuseIdentifier: "cell")
        navigationCollection.delegate = self
        navigationCollection.dataSource = self
        
        SetUpConstraint()
        
        
        let navIndex = IndexPath(item: 0, section: 0)
        self.navigationCollection.selectItem(at: navIndex, animated: false, scrollPosition: .centeredHorizontally)
        
        
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! NavigationBarTypeCell
        cell.label.text = table[indexPath.item]
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 2, height: collectionView.frame.height)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        showDetailHomeVC?.getIndex(index: indexPath.item)
    }
    
    func SetUpConstraint() {
        
        addSubview(navigationCollection)
        navigationCollection.topAnchor.constraint(equalTo: topAnchor).isActive = true
        navigationCollection.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
        navigationCollection.heightAnchor.constraint(equalTo: heightAnchor).isActive = true
        navigationCollection.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        
        addSubview(viewBar)
        viewBar.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        viewBar.widthAnchor.constraint(equalToConstant: frame.width / 2).isActive = true
        viewBar.heightAnchor.constraint(equalToConstant: 2).isActive = true
        viewBarHorizontalConstraint = viewBar.leftAnchor.constraint(equalTo: leftAnchor)
        viewBarHorizontalConstraint?.isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}



