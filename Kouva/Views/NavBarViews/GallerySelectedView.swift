//
//  GallerySelectedView.swift
//  Kouva
//
//  Created by Bio'S on 03/06/2019.
//  Copyright © 2019 Kouva. All rights reserved.
//

import UIKit

class GallerySelectedView: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var urlSelected:String?
    var urlData:[ImageHome]!
    var imageGallery: UIImageView = {
        let img = UIImageView()
        img.image = UIImage(named: "2.jpg")
        img.contentMode = .scaleAspectFill
        img.clipsToBounds = true
        img.translatesAutoresizingMaskIntoConstraints = false
        return img
    }()
    var imageShowCollection: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        let collection = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collection.backgroundColor = UIColor.white
        collection.isPagingEnabled = true
        collection.translatesAutoresizingMaskIntoConstraints = false
        return collection
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.black
        view.addSubview(imageShowCollection)
        imageShowCollection.register(GalleryCell.self, forCellWithReuseIdentifier: "cell")
        imageShowCollection.dataSource = self
        imageShowCollection.delegate = self
        
        SetUpConstraint()
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return urlData.count
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! GalleryCell
        cell.imageGallery.loadImageUsingCache(withPath: urlData[indexPath.row].path)
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width, height: collectionView.bounds.height)
    }
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
    }
    func SetUpConstraint()  {
        imageShowCollection.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        imageShowCollection.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        imageShowCollection.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        imageShowCollection.heightAnchor.constraint(equalToConstant: view.bounds.height / 2.5).isActive = true
        
    }
}

