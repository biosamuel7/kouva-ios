//
//  GalleryView.swift
//  Kouva
//
//  Created by Bio'S on 03/06/2019.
//  Copyright © 2019 Kouva. All rights reserved.
//

import UIKit
import Firebase

class GalleryView: UIView, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    
    
    var showDetailHomeVC:ShowDetailHomeViewController?
    var galleryCollectionView: UICollectionView = {
        let frame = CGRect.zero
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 1
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = .vertical
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 75, right: 0)
        let collection = UICollectionView(frame: frame, collectionViewLayout: layout)
        collection.backgroundColor = UIColor.white
        collection.allowsSelection = true
        collection.translatesAutoresizingMaskIntoConstraints = false
        return collection
    }()
    var data:[ImageHome] = []
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(galleryCollectionView)
        
        galleryCollectionView.register(GalleryCell.self, forCellWithReuseIdentifier: "cell")
        galleryCollectionView.dataSource = self
        galleryCollectionView.delegate = self
        
        //SET UP CONSTRAINT OF GALLERYVIEW
        galleryCollectionView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        galleryCollectionView.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
        galleryCollectionView.heightAnchor.constraint(equalTo: heightAnchor, constant: 0).isActive = true
        galleryCollectionView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! GalleryCell
        let url = data[indexPath.row]
        cell.imageGallery.loadImageUsingCache(withPath: url.path)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let GallerySelected = GallerySelectedView()
        GallerySelected.urlData = data
        GallerySelected.urlSelected = data[indexPath.row].path
        //        SuperClass.navigationController?.pushViewController(GallerySelected, animated: true)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let dimension = (bounds.width / 3) - 1
        return CGSize(width: dimension, height: dimension)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

