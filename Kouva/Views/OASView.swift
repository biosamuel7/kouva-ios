//
//  OASView.swift
//  Kouva
//
//  Created by Bio'S on 27/06/2019.
//  Copyright © 2019 Kouva. All rights reserved.
//

import UIKit
import Firebase

class OASView: UIView, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    var mainViewController: MainViewController?
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return homeOASData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! TypeDeBienCell
        cell.showOpportunityASaisirCell()
        cell.homeLikeBtn.tag = indexPath.item
        cell.homeLikeBtn.addTarget(self, action: #selector(likeHomeHandle(btn:)), for: .touchUpInside)
        let home = homeOASData[indexPath.item]
        cell.home = home
        DataBaseService.instance.checkIfHomeIsLiked(likeBtn: cell.homeLikeBtn, home: home)
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var width = frame.width / 2.5
    
        width = width + 100
        if frame.height == 348.8 {
            width = frame.width / 1.5
        }
        
        return CGSize(width: width, height: collectionView.frame.height)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let viewController = ShowDetailHomeViewController()
        viewController.homeSelected = homeOASData[indexPath.item]
        mainViewController?.present(viewController, animated: true, completion: {
            collectionView.deselectItem(at: indexPath, animated: false)
        })
    }
    
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 10)
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 10
        layout.minimumInteritemSpacing = 0
        
        let collection = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        collection.backgroundColor = .white
        collection.translatesAutoresizingMaskIntoConstraints = false
        collection.showsHorizontalScrollIndicator = false
        return collection
    }()
    
    let listingHomeShimmerView: UIView = {
        let v = UIView()
        v.isHidden = false
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    var homeOASData = [HomeModel]()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(collectionView)
        addContraintAllScreen(view: collectionView)
        collectionView.register(TypeDeBienCell.self, forCellWithReuseIdentifier: "cell")
        collectionView.delegate = self
        collectionView.dataSource = self
        shimmerView()
        //loadData()
    }
    //like home or deleting
    @objc func likeHomeHandle(btn: UIButton){
        
        let item = btn.tag
        let indexPath = IndexPath(item: item, section: 0)
        
        let home = homeOASData[indexPath.item]
        if home.isLiked {
            if let cell = collectionView.cellForItem(at: indexPath) as? TypeDeBienCell {
                cell.homeLikeBtn.setImage(#imageLiteral(resourceName: "unlike").withRenderingMode(.alwaysOriginal), for: .normal)
                home.isLiked = false
                DataBaseService.instance.userWhishCollection.document(home.homeID).delete()
                
            }
        } else {
            if UserDefaults.standard.value(forKey: "userID") != nil {
                if let cell = collectionView.cellForItem(at: indexPath) as? TypeDeBienCell {
                    cell.homeLikeBtn.setImage(#imageLiteral(resourceName: "like").withRenderingMode(.alwaysOriginal), for: .normal)
                    let value:[String:Any] = ["typeDuBien":home.type,"possessionDuBien":home.possessionDuBien,"state":"wish"]
                    DataBaseService.instance.userWhishCollection.document(home.homeID).setData(value)
                    home.isLiked = true
                }
            } else {
                let vc = UserLoginViewController()
                vc.dissMissViewBtn.isHidden = false
                if let _mainViewController = mainViewController {
                    _mainViewController.present(vc, animated: true, completion: nil)
                }
            }
        }
        
    }
    
    func shimmerView() {
        
        addSubview(listingHomeShimmerView)
        addContraintAllScreen(view: listingHomeShimmerView)
        
        let titleHeigh:CGFloat = 10
        
        var width = frame.width / 2.5
        width = width + 100
        if frame.height == 348.8 {
            width = frame.width / 1.5
        }
        
        let imgShimme1 = UIView()
        imgShimme1.backgroundColor = UIColor(r: 241, g: 241, b: 241)
        imgShimme1.translatesAutoresizingMaskIntoConstraints = false
        listingHomeShimmerView.addSubview(imgShimme1)
        imgShimme1.topAnchor.constraint(equalTo: listingHomeShimmerView.topAnchor, constant: 0).isActive = true
        imgShimme1.leftAnchor.constraint(equalTo: leftAnchor, constant: 20).isActive = true
        imgShimme1.widthAnchor.constraint(equalToConstant: width).isActive = true
        imgShimme1.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 2/3).isActive = true
        
        let priceShimmer1 = UIView()
        priceShimmer1.backgroundColor = UIColor(r: 241, g: 241, b: 241)
        priceShimmer1.translatesAutoresizingMaskIntoConstraints = false
        listingHomeShimmerView.addSubview(priceShimmer1)
        priceShimmer1.topAnchor.constraint(equalTo: imgShimme1.bottomAnchor, constant: 5).isActive = true
        priceShimmer1.leftAnchor.constraint(equalTo: imgShimme1.leftAnchor).isActive = true
        priceShimmer1.widthAnchor.constraint(equalTo: imgShimme1.widthAnchor, multiplier: 1/2).isActive = true
        priceShimmer1.heightAnchor.constraint(equalToConstant: titleHeigh).isActive = true
        
        let secteurShimmer1 = UIView()
        secteurShimmer1.backgroundColor = UIColor(r: 241, g: 241, b: 241)
        secteurShimmer1.translatesAutoresizingMaskIntoConstraints = false
        listingHomeShimmerView.addSubview(secteurShimmer1)
        secteurShimmer1.topAnchor.constraint(equalTo: priceShimmer1.bottomAnchor, constant: 5).isActive = true
        secteurShimmer1.leftAnchor.constraint(equalTo: imgShimme1.leftAnchor).isActive = true
        secteurShimmer1.widthAnchor.constraint(equalTo: imgShimme1.widthAnchor, constant: -40).isActive = true
        secteurShimmer1.heightAnchor.constraint(equalToConstant: titleHeigh).isActive = true
        
        let pieceShimmer1 = UIView()
        pieceShimmer1.backgroundColor = UIColor(r: 241, g: 241, b: 241)
        pieceShimmer1.translatesAutoresizingMaskIntoConstraints = false
        listingHomeShimmerView.addSubview(pieceShimmer1)
        pieceShimmer1.topAnchor.constraint(equalTo: secteurShimmer1.bottomAnchor, constant: 5).isActive = true
        pieceShimmer1.leftAnchor.constraint(equalTo: imgShimme1.leftAnchor).isActive = true
        pieceShimmer1.widthAnchor.constraint(equalTo: imgShimme1.widthAnchor, multiplier: 1/4).isActive = true
        pieceShimmer1.heightAnchor.constraint(equalToConstant: titleHeigh).isActive = true
        
        let imgShimme2 = UIView()
        imgShimme2.backgroundColor = UIColor(r: 241, g: 241, b: 241)
        imgShimme2.translatesAutoresizingMaskIntoConstraints = false
        listingHomeShimmerView.addSubview(imgShimme2)
        imgShimme2.topAnchor.constraint(equalTo: listingHomeShimmerView.topAnchor, constant: 0).isActive = true
        imgShimme2.leftAnchor.constraint(equalTo: imgShimme1.rightAnchor, constant: 10).isActive = true
        imgShimme2.widthAnchor.constraint(equalTo: imgShimme1.widthAnchor).isActive = true
        imgShimme2.heightAnchor.constraint(equalTo: imgShimme1.heightAnchor).isActive = true

        let priceShimmer2 = UIView()
        priceShimmer2.backgroundColor = UIColor(r: 241, g: 241, b: 241)
        priceShimmer2.translatesAutoresizingMaskIntoConstraints = false
        listingHomeShimmerView.addSubview(priceShimmer2)
        priceShimmer2.topAnchor.constraint(equalTo: priceShimmer1.topAnchor).isActive = true
        priceShimmer2.leftAnchor.constraint(equalTo: imgShimme2.leftAnchor).isActive = true
        priceShimmer2.widthAnchor.constraint(equalTo: priceShimmer1.widthAnchor).isActive = true
        priceShimmer2.heightAnchor.constraint(equalToConstant: titleHeigh).isActive = true
        
        let secteurShimmer2 = UIView()
        secteurShimmer2.backgroundColor = UIColor(r: 241, g: 241, b: 241)
        secteurShimmer2.translatesAutoresizingMaskIntoConstraints = false
        listingHomeShimmerView.addSubview(secteurShimmer2)
        secteurShimmer2.topAnchor.constraint(equalTo: secteurShimmer1.topAnchor).isActive = true
        secteurShimmer2.leftAnchor.constraint(equalTo: imgShimme2.leftAnchor).isActive = true
        secteurShimmer2.widthAnchor.constraint(equalTo: secteurShimmer1.widthAnchor).isActive = true
        secteurShimmer2.heightAnchor.constraint(equalToConstant: titleHeigh).isActive = true

        let pieceShimmer2 = UIView()
        pieceShimmer2.backgroundColor = UIColor(r: 241, g: 241, b: 241)
        pieceShimmer2.translatesAutoresizingMaskIntoConstraints = false
        listingHomeShimmerView.addSubview(pieceShimmer2)
        pieceShimmer2.topAnchor.constraint(equalTo: pieceShimmer1.topAnchor).isActive = true
        pieceShimmer2.leftAnchor.constraint(equalTo: imgShimme2.leftAnchor).isActive = true
        pieceShimmer2.widthAnchor.constraint(equalTo: pieceShimmer1.widthAnchor).isActive = true
        pieceShimmer2.heightAnchor.constraint(equalTo: pieceShimmer1.heightAnchor).isActive = true
    }
    func loadData() {
        DataBaseService.instance.homeOASCollection.limit(to: 5).getDocuments { (snapshot, error) in
            if let documents = snapshot?.documents {
                             
                             for document in documents {
                                 if let home = HomeModel(data: document.data()) {
                                     home.homeID = document.documentID
                                     if home.stateHome {
                                         self.homeOASData.append(home)
                                     }
                                 }
                             }
                     }
                                 
                     DispatchQueue.main.async {
                         self.collectionView.reloadData()
                         self.listingHomeShimmerView.isHidden = true
                     }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


