//
//  PieceListingsHomeView.swift
//  Kouva
//
//  Created by Bio'S on 03/06/2019.
//  Copyright © 2019 Kouva. All rights reserved.
//

import UIKit

class PieceListingsHomeView: UIView, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var listingsHomeVC: ListingHomesViewController?
    var pieceData:[Int] = []
    let pieceCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 0)
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 5
        layout.minimumInteritemSpacing = 0
        
        let collection = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        collection.backgroundColor = .white
        collection.translatesAutoresizingMaskIntoConstraints = false
        return collection
    }()
    var heighBounds:CGFloat?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        addSubview(pieceCollectionView)
        addContraintAllScreen(view: pieceCollectionView)
        
        pieceCollectionView.register(NombreDePieceCell.self, forCellWithReuseIdentifier: "cell")
        pieceCollectionView.delegate = self
        pieceCollectionView.dataSource = self
        
        
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pieceData.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! NombreDePieceCell
        cell.nombreDePiceLabel.text = String(pieceData[indexPath.item])
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var height = collectionView.frame.height - 20
        
        if heighBounds! == 812.0 {
            height = height - 20
        }
        
        return CGSize(width: collectionView.frame.width / 8, height: height)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        listingsHomeVC?.getHomePieceIndex(sectionPiece: indexPath.item)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

