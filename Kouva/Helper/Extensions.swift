//
//  Extensions.swift
//  Kouva
//
//  Created by Bio'S on 03/06/2019.
//  Copyright © 2019 Kouva. All rights reserved.
//

import UIKit
import FirebaseStorage

let imageCache = NSCache<AnyObject, AnyObject>()
extension UIImageView {
    
    
    func loadImageUsingCache(withPath path: String) {
        // avoid flash during loading image, print blank during it
        self.image = #imageLiteral(resourceName: "placeholder")
        
        //check for the first time if image is cached
        if let cachedImage = imageCache.object(forKey: path as AnyObject) as? UIImage {
            self.image = cachedImage
            return
        }
        
        let ref = Storage.storage().reference(withPath: path)
        ref.getData(maxSize: 1 * 1024 * 1024, completion: { (data, error) in
            if let error = error {
                print(error)
            } else {
                DispatchQueue.main.async {
                    if let downloadedImage = UIImage(data: data!) {
                        imageCache.setObject(downloadedImage, forKey: path as AnyObject)
                        self.image = downloadedImage
                    }
                }
            }
        })
    }
    
    func loadImageUsingCacheWithUrlString2(urlProfile: String) {
        let url = URL(string: urlProfile)
        
        // avoid flash during loading image, print blank during it
        self.image = #imageLiteral(resourceName: "placeholder")
        
        //check for the first time if image is cached
        if let cachedImage = imageCache.object(forKey: urlProfile as AnyObject) as? UIImage {
            self.image = cachedImage
            return
        }
        
        //download image using url
        URLSession.shared.dataTask(with: url!) { (data, respone, error) in
            
            if error != nil {
                print(error!.localizedDescription)
                return
            }
            
            DispatchQueue.main.async {
                
                if let downloadedImage = UIImage(data: data!) {
                    imageCache.setObject(downloadedImage, forKey: urlProfile as AnyObject)
                    
                    self.image = downloadedImage
                }
            }
            
            }.resume()
    }
}

extension UIColor{
    convenience init(r: CGFloat, g: CGFloat, b:CGFloat, alpha: CGFloat = 1.0) {
        self.init(red: r/255, green: g/255, blue: b/255, alpha: alpha)
    }
}

extension UIView {
    func addContraintAllScreen(view: UIView) {
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[v]|", options: [], metrics: nil, views: ["v":view]))
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[v]|", options: [], metrics: nil, views: ["v":view]))
    }
}
extension UIButton {
    func SetTitleBtnAndFont(title:String,size:CGFloat = 20,titleColor:UIColor = UIColor.white,font:String = "Avenir-Black") {
        let attribut:NSAttributedString = NSAttributedString(string: title, attributes: [NSAttributedStringKey.font:UIFont(name: font, size: size)!,NSAttributedStringKey.foregroundColor:titleColor])
        self.setAttributedTitle(attribut, for: .normal)
    }
}

extension String {
    func estimateheight(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: [.usesLineFragmentOrigin,.usesFontLeading], attributes: [NSAttributedStringKey.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    func uppercaseFirstCaracters() -> String {
        return self.prefix(1).capitalized + self.dropFirst().lowercased()
    }
    mutating func uppercaseFirstCaracters() {
        self = self.uppercaseFirstCaracters()
    }
}

extension Notification.Name {
    static let didShowUserAddNumberView = Notification.Name("showUserAddNumberView")
    static let didSignUserPhoneNumberByVerificationCode = Notification.Name("signUserPhoneNumberByVerificationCode")
    static let didLoadUserFavoris = Notification.Name("didLoadUserFavoris")
    static let didUserConnexion = Notification.Name("didUserConnexion")
}
