//
//  Helpers.swift
//  Kouva
//
//  Created by Bio'S on 03/06/2019.
//  Copyright © 2019 Kouva. All rights reserved.
//

import Foundation
import UIKit
import FirebaseAuth


class Alertes {
    
    static func AlerteError(error:String,VC:UIViewController){
        let Alert = UIAlertController(title: "Attention", message: error, preferredStyle: .alert)
        let action = UIAlertAction(title: "ok", style: .cancel, handler: nil)
        Alert.addAction(action)
        VC.present(Alert, animated: true, completion: nil)
    }
}
class SaveUserSecure {
    static func saveID(userID:String){
        UserDefaults.standard.set(userID, forKey: "userID")
        UserDefaults.standard.synchronize()
    }
    
    static func RemoveUser(){
        UserDefaults.standard.removeObject(forKey: "userID")
        UserDefaults.standard.removeObject(forKey: "userEmail")
        UserDefaults.standard.removeObject(forKey: "userFirstName")
        UserDefaults.standard.removeObject(forKey: "userName")
        UserDefaults.standard.removeObject(forKey: "verificationID")
        UserDefaults.standard.synchronize()
    }
    
}

//var primaryColor:UIColor = {
//
//    return UIColor(r: 80, g: 69, b: 146)
//}()

class AppColor {
    static func black() -> UIColor {
        return UIColor(r: 46, g: 47, b: 65)
    }
    static func primaryColor() -> UIColor {
        return UIColor(r: 88, g: 10, b: 141)
    }
    
    static func blackColorTitle() -> UIColor {
        return UIColor(r: 46, g: 47, b: 65)
    }
    
    static func priceColor() -> UIColor {
        return UIColor(r: 51, g: 10, b: 78)
    }
    
    static func ClearColor() -> UIColor {
        return UIColor(r: 91, g: 111, b: 129)
    }
    
    static func jauneColor() -> UIColor {
        return UIColor(r: 253, g: 241, b: 7)
    }
    
    static func titleGrayColor() -> UIColor {
        return UIColor(r: 87, g: 87, b: 87)
    }
    
    static func titleGrayMediumColor() -> UIColor {
        return UIColor(r: 116, g: 116, b: 116)
    }
    
    static func green() -> UIColor {
        return UIColor(r: 16, g: 186, b: 196)
    }
}



class Constants {
    static let MAIN_HEADER_TABLE:[String] = ["Location","Vente","Opportunités à saisir"]
    static var LOCATION_BIEN_TABLE:[String] = ["STUDIO","APPARTEMENT","VILLA","MAGASIN"]
    static var VENTE_BIEN_TABLE:[String] = ["APPARTEMENT","VILLA"]
    static var COMODITE_TABLE:[String] = ["Garage","Piscine","Jardin"]
    static var BUDGET_HOME_SUGGESTION = ["80 000 - 100 000","100 000 - 200 000","200 000 - 300 000","400 000 - 600 000","600 000 - 1 000 000"]
    static var PROFESSION_TYPE = ["Etudiant","Entrepreneur","Fonctionnaire"]
    
    
    static var MAP_KEY = "AIzaSyB0fjQB8NML8L7iIzV_r5_0P4lqYpNrsX0"
}




