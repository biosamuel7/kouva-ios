//
//  AppDelegate.swift
//  Kouva
//
//  Created by Bio'S on 02/06/2019.
//  Copyright © 2019 KOUVA. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDynamicLinks
import FirebaseInstanceID
import UserNotifications
import Fabric
import Crashlytics
import FBSDKCoreKit
import GoogleSignIn
import GoogleMaps
import GooglePlaces

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, MessagingDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        Fabric.with([Crashlytics.self])
        FirebaseApp.configure()
        
        //map config provide key
        GMSServices.provideAPIKey(Constants.MAP_KEY)
        GMSPlacesClient.provideAPIKey(Constants.MAP_KEY)
        
        // facebook sdk config
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
    
        //google sign config
        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
        
        
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        window?.rootViewController = TabBarController()
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        UIApplication.shared.statusBarStyle = .lightContent
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        Messaging.messaging().delegate = self
        
        
        return true
    }
   
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("dd token", fcmToken)
        //To be notified whenever the token is updated
        let dataDict:[String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        UserDefaults.standard.set(fcmToken, forKey: "fcmToken")
        if UserDefaults.standard.value(forKey: "userID") != nil {
            DataBaseService.instance.userDocument.updateData(["fcm_token":fcmToken])
        }
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        print("dd",userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print(userInfo)
        guard let homeID = userInfo["homeId"] as? String else { return }
        print(homeID)
        completionHandler(UIBackgroundFetchResult.newData)
    }

    
    // Deep link
    //Handle all Deep link above
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        if let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url) {
            self.handleIncomingDynamicLink(dynamicLink)
            return true
        } else {
            //GIDSignIn.sharedInstance().handle(url,sourceApplication: sourceApplication,annotation: annotation)
            return false
        }
    }
    //handle the custom url recieve by the user and lower version of ios app
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        print("debug custom scheme",url.absoluteString)
        if let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url) {
            self.handleIncomingDynamicLink(dynamicLink)
            return true
        } else {
            let sourceApplication: String? = options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String
            ApplicationDelegate.shared.application(app, open: url, sourceApplication: sourceApplication, annotation: nil)
            //GIDSignIn.sharedInstance()?.handle(url, sourceApplication: sourceApplication, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
            return application(app, open: url, sourceApplication: sourceApplication, annotation: "")
        }
    }
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return .portrait
    }
    
    //handle dynamick link recieve to user
    func handleIncomingDynamicLink(_ dynamicLink:DynamicLink) {
        guard let url = dynamicLink.url else {
            print("DynamicLink has no url !!!")
            return
        }
        print("debug url deep link",url.absoluteString)
        guard let component = URLComponents(url: url, resolvingAgainstBaseURL: false), let queryItems = component.queryItems else { return }
        
        switch component.path {
        case "/search/home": // path for listing homes
            if let id = queryItems[0].value {
                let VC = ShowDetailHomeViewController()
                VC.homeID = id
                guard let root = window?.rootViewController else { return  }
                root.present(VC, animated: true, completion: nil)
            }
        default: break
        }
        
    }
    //handle the dynamic links recieve
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        if let incomingUrl = userActivity.webpageURL {
            print("debug incoming Url \(incomingUrl)")
            let linkHandled = DynamicLinks.dynamicLinks().handleUniversalLink(incomingUrl) { (dynamicLink, error) in
                guard error == nil else {
                    print("Found an error \(error!.localizedDescription)")
                    return
                }
                if let _dynamickLink = dynamicLink {
                    self.handleIncomingDynamicLink(_dynamickLink)
                }
            }
            return linkHandled
        } 
        return false
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

