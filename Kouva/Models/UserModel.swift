//
//  UserModel.swift
//  Kouva
//
//  Created by Bio'S on 03/06/2019.
//  Copyright © 2019 Kouva. All rights reserved.
//

import UIKit

class UserModel {
    
    var ID:String!
    var userEmail:String!
    var userName:String!
    var userFirstName:String!
    var userNumber:String!
    var userProfession:String?
    var userSalaire:String?
    
    init?(data:[String:Any]) {
        guard let email = data["email"] as? String, let username = data["nom"] as? String, let firstname = data["prenom"] as? String, let number = data["numero"] as? String else {
            return nil
        }
        self.userEmail = email
        self.userName = username
        self.userFirstName = firstname
        self.userNumber = number
    }
}

struct Budget {
    var min:String!
    var max:String!
}

struct UserSearch {
    var budget: Budget!
    var type: String!
    var possession: String!
    var places: KGSMPlace!
}
