//
//  HomeModel.swift
//  Kouva
//
//  Created by Bio'S on 03/06/2019.
//  Copyright © 2019 Kouva. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestore

class GalleryModel {
    var urlPath:String!
    var id:String!
    init?(data:[String:Any]) {
        guard let url = data["path"], let id = data["propid"] else {
            return nil
        }
        self.id = (id as! String)
        self.urlPath = (url as! String)
    }
}

struct BudgetTrie {
    var prixToInt:Int!
    var prixToSting:String!
}



class WishListModel {
    var typeDeBien:String!
    var possessionDuBien:String!
    var state:String!
    
    init?(data:[String:Any]) {
        guard let possession = data["possessionDuBien"], let type = data["typeDuBien"], let state = data["state"] else {
            return nil
        }
        
        self.typeDeBien = (type as! String)
        self.possessionDuBien = (possession as! String)
        self.state = (state as! String)
    }
    
}

struct KGSMPlace {
    var longitude: Double!
    var latitude: Double
    
    init?(place data:[String:Any]) {
        guard let lng = data["longitude"] as? Double,
        let lat = data["latitude"] as? Double
        else {
            return nil
        }
        self.latitude = lat
        self.longitude = lng
    }
}

struct ImageHome {
    var path: String!
    var downloadURL: String!
    
    init?(image data:[String: Any]) {
        guard let path = data["path"] as? String,
        let url = data["downloadURL"] as? String else {
            return nil
        }
        self.path = path
        self.downloadURL = url
    }
}

class HomeModel {
    
    var type:String!
    var possessionDuBien:String!
    var douche:Int!
    var salon:Int!
    var cuisine:Int = 1
    var chambre:Int!
    var garage:Int!
    var description:String!
    var prix:String!
    var commune:String!
    var secteur:String!
    var piece:Int!
    var etageNiveau:String!
    var superficie:String!
    var place: KGSMPlace!
    var homeID:String!
    var numero:String!
    var pasDePorte:String!
    var isLiked:Bool = false
    var imagePrincipale:ImageHome!
    var imageGallerie:[ImageHome] = []
    var stateLike:String!
    var stateHome:Bool!
    var date: Timestamp!
    
    init?(data:[String:Any]) {
      guard let prix = data["prix"] as? String,
                   let location = data["commune"] as? String,
                   let secteur = data["quartier"] as? String,
                   let piece = data["pieces"] as? Int,
                   let principale = data["imagePrincipale"] as? [String:Any],
                   let state = data["disponibilite"] as? Bool,
                   let possession = data["possession"] as? String,
                   let type = data["type"] as? String
                   else {
                       return nil
               }
               self.possessionDuBien = possession
               self.type = type
               self.prix = prix
               self.commune = location
               self.secteur = secteur
               self.piece = piece
               self.stateHome = state
               
               if let img = ImageHome(image: principale) {
                   self.imagePrincipale = img
               }
    
    }
    
    
    init?(studio data: [String:Any]) {
        guard let prix = data["prix"] as? String,
            let location = data["commune"] as? String,
            let descript = data["description"] as? String,
            let secteur = data["quartier"] as? String,
            let piece = data["pieces"] as? Int,
            let superficie = data["superficie"] as? String,
            let principale = data["imagePrincipale"] as? [String:Any],
            let gallerieData = data["imageGallerie"] as? [Any],
            let position = data["place"] as? [String:Any],
            let state = data["disponibilite"] as? Bool,
            let createdAt = data["createdAt"] as? Timestamp,
            let num = data["numero"] as? String,
            let possession = data["possession"] as? String,
            let type = data["type"] as? String
            else {
                return nil
        }
        self.possessionDuBien = possession
        self.type = type
        self.prix = prix
        self.description = descript
        self.commune = location
        self.secteur = secteur
        self.piece = piece
        self.superficie = superficie
        self.stateHome = state
        self.date = createdAt
        self.numero = num
        
        if let place = KGSMPlace(place: position){
            self.place = place
        }
        
        if let img = ImageHome(image: principale) {
            self.imagePrincipale = img
        }
        self.imageGallerie.append(self.imagePrincipale)
        
        // RETRIEVE IMAGE GALLERY
        let galleries = gallerieData
        for imageItem in galleries {
            let image = (imageItem as! [String:Any])
            if let img =  ImageHome(image: image) {
                self.imageGallerie.append(img)
            }
        }
    }
    
    //appartement seraliazing
    init?(appartement data: [String:Any]) {
        guard let prix = data["prix"] as? String,
            let douche = data["douches"] as? Int,
            let chambre = data["chambres"] as? Int,
            let location = data["commune"] as? String,
            let descript = data["description"] as? String,
            let secteur = data["quartier"] as? String,
            let piece = data["pieces"] as? Int,
            let niveau = data["etage"] as? String,
            let superficie = data["superficie"] as? String,
            let principale = data["imagePrincipale"] as? [String:Any],
            let gallerieData = data["imageGallerie"] as? [Any],
            let position = data["place"] as? [String:Any],
            let state = data["disponibilite"] as? Bool,
            let createdAt = data["createdAt"] as? Timestamp,
            let num = data["numero"] as? String,
            let possession = data["possession"] as? String,
            let type = data["type"] as? String
            else {
                return nil
        }
        self.possessionDuBien = possession
        self.type = type
        self.douche = douche
        self.chambre = chambre
        self.prix = prix
        self.description = descript
        self.commune = location
        self.secteur = secteur
        self.piece = piece
        self.etageNiveau = niveau
        self.superficie = superficie
        self.stateHome = state
        self.date = createdAt
        self.numero = num
        
        if let place = KGSMPlace(place: position){
            self.place = place
        }
        
        if let img = ImageHome(image: principale) {
            self.imagePrincipale = img
        }
        self.imageGallerie.append(self.imagePrincipale)
        
        // RETRIEVE IMAGE GALLERY
        let galleries = gallerieData
        for imageItem in galleries {
            let image = (imageItem as! [String:Any])
            if let img =  ImageHome(image: image) {
                self.imageGallerie.append(img)
            }
        }
    }
    
    
    //Villa seraliazing
    init?(villa data: [String:Any]) {
        guard let prix = data["prix"] as? String,
            let douche = data["douches"] as? Int,
            let chambre = data["chambres"] as? Int,
            let location = data["commune"] as? String,
            let descript = data["description"] as? String,
            let secteur = data["quartier"] as? String,
            let piece = data["pieces"] as? Int,
            let salon = data["salons"] as? Int,
            let superficie = data["superficie"] as? String,
            let principale = data["imagePrincipale"] as? [String:Any],
            let gallerieData = data["imageGallerie"] as? [Any],
            let position = data["place"] as? [String:Any],
            let state = data["disponibilite"] as? Bool,
            let createdAt = data["createdAt"] as? Timestamp,
            let num = data["numero"] as? String,
            let possession = data["possession"] as? String,
            let type = data["type"] as? String
            else {
                return nil
        }
        self.possessionDuBien = possession
        self.type = type
        self.douche = douche
        self.chambre = chambre
        self.prix = prix
        self.description = descript
        self.commune = location
        self.secteur = secteur
        self.piece = piece
        self.salon = salon
        self.superficie = superficie
        self.stateHome = state
        self.date = createdAt
        self.numero = num
        
        if let place = KGSMPlace(place: position){
            self.place = place
        }
        
        if let img = ImageHome(image: principale) {
            self.imagePrincipale = img
        }
        self.imageGallerie.append(self.imagePrincipale)
        
        // RETRIEVE IMAGE GALLERY
        let galleries = gallerieData
        for imageItem in galleries {
            let image = (imageItem as! [String:Any])
            if let img =  ImageHome(image: image) {
                self.imageGallerie.append(img)
            }
        }
    }
    
    
    //Magazin
    init?(magasin data:[String:Any]) {
        guard let prix = data["prix"] as? String,
            let location = data["commune"] as? String,
            let descript = data["description"] as? String,
            let secteur = data["quartier"] as? String,
            let piece = data["pieces"] as? Int,
            let niveau = data["etage"] as? String,
            let superficie = data["superficie"] as? String,
            let principale = data["imagePrincipale"] as? [String:Any],
            let gallerieData = data["imageGallerie"] as? [Any],
            let position = data["place"] as? [String:Any],
            let state = data["disponibilite"] as? Bool,
            let createdAt = data["createdAt"] as? Timestamp,
            let num = data["numero"] as? String,
            let possession = data["possession"] as? String,
            let type = data["type"] as? String
            else {
                return nil
        }
        self.possessionDuBien = possession
        self.type = type
        self.prix = prix
        self.description = descript
        self.commune = location
        self.secteur = secteur
        self.piece = piece
        self.etageNiveau = niveau
        self.superficie = superficie
        self.stateHome = state
        self.date = createdAt
        self.numero = num
        
        if let place = KGSMPlace(place: position){
            self.place = place
        }
        
        if let img = ImageHome(image: principale) {
            self.imagePrincipale = img
        }
        self.imageGallerie.append(self.imagePrincipale)
        
        // RETRIEVE IMAGE GALLERY
        let galleries = gallerieData
        for imageItem in galleries {
            let image = (imageItem as! [String:Any])
            if let img =  ImageHome(image: image) {
                self.imageGallerie.append(img)
            }
        }
      
    }
}

