//
//  DataBaseService.swift
//  Kouva
//
//  Created by Bio'S on 02/06/2019.
//  Copyright © 2019 KOUVA. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth
import FirebaseFirestore

class DataBaseService {
    private static let _instance = DataBaseService()
    static var instance:DataBaseService {
        return _instance
    }
    func homeQuery(possession: String, type: String) -> Query {
        return Firestore.firestore().collection("homes").whereField("possession", isEqualTo: "\(possession)").whereField("type", isEqualTo: "\(type)")
    }
    
    var userWhishCollection: CollectionReference {
        return userCollection.document(AuthService.instance.userID).collection("wishlist")
    }
    
    func userCall(homeId: String) {
        Firestore.firestore().collection("calls").addDocument(data: ["homeId":homeId,"userId":AuthService.instance.userID,"createdAt":FieldValue.serverTimestamp()])
    }
    
    var userCollection: CollectionReference {
        return Firestore.firestore().collection("users")
    }
    
    var userSearchCollection: CollectionReference {
        return userDocument.collection("user-search")
    }
    
    var alerteCollection: CollectionReference {
        return userDocument.collection("alerte")
    }
    
    var homeCollection: CollectionReference {
        return Firestore.firestore().collection("homes")
    }
    
    
    var userDocument: DocumentReference {
        return userCollection.document(AuthService.instance.userID)
    }
    
    var setUserPhoneNumber:Void {
        if let _userNumber = UserDefaults.standard.value(forKey: "userNumber") as? String {
            userDocument.updateData(["numero":_userNumber])
        }
    }
    
    var setFcmToken: Void {
        if let fcmToken = UserDefaults.standard.value(forKey: "fcmToken") as? String {
            print("service",fcmToken)
            userDocument.updateData(["fcm_token":fcmToken])
        }
    }
    
    var homeOASCollection: CollectionReference {
        return Firestore.firestore().collection("oas")
    }
  
    func checkIfHomeIsLiked(likeBtn: UIButton, home: HomeModel) {
        if UserDefaults.standard.value(forKey: "userID") != nil {
            DispatchQueue.main.async {
                self.userWhishCollection.document(home.homeID).addSnapshotListener { (snapshot, erro) in
                    guard let snapshot = snapshot else {return}
                    if snapshot.exists {
                        home.isLiked = true
                        home.stateLike = "wish"
                        likeBtn.setImage(#imageLiteral(resourceName: "like").withRenderingMode(.alwaysOriginal), for: .normal)
                    } else {
                        home.isLiked = false
                        likeBtn.setImage(#imageLiteral(resourceName: "unlike").withRenderingMode(.alwaysOriginal), for: .normal)
                    }
                }
            }
        }
    }
    
    
}
