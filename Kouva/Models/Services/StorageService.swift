//
//  StorageService.swift
//  Kouva
//
//  Created by Bio'S on 03/06/2019.
//  Copyright © 2019 Kouva. All rights reserved.
//

import UIKit
import FirebaseStorage
import Firebase

class StorageService {
    private static let _instance = StorageService()
    static var instance:StorageService{
        return _instance
    }
    
    var mainRef:StorageReference{
        return Storage.storage().reference()
    }
    var userRef:StorageReference{
        return Storage.storage().reference(withPath: "Users/\(Auth.auth().currentUser!.uid)")
    }
    
    //    func UploadFileToCreateUser(email:String, password:String,username:String,user:User,VC:UIViewController) {
    //        let dataToSend = ["email":email,"username":username,"password":password,"ID":"\(user.uid)"] as [String : Any]
    //    }
    
}
