//
//  AuthService.swift
//  Kouva
//
//  Created by Bio'S on 03/06/2019.
//  Copyright © 2019 Kouva. All rights reserved.
//

import UIKit
import Foundation
import FirebaseAuth
import FBSDKLoginKit
import FBSDKCoreKit
import GoogleSignIn

class AuthService {
    private static let _instance = AuthService()
    
    static var instance:AuthService {
        return _instance
    }
    var userID:String {
        var userID:String!
        if let _userID = UserDefaults.standard.value(forKey: "userID") as? String {
            userID = _userID
        }
        return userID
    }
    
    func login(email:String, password:String,VC:UserLoginViewController) {
        Auth.auth().signIn(withEmail: email, password: password, completion: { [weak self] userAuth, error in
            //guard let strongSelf = self else { return }
            if error != nil {
                VC.progressView.stopAnimating()
                VC.connexionBtn.setTitle("Se connecter", for: .normal)
                VC.connexionBtn.isEnabled = true
                Alertes.AlerteError(error: "Email ou Password incorrete", VC: VC)
                
            } else {
                guard let user = userAuth?.user else { return }
                SaveUserSecure.saveID(userID: user.uid)
                DataBaseService.instance.userCollection.document(user.uid).getDocument { (snapshot, error) in
                    guard let data = snapshot?.data() else { return }
                    if let user = UserModel(data: data) {
                        UserDefaults.standard.set(user.userEmail, forKey: "userEmail")
                          UserDefaults.standard.set(user.userFirstName, forKey: "userFirstName")
                          UserDefaults.standard.set(user.userName, forKey: "userName")
                          VC.DismissLoginView()
                          VC.progressView.stopAnimating()
                          VC.connexionBtn.setTitle("Se connecter", for: .normal)
                          let emailIndexPath = IndexPath(item: 0, section: 0)
                          if let cell = VC.textFieldCollection.cellForItem(at: emailIndexPath) as? TextFieldCell {
                              cell.input.text = nil
                      }
                    }
                }
            }
        })
    }
    
    func retriveUser(uid: String, VC: UserLoginViewController) {
        
    }
    
    func facebookLogin(VC:UserLoginViewController) {
        VC.connectionLoadView.isHidden = false
        let loginManager = LoginManager()
        loginManager.logOut()
        loginManager.logIn(permissions: ["email","public_profile"], from: VC.self) { (loginResult, error) in
            if error != nil {
                print("FB Login failed:", error!)
                return
            }
            if let cancel = loginResult?.isCancelled {
                if cancel {
                    VC.connectionLoadView.isHidden = true
                } else {
                    print("debug:","user id connected")
                    if let token = AccessToken.current?.tokenString {
                        let credential = FacebookAuthProvider.credential(withAccessToken: token)
                        print("debug",credential)
                        Auth.auth().signIn(with: credential, completion: { (authResult, error) in
                            if let err = error {
                                print("debug err:",err.localizedDescription)
                                VC.connectionLoadView.isHidden = true
                            }
                            guard let user = authResult?.user else { return }
                            print("debug id :",user.uid)
                            
                            SaveUserSecure.saveID(userID: user.uid)
                            UserDefaults.standard.set(user.email!, forKey: "userEmail")
                            UserDefaults.standard.set(user.displayName!, forKey: "userFirstName")
                            UserDefaults.standard.set("", forKey: "userName")

                            DataBaseService.instance.setFcmToken
                            
                            //check if user number is already save before save user data informations
                            DataBaseService.instance.userDocument.getDocument { (snapshot, error) in
                                if let snapshot = snapshot {
                                    if snapshot.exists {
                                        if let data = snapshot.data(){
                                            if let _ = data["numero"] as? String {
                                                VC.connectionLoadView.isHidden = true
                                                VC.DismissLoginView()
                                            }else {
                                                NotificationCenter.default.post(name: .didShowUserAddNumberView, object: nil)
                                            }
                                        }
                                    } else {
                                        let data:[String:Any] = ["nom":user.displayName!,"prenom":"","email":user.email!]
                                        DataBaseService.instance.userDocument.setData(data)
                                        DataBaseService.instance.setFcmToken
                                        NotificationCenter.default.post(name: .didShowUserAddNumberView, object: nil)
                                   }
                                }
                            }
                            
                    })
                }
            }
            
        }
        
    }
    }
    
    
    // TODO: store user info using user form
    func CreateUser(email:String, password:String,VC:UserCreationController,dataInfo:[String:Any]) {
        Auth.auth().createUser(withEmail: email, password: password) { (authResult, error) in
            if error != nil {
                VC.progressView.stopAnimating()
                VC.NextTapBtn.SetTitleBtnAndFont(title: "Valider")
                VC.NextTapBtn.isEnabled = true
                Alertes.AlerteError(error: "Utilisateur déjà enregistre", VC: VC)
            }else {
                guard let user = authResult?.user else { return }
                SaveUserSecure.saveID(userID: user.uid)
                DataBaseService.instance.userDocument.setData(dataInfo)
                DataBaseService.instance.setFcmToken
                
                UserDefaults.standard.set(dataInfo["email"], forKey: "userEmail")
                UserDefaults.standard.set(dataInfo["prenom"], forKey: "userFirstName")
                UserDefaults.standard.set(dataInfo["nom"], forKey: "userName")
                
                NotificationCenter.default.post(name: .didShowUserAddNumberView, object: nil)
                VC.DismissView()
                
                VC.progressView.stopAnimating()
                VC.NextTapBtn.SetTitleBtnAndFont(title: "Valider")
                VC.NextTapBtn.isEnabled = true
                
            }
        }
    }
        
    func logOut() {
        SaveUserSecure.RemoveUser()
        
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
            GIDSignIn.sharedInstance()?.signOut()
            LoginManager().logOut()
            SaveUserSecure.RemoveUser()
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
        
    }
    
}

